# API So FAR

Le back doit être déployé sur un serveur php (ex en local: wamp).
Actuellement déployé sur AWS Elastic Beanstalk.

## Avertissement
Le code php utilise certaines fonctionnalités fournis par le SDK d'AWS (notamment pour le stockage des photos).
Ces bouts de codes ne sont donc disponible que sur un serveur AWS.

## Déployement
Compresser le dossier [sofar_api](./sofar_api),
**index.php**, **confidentiality.html** et **contact.html** en **api.zip**.
Ce fichier zip contient le serveur php à déployer.



Se rendre sur la console AWS et aller dans : **Elastic Beanstalk > Environments > Sofarapi-env**
puis cliquer sur *Upload and deploy*, puis *choose file* et sélectionner le dossier **api.zip** précédemment 
créé.

Si aucune erreur n'est rencontrée, le serveur est déployé.
