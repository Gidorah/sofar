<?php

include "./connect.php";

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$result = array();


$NAME = addslashes($data['name']);
$ADRESSE = addslashes($data['adresse']);
$NUM_SIRET = $data['siret'];
$EMAIL = $data['email'];
$DESCRIPTION = addslashes($data['desc']);
$CATEGORIE = addslashes($data['cat']);
$TERASSE = $data['terasse'];
$longitude = $data['longitude'];
$latitude = $data['latitude'];
$service = $data['service'];
$commander = $data['commander'];

//Récupération des horaires par jour
//LUNDI

if($data['horairesOuvertL'] == NULL || $data['horairesOuvertL'] == "NULL" ){
    $HORAIRES_LUNDI="ferme";
} else {
    if($data['demiHorairesOuvertL'] == NULL || $data['demiHorairesOuvertL'] == "NULL" ){
        $HORAIRES_LUNDI= $data['horairesOuvertL']."-". $data['horairesFermeL'];
        }else {
            $HORAIRES_LUNDI= $data['horairesOuvertL']."-". $data['horairesFermeL']."/".$data['demiHorairesOuvertL']."-".$data['demiHorairesFermeL'];
        }
}
//MARDI
if($data['horairesOuvertMa'] == NULL || $data['horairesOuvertMa'] == "NULL"  ){
    $HORAIRES_MARDI="ferme";
} else {
    if($data['demiHorairesOuvertMa'] == NULL || $data['demiHorairesOuvertMa'] == "NULL" ){
        $HORAIRES_MARDI= $data['horairesOuvertMa']."-". $data['horairesFermeMa'];
        }else {
            $HORAIRES_MARDI= $data['horairesOuvertMa']."-". $data['horairesFermeMa']."/".$data['demiHorairesOuvertMa']."-".$data['demiHorairesFermeMa'];
        }
}
//MERCREDI
if($data['horairesOuvertMe'] == NULL || $data['horairesOuvertMe'] == "NULL" ){
    $HORAIRES_MERCREDI="ferme";
} else {
    if($data['demiHorairesOuvertMe'] == NULL || $data['demiHorairesOuvertMe'] == "NULL" ){
        $HORAIRES_MERCREDI= $data['horairesOuvertMe']."-". $data['horairesFermeMe'];
        }else {
            $HORAIRES_MERCREDI= $data['horairesOuvertMe']."-". $data['horairesFermeMe']."/".$data['demiHorairesOuvertMe']."-".$data['demiHorairesFermeMe'];
        }
}
//JEUDI
if($data['horairesOuvertJ'] == NULL || $data['horairesOuvertJ'] == "NULL"  ){
    $HORAIRES_JEUDI="ferme";
} else {
    if($data['demiHorairesOuvertJ'] == NULL || $data['demiHorairesOuvertJ'] == "NULL" ){
        $HORAIRES_JEUDI= $data['horairesOuvertJ']."-". $data['horairesFermeJ'];
        }else {
            $HORAIRES_JEUDI= $data['horairesOuvertJ']."-". $data['horairesFermeJ']."/".$data['demiHorairesOuvertJ']."-".$data['demiHorairesFermeJ'];
        }
}
//VENDREDI
if($data['horairesOuvertV'] == NULL || $data['horairesOuvertV'] == "NULL" ){
    $HORAIRES_VENDREDI="ferme";
} else {
    if($data['demiHorairesOuvertV'] == NULL || $data['demiHorairesOuvertV'] == "NULL" ){
        $HORAIRES_VENDREDI= $data['horairesOuvertV']."-". $data['horairesFermeV'];
        }else {
            $HORAIRES_VENDREDI= $data['horairesOuvertV']."-". $data['horairesFermeV']."/".$data['demiHorairesOuvertV']."-".$data['demiHorairesFermeV'];
        }
}
//SAMEDI
if($data['horairesOuvertS'] == NULL || $data['horairesOuvertS'] == "NULL"){
    $HORAIRES_SAMEDI="ferme";
} else {
    if($data['demiHorairesOuvertS'] == NULL || $data['demiHorairesOuvertS'] == "NULL" ){
        $HORAIRES_SAMEDI= $data['horairesOuvertS']."-". $data['horairesFermeS'];
        }else {
            $HORAIRES_SAMEDI= $data['horairesOuvertS']."-". $data['horairesFermeS']."/".$data['demiHorairesOuvertS']."-".$data['demiHorairesFermeS'];
        }
}
//DIMANCHE
if($data['horairesOuvertD'] == NULL || $data['horairesOuvertD'] == "NULL" ){
    $HORAIRES_DIMANCHE="ferme";
} else {
    if($data['demiHorairesOuvertD'] == NULL || $data['demiHorairesOuvertD'] == "NULL" ){
    $HORAIRES_DIMANCHE= $data['horairesOuvertD']."-". $data['horairesFermeD'];
    }else {
        $HORAIRES_DIMANCHE= $data['horairesOuvertD']."-". $data['horairesFermeD']."/".$data['demiHorairesOuvertD']."-".$data['demiHorairesFermeD'];
    }

}




$sql="INSERT INTO `pro` ( `date_inscr`,`nom_pro`, `adresse`, `siret`, `email_pro`, `terrasse_pro`, `description_pro`, `categorie_pro`, `mdp_pro`, `service`, `longitude`, `latitude`, `commander_dispo`) 
      VALUES (DATE( NOW() ) ,'$NAME', '$ADRESSE', '$NUM_SIRET', '$EMAIL', '$TERASSE' , '$DESCRIPTION', '$CATEGORIE', ' ', '$service', '$longitude', '$latitude', '$commander')";

$query = mysqli_query($_SESSION['connexion'],$sql);

if ($query) {
    echo "New Pro created successfully // ";
    $rep="SELECT id_pro FROM pro WHERE email_pro = '$EMAIL' ";
    $query = mysqli_query($_SESSION['connexion'],$rep);

    $i = 0;
    while ($row = mysqli_fetch_assoc($query)) {
        foreach ($row as $key => $ID_PRO) {
            $message[$i][$key] = $ID_PRO;
        }
        $i++;
    }

    if ($query) {

        echo " -- récupération réussie id = $ID_PRO ";
        $sql2=" INSERT INTO `horaires` (`id_pro`, `lundi`, `mardi`, `mercredi`, `jeudi`, `vendredi`, `samedi`, `dimanche`) VALUES ('$ID_PRO', '$HORAIRES_LUNDI', '$HORAIRES_MARDI', '$HORAIRES_MERCREDI', '$HORAIRES_JEUDI', '$HORAIRES_VENDREDI', '$HORAIRES_SAMEDI', '$HORAIRES_DIMANCHE' ) ";
        $query = mysqli_query($_SESSION['connexion'],$sql2);

        if ($query) {
            echo "Horaires Pro ajoutés";
            $sql3 = "INSERT INTO `delais` VALUES ('$ID_PRO', 60, 'aucune' )  ";
            $query = mysqli_query($_SESSION['connexion'],$sql3);
            if($query){
                echo "Délais Pro ajoutés";
            }else{
                echo "Error: " . $sql3 . "<br/>" ;
            }
        }else {
            echo "Error: " . $sql2 . "<br/>" ;
        }
    } else {
        echo "Error: " . $ID_PRO . "<br/>" ;
    }
} else { echo "Error: " . $sql . "<br>" ;
}




?>
