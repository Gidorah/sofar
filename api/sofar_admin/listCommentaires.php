<?php
include "./connect.php";
$message = array();
$query = mysqli_query($_SESSION['connexion'], "SELECT * FROM notes  WHERE `commentaire`!='' ORDER BY `idnotes` DESC");

if (!$query) {
    $message['status'] = 'error';
} else {
    //$message['result'] = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($query)) {
        foreach ($row as $key => $value) {
            //$message['result'][$i][$key] = $value;
            $message[$i][$key] = $value;
        }
        $i++;
    }
}
echo json_encode($message);
