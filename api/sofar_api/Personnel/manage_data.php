<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;


$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$action = $data['action'];

if ($action == "login") {

    $mdp = $data['mdp'];
    $login = $data['login'];

    $q = mysqli_query($_SESSION['connexion'],
        "SELECT * FROM personnel WHERE login='$login'");

    if ($q) {
        if ($q->num_rows === 1) {
            $result = array();
            $row = mysqli_fetch_assoc($q);
            $mdpPer = '';
            foreach ($row as $key => $value) {
                if (!in_array($key, array('login', 'mdp_per'))) {
                    $result[$key] = $value;
                } else if ($key == 'mdp_per') {
                    $mdpPer = $value;
                }
            }

            if (password_verify($mdp, $mdpPer)) {
                $expiration = time() + 365 * 60 * 24 * 60;
                $issuer = 'localhost';

                $payload = [
                    'iat' => time(),
                    'user_id' => $result['id_per'],
                    'cat' => 'personnel',
                    'pro_id' => $result['id_pro'],
                    'exp' => $expiration,
                    'iss' => $issuer
                ];

                $token = Token::customPayload($payload, $_SESSION['secret']);

                $message['result']['pers'] = $result;
                $message['result']['access_token'] = $token;
            } else {
                $message['error'] = 'wrong-pw';
            }
        } else {
            $message['error'] = 'not-found';
        }
    } else {
        $message['error'] = mysqli_error($_SESSION['connexion']);
    }
} else if ($action == 'fetch') {
    $token = $data['token'];
    if ($token == null) {
        $message['error'] = 'no token provided';
        echo json_encode($message);
        exit();
    }
    $pers = new User($token);
// Check for token validation
    if ($pers->isValid()) {
        $idPers = $pers->getPayload()['user_id'];

        $query = mysqli_query($_SESSION['connexion'], "SELECT id_per, id_pro, alias, date_creation FROM personnel WHERE id_per = '$idPers'");
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        } else {
            $row = mysqli_fetch_assoc($query);
            foreach ($row as $key => $value) {
                $message['pers'][$key] = $value;
            }

            $idPro = $message['pers']['id_pro'];
            $queryEtab = mysqli_query($_SESSION['connexion'],
                "SELECT nom_pro, adresse, categorie_pro, description_pro, service, commander_dispo FROM pro WHERE id_pro = '$idPro'");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            } else {
                $row = mysqli_fetch_assoc($queryEtab);
                foreach ($row as $key => $value) {
                    $message['etab'][$key] = $value;
                }
            }
        }
    } else {
        $message['error'] = 'auth-token wrong signature';
    }
} else {
    $message['error'] = 'action ' . $data['action'] . ' not recognized';
}


echo json_encode($message);
