<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
// Check for token validation
if ($validate) {
    $id = $payload['user_id'];

    $password = $data['password'];
    $newPassword = $data['new_password'];

    // Check taille du nouveau mdp
    if (strlen($newPassword) < 6) {
        $message['error'] = 'too-short';
    } else {
        $q = mysqli_query($_SESSION['connexion'], "SELECT mdp_per FROM personnel WHERE id_per=$id");

        if (!$q) {
            $message['error'] = '$q : ' . mysqli_error($_SESSION['connexion']);
        } else {
            // Check si le mdp actuel est bon

            $resultQ = mysqli_fetch_row($q);
            if (password_verify($password, $resultQ[0])) {
                // Tout est ok on peut update le pw
                $newPasswordEncrypted = password_hash($newPassword, PASSWORD_DEFAULT);

                $queryUpdate = mysqli_query($_SESSION['connexion'], "UPDATE personnel SET mdp_per = '$newPasswordEncrypted' WHERE id_per=$id");
                if (!$queryUpdate) {
                    $message['error'] = '$queryUpdate : ' . mysqli_error($_SESSION['connexion']);
                } else {
                    $message['status'] = 'success';
                }
            } else {
                $message['error'] = 'wrong-pw';
            }
        }
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);