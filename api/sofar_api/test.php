<!DOCTYPE html>
<html>
<body>
<h1>Bucket test</h1>
<?php
include 'connect.php';

// snippet-start:[s3.php.list_buckets.complete]
// snippet-start:[s3.php.list_buckets.import]

require './vendor/autoload.php';

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

// snippet-end:[s3.php.list_buckets.import]


/**
 * List your Amazon S3 buckets.
 *
 * This code expects that you have AWS credentials set up per:
 * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html
 */

//Create a S3Client
// snippet-start:[s3.php.list_buckets.main]

// Instantiate the S3 class and point it at the desired host
$s3Client = S3Client::factory(array('credentials' => [
    'key' => AWS_KEY,
    'secret' => AWS_SECRET_KEY
],
    'region' => 'eu-west-3',
    'version' => 'latest'));

//Listing all S3 Bucket
$buckets = $s3Client->listBuckets();
echo json_encode($buckets['Buckets']);
foreach ($buckets['Buckets'] as $bucket) {
    echo "<p>" . $bucket['Name'] . "\n </p>";
}


?>
</body>
</html>