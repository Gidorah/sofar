<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $payload['user_id'];

    if ($data['action'] === 'insert') {

        $nomf = $data['nom'];
        $idp = $data['idp'];
        $nomf = addslashes($nomf);

        $q = mysqli_query($_SESSION['connexion'], "SELECT * FROM favoris WHERE `id_pro`='$idp'AND `id_u`='$idu'");
        $nbr = $q->num_rows;

        if ($nbr != 0) {
            $message['status'] = 'already_in_db';
        } else {
            $query = mysqli_query($_SESSION['connexion'], "INSERT INTO `favoris`( `nom_fav`, `id_u`, `id_pro`) VALUES ('$nomf', '$idu', '$idp')");
            $message['status'] = 'fav_ajoute';
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        }
    }

    if ($data['action'] === 'update') {

        $nomf = $data['nom'];
        $idp = $data['idp'];
        $nomf = addslashes($nomf);

        $query = mysqli_query($_SESSION['connexion'], "UPDATE `favoris`
             SET `nom_fav`='$nomf', `id_u`= '$idu', `id_pro`='$idp' 
             WHERE `id_pro`='$idp'AND `id_u`='$idu'");

        $message['status'] = 'success';
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
    }
    if ($data['action'] === 'delete') {

        $idp = $data['idp'];

        $query = mysqli_query($_SESSION['connexion'], "DELETE FROM `favoris`
                                    WHERE `id_u`='$idu' AND `id_pro`= '$idp'");

        $message['status'] = 'fav_retire';
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);