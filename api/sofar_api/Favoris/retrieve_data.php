<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;


$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $payload['user_id'];

    $query = mysqli_query($_SESSION['connexion'], "SELECT 
       f.`nom_fav`, p.`description_pro`, p.`categorie_pro`, p.`id_pro`,
       p.`nom_pro`, p.`adresse`, p.`date_ouvert_pro`, p.`date_happy_pro`,
       p.`terrasse_pro`, p.photo_pro, p.need_resa_details, p.commander_dispo
                            FROM `pro` p INNER JOIN  `favoris` f ON f.`id_u`=$idu AND f.`id_pro`=p.`id_pro`");


    if (!$query) {
        $message['status'] = mysqli_error($_SESSION['connexion']);
    }
    $message['result'] = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($query)) {
        foreach ($row as $key => $value) {
            $message['result'][$i][$key] = $value;
            if ($key == 'photo_pro') {
                $message['result'][$i]['photo_pro'] = '';
                /*
                 * fetch photo
                 */
                // Instantiate the S3 class and point it at the desired host
                $s3Client = S3Client::factory(array('credentials' => [
                    'key' => AWS_KEY,
                    'secret' => AWS_SECRET_KEY
                ],
                    'region' => 'eu-west-3',
                    'version' => 'latest'
                ));
                $s3Client->registerStreamWrapper();
                $baseURL = 's3://' . BUCKET_NAME;

                $photoPath = $baseURL . '/profile_pictures/pro/' . $value;
                if (file_exists($photoPath) && $value != '') {
                    $message['result'][$i]['photo_pro'] = base64_encode(file_get_contents($photoPath));
                }
            }
        }
        $i++;
    }

//    $result = mysqli_fetch_all($query);
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);


