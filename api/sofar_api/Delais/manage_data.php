<?php

include "../connect.php";

require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');

$data = json_decode($input, true);

$message = array();
$result = array();

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

$id_pro = $payload['user_id'];

if ($validate) {
    if ($data['action'] == "changement_jours") {

        $delais_jours = $data['delais_jours'];

        $query = mysqli_query($_SESSION['connexion'], "UPDATE `delais` SET delais_jours = '$delais_jours' WHERE `id_pro` = '$id_pro'");

        $message['status'] = 'delais_modifies';
        if (!$query) {
            $message['status'] = 'error';
        }

        echo json_encode($message);

    }

    else if ($data['action'] == "changement_heures") {

        $delais_heures = $data['delais_heures'];

        $query = mysqli_query($_SESSION['connexion'], "UPDATE `delais` SET delais_heures= '$delais_heures'WHERE `id_pro` = '$id_pro'");

        $message['status'] = 'delais_modifies';
        if (!$query) {
            $message['status'] = 'error';
        }

        echo json_encode($message);

    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

?>
