<?php
include "../../connect.php";
require "../../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';
$action = $data['action'];

if ($action === 'fetch-all') {
    $idPro = $data['id_pro'];
    $query = mysqli_query($_SESSION['connexion'],
        "SELECT * FROM articles WHERE id_pro = '$idPro'");
    if (!$query) {
        $message['error'] = mysqli_error($_SESSION['connexion']);
    } else {
        $message['result'] = array();
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            foreach ($row as $key => $value) {
                $message['result'][$i][$key] = $value;
            }
            $i++;
        }
    }
} else {


    $token = $data['token'];
    if ($token == null) {
        $message['error'] = 'no token provided';
        echo json_encode($message);
        exit();
    }
    $payload = Token::getPayload($token, $_SESSION['secret']);
    $validate = Token::validate($token, $_SESSION['secret']);

    if ($validate) {
        $idPro = $payload['user_id'];

        if ($action === 'add') {

            $nom = addslashes($data['nom']);
            $prix = $data['prix'];
            $desc = addslashes($data['desc']);
            $dispo = $data['dispo'];
            $categorie = $data['categorie'];
            $photo = addslashes($data['photo']);
            $taxe = $data['taxe'];
            // TODO : photo
            $query = mysqli_query($_SESSION['connexion'],
                "INSERT INTO articles (id_pro, article_nom, article_prix, article_desc, article_dispo, article_categorie, article_photo, article_taxe)
                    VALUES ('$idPro', '$nom', '$prix', '$desc', '$dispo', '$categorie', '$photo', '$taxe')");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }

        } else if ($action === 'edit') {
            $idArt = $data['article_id'];
            $nom = addslashes($data['nom']);
            $prix = $data['prix'];
            $desc = addslashes($data['desc']);
            $dispo = $data['dispo'];
            $categorie = $data['categorie'];
            $photo = addslashes($data['photo']);
            $taxe = $data['taxe'];
            // TODO : photo
            $query = mysqli_query($_SESSION['connexion'],
                "UPDATE articles
                    SET id_pro = '$idPro', article_nom = '$nom', article_prix = '$prix', article_desc = '$desc',
                     article_dispo = '$dispo', article_categorie = '$categorie', article_photo = '$photo', article_taxe = '$taxe'
                     WHERE article_id = $idArt and id_pro = $idPro");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        } else if ($action === 'delete') {
            $idArt = $data['article_id'];
            $query = mysqli_query($_SESSION['connexion'],
                "DELETE FROM articles WHERE article_id = '$idArt' and id_pro = '$idPro'");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        } else {
            $message['error'] = 'action \'' . $action . '\' not recognized.';
        }
    } else {
        $message['error'] = 'auth-token wrong signature';
    }
}

echo json_encode($message);