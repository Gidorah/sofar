<?php
include "../../connect.php";
require "../../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$action = $data['action'];

if ($action === 'fetch-all') {
    $idPro = $data['id_pro'];
    $query = mysqli_query($_SESSION['connexion'],
        "SELECT * FROM categories WHERE id_pro = '$idPro'");
    if (!$query) {
        $message['error'] = mysqli_error($_SESSION['connexion']);
    } else {
        $message['result'] = array();
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            foreach ($row as $key => $value) {
                $message['result'][$i][$key] = $value;
            }
            $i++;
        }
    }
} else {

    $token = $data['token'];
    if ($token == null) {
        $message['error'] = 'no token provided';
        echo json_encode($message);
        exit();
    }
    $payload = Token::getPayload($token, $_SESSION['secret']);
    $validate = Token::validate($token, $_SESSION['secret']);

    if ($validate) {
        $idPro = $payload['user_id'];

        if ($action === 'add') {
            if (array_key_exists('cat_name', $data)) {
                $catName = addslashes($data['cat_name']);
                $query = mysqli_query($_SESSION['connexion'],
                    "INSERT INTO categories (id_pro, categorie_nom) VALUES ('$idPro', '$catName')");
                if (!$query) {
                    $message['error'] = mysqli_error($_SESSION['connexion']);
                }
            } else {
                $message['error'] = 'the name of the categorie to add was not found in the submitted form';
            }
        } else if ($action === 'edit') {
            $catNom = addslashes($data['nom']);
            $idCat = $data['categorie_id'];
            $query = mysqli_query($_SESSION['connexion'],
                "UPDATE categories
                    SET categorie_nom = '$catNom'
                    WHERE categorie_id = '$idCat' and id_pro = '$idPro'");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        } else if ($action === 'delete') {
            $idCat = $data['categorie_id'];
            $queryDelArts = mysqli_query($_SESSION['connexion'],
                "DELETE FROM articles WHERE article_categorie = '$idCat' and id_pro = '$idPro'");
            if (!$queryDelArts) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            } else {
                $queryDelCat = mysqli_query($_SESSION['connexion'],
                    "DELETE FROM categories WHERE categorie_id = '$idCat' and id_pro = '$idPro'");
                if (!$queryDelCat) {
                    $message['error'] = mysqli_error($_SESSION['connexion']);
                }
            }
        } else {
            $message['error'] = 'action \'' . $action . '\' not recognized.';
        }
    } else {
        $message['error'] = 'auth-token wrong signature';
    }
}
echo json_encode($message);
