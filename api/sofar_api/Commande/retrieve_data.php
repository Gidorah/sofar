<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;

$postdata = file_get_contents("php://input");
$data = json_decode($postdata);

$action = $data->action;

$message = array();

$jwtToken = $data->token;
if ($jwtToken == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($jwtToken, $_SESSION['secret']);
$validate = Token::validate($jwtToken, $_SESSION['secret']);
if ($validate) {
    $idUser = $payload['user_id'];

    if ($action == 'fetch-all') {
        $queryOrder = mysqli_query($_SESSION['connexion'],
            "SELECT * FROM `order` WHERE user_id = '$idUser' ORDER BY `createdAt` DESC");
        if (!$queryOrder) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        } else {
            $message['orders'] = array();
            $i = 0;

            $distinctPro = array();

            $s3Client = S3Client::factory(array('credentials' => [
                'key' => AWS_KEY,
                'secret' => AWS_SECRET_KEY
            ],
                'region' => 'eu-west-3',
                'version' => 'latest'
            ));
            $s3Client->registerStreamWrapper();
            $baseURL = 's3://' . BUCKET_NAME;

            while ($row = mysqli_fetch_assoc($queryOrder)) {
                foreach ($row as $key => $value) {
                    $message['orders'][$i][$key] = $value;

                    if ($key == 'pro_id') {
                        $id_pro = $value;
                        if (!array_key_exists($id_pro, $distinctPro)) {
                            $q2 = mysqli_query($_SESSION['connexion'], "SELECT photo_pro FROM `pro` WHERE id_pro='$id_pro' ");
                            if ($q2) {
                                $row2 = mysqli_fetch_assoc($q2);
                                $photo_pro = $row2['photo_pro'];

                                $pro['photo_pro'] = '';

                                $photoPath = $baseURL . '/profile_pictures/pro/' . $photo_pro;
                                if (file_exists($photoPath) && $photo_pro != '') {
                                    $pro['photo_pro'] = base64_encode(file_get_contents($photoPath));
                                }

                                $distinctPro[$id_pro] = $pro;
                            } else {
                                $message['error'] = mysqli_error($_SESSION['connexion']);
                            }
                        }
                    }
                }

                $orderId = $row['id'];

                $queryOrderItem = mysqli_query($_SESSION['connexion'],
                    "SELECT * FROM `order_item` WHERE order_id = '$orderId'");
                if (!$queryOrderItem) {
                    $message['error'] = mysqli_error($_SESSION['connexion']);
                } else {
                    $message['orders'][$i]['items'] = array();
                    $j = 0;
                    while ($row = mysqli_fetch_assoc($queryOrderItem)) {
                        foreach ($row as $key => $value) {
                            $message['orders'][$i]['items'][$j][$key] = $value;
                        }
                        $j++;
                    }
                }
                $i++;
            }

            $message['pros'] = $distinctPro;
        }
    } else {
        $message['error'] = 'unknown action: ' . $action . '.';
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}
echo json_encode($message);
