<?php

include "../connect.php";
require "../vendor/autoload.php";

use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;
use ReallySimpleJWT\Token;


$postdata = file_get_contents("php://input");
$data = json_decode($postdata);

$action = $data->action;

$message = array();

$jwtToken = $data->user_token;
if ($jwtToken == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($jwtToken, $_SESSION['secret']);
$validate = Token::validate($jwtToken, $_SESSION['secret']);
if ($validate) {
    $idUser = $payload['user_id'];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $stripeToken = $data->stripe_token;

        Stripe::setApiKey($_SESSION['stripe']['secret_key']);

        $idPro = $data->id_pro;
        $price = intval(($data->price) * 100);
        $pourboire = $data->commande->pourboire;
        if ($pourboire < 0) {
            $message['error'] = 'Le pourboire ne peut pas être négatif.';
            goto end;
        }
        $calculatedPrice = $pourboire;
        $panier = $data->commande->panier;
        $service = $data->service;

        $orderItems = array();
        /*
         * On parcours le panier parcouru (article et leurs quantité)
         * On récupère pour chaque article le nom, le prix, la taxe
         * On calcul le total du prix pour chaque article * quantité
         * On vérifie que chaque article est encore disponible
         */
        foreach ($panier as $produit) {
            $quantity = $produit->quantity;
            $article = $produit->article;

            if ($quantity < 1) goto end;

            $idArticle = $article->article_id;
            /*
             * On récupère ici les infos sur un article et on vérifie sa disponibilité
             */
            $queryGetDispo = mysqli_query($_SESSION['connexion'],
                "SELECT article_prix, article_nom, article_dispo, article_taxe FROM articles WHERE id_pro = '$idPro' and article_id = '$idArticle'");
            if (!$queryGetDispo) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
                goto end;
            }
            $row = mysqli_fetch_assoc($queryGetDispo);
            $articlePrix = $row['article_prix'];
            $articleNom = $row['article_nom'];
            $articleDispo = $row['article_dispo'];
            $articleTaxe = $row['article_taxe'];
            if (!$articleDispo) {
                $message['error'] = 'L\'article ' . $articleNom . '(' . $idArticle . ') n\'est actuellement pas disponible.';
                goto end;
            }
            $total = round($articlePrix * $quantity, 2);
            $calculatedPrice += $total;
            $orderItem = array(
                'article_id' => $idArticle,
                'quantity' => $quantity,
                'article_nom' => $articleNom,
                'article_prix' => $articlePrix,
                'article_taxe' => $articleTaxe,
                'total' => $total,
            );
            array_push($orderItems, $orderItem);
        }

        /*
         * On récupère les infos du user qui fait la commande
         */
        $queryGetUser = mysqli_query($_SESSION['connexion'],
            "SELECT nom_u, prenom_u, stripe_id, email_u FROM user WHERE id_u = '$idUser'");
        if (!$queryGetUser) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
            goto end;
        }
        $userRow = $queryGetUser->fetch_row();
        $userNom = $userRow[0];
        $userPrenom = $userRow[1];
        $stripeCustomerId = $userRow[2];
        $email_u = $userRow[3];

        if (!$stripeCustomerId || $stripeCustomerId == '') {
//            Si le user n'a pas de Stripe::Customer d'associé, on le crée.
            $customer = Customer::create(array(
                'email' => $email_u
            ));
            $stripeCustomerId = $customer['id'];
            $queryUpdateUser = mysqli_query($_SESSION['connexion'],
                "UPDATE user SET stripe_id = '$stripeCustomerId' where email_u = '$email_u'");
            if (!$queryUpdateUser) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
                goto end;
            }
        }

        $source = Customer::createSource(
            $stripeCustomerId,
            [
                'source' => $stripeToken,
            ]
        );

        $stripeSourceId = $source['id'];

        /*
         * On récupère les infos liées au pro chez qui la commande est passée
         */
        $queryGetPro = mysqli_query($_SESSION['connexion'],
            "SELECT nom_pro, service FROM pro WHERE id_pro = '$idPro'");
        if (!$queryGetPro) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
            goto end;
        }
        $proRow = $queryGetPro->fetch_row();
        $proNom = $proRow[0];
        $proService = $proRow[1]; // TODO : check si service et proService correspondent

        $orderStatus = 'creation';

        /*
         * Création de la commande dans la table: order
         */
        $querySetCommande = mysqli_query($_SESSION['connexion'],
            "INSERT INTO `order`
                (user_id, user_prenom, user_nom, pro_id, pro_nom, order_status, total, pourboire, service, stripe_source)
                VALUES ('$idUser', '$userPrenom', '$userNom', '$idPro', '$proNom', '$orderStatus', '$calculatedPrice',
                        '$pourboire', '$service', '$stripeSourceId')");
        if (!$querySetCommande) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
            goto end;
        }

        $orderId = mysqli_insert_id($_SESSION['connexion']);

        /*
         * Pour chaque article du panier, on créer une ligne dans la table: order_item
         */
        foreach ($orderItems as $item) {
            $artId = $item['article_id'];
            $artQuantity = $item['quantity'];
            $artNom = $item['article_nom'];
            $artPrix = $item['article_prix'];
            $artTaxe = $item['article_taxe'];
            $artTotal = $item['total'];
            $querySetItem = mysqli_query($_SESSION['connexion'],
                "INSERT INTO `order_item`
                (article_id, quantity, article_nom, article_prix, article_taxe, order_id, total)
                VALUES ('$artId', '$artQuantity', '$artNom',
                        '$artPrix', '$artTaxe', '$orderId', '$artTotal')");
            if (!$querySetItem) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
                goto end;
            }
        }


    }
} else {
    $message['error'] = 'auth-token wrong signature';
}
end:
echo json_encode($message);
