<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;


$postdata = file_get_contents("php://input");
$data = json_decode($postdata);

$action = $data->action;

$message = array();

$jwtToken = $data->token;
if ($jwtToken == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$user = new User($jwtToken);

if ($user->isValid()) {
    $payload = $user->getPayload();
    $categorie = $payload['cat'];
    $idPro = null;
    if ($categorie == 'pro') {
        $idPro = $payload['user_id'];
    } elseif ($categorie == 'personnel') {
        $idPro = $payload['pro_id'];
    }

    if ($action == 'fetch-all') {
        $queryOrder = mysqli_query($_SESSION['connexion'],
            "SELECT * FROM `order` WHERE pro_id = '$idPro'");
        if (!$queryOrder) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        } else {
            $message['orders'] = array();
            $i = 0;
            while ($row = mysqli_fetch_assoc($queryOrder)) {
                foreach ($row as $key => $value) {
                    $message['orders'][$i][$key] = $value;
                }

                $orderId = $row['id'];

                $queryOrderItem = mysqli_query($_SESSION['connexion'],
                    "SELECT * FROM `order_item` WHERE order_id = '$orderId'");
                if (!$queryOrderItem) {
                    $message['error'] = mysqli_error($_SESSION['connexion']);
                } else {
                    $message['orders'][$i]['items'] = array();
                    $j = 0;
                    while ($row = mysqli_fetch_assoc($queryOrderItem)) {
                        foreach ($row as $key => $value) {
                            $message['orders'][$i]['items'][$j][$key] = $value;
                        }
                        $j++;
                    }
                }
                $i++;
            }
        }
    } else {
        $message['error'] = 'unknown action: ' . $action . '.';
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}
echo json_encode($message);
