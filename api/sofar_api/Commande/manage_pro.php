<?php

include "../connect.php";
require "../vendor/autoload.php";

use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;
use ReallySimpleJWT\Token;


$postdata = file_get_contents("php://input");
$data = json_decode($postdata);

$action = $data->action;

$message = array();

$jwtToken = $data->token;
if ($jwtToken == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$user = new User($jwtToken);

if ($user->isValid()) {
    $payload = $user->getPayload();
    $categorie = $payload['cat'];
    $idPro = null;
    if ($categorie == 'pro') {
        $idPro = $payload['user_id'];
    } elseif ($categorie == 'personnel') {
        $idPro = $payload['pro_id'];
    }

    Stripe::setApiKey($_SESSION['stripe']['secret_key']);

    $action = $data->action;

    $orderId = $data->order_id;

    if ($action == 'confirm') {
        /*
         * On récupère les infos de la commande:
         *  - l'id du user
         *  -
         */
        $queryGetOrder = mysqli_query($_SESSION['connexion'],
            "SELECT user_id, total, stripe_source FROM `order` WHERE id = '$orderId' and pro_id = '$idPro'");
        if (!$queryGetOrder) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }

        $orderRow = $queryGetOrder->fetch_row();
        $userID = $orderRow[0];
        $price = $orderRow[1];
        $stripeSource = $orderRow[2];

        /*
         * On récupère les infos du user qui fait la commande
         */
        $queryGetUser = mysqli_query($_SESSION['connexion'],
            "SELECT stripe_id FROM user WHERE id_u = '$userID'");
        if (!$queryGetUser) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
        $userRow = $queryGetUser->fetch_row();
        $stripeCustomerId = $userRow[0];

        $encouteredError = false;
        try {
            /**
             * On facture ici le client pour sa commande
             */
            $charge = Charge::create(array(
                'customer' => $stripeCustomerId,
                'amount' => intval(($price) * 100),
                'currency' => 'EUR'
            ));

            /*
             * On delete la source après facturation:
             * en réalité ici on 'oublie' la carte du client
             * On pourrait se souvenir de sa carte pour de futur payement, il faudra alors
             * prévenir et gérer ca dans le front
             */
            Customer::deleteSource(
                $stripeCustomerId,
                $stripeSource
            );
        } catch (\Stripe\Error\Base $e) {
            // Code to do something with the $e exception object when an error occurs
            $encouteredError = true;
            $message['error'] = $e->getMessage();
        } catch (Exception $e) {
            // Catch any other non-Stripe exceptions
            $encouteredError = true;
            $message['error'] = $e->getMessage();
        }

        if (!$encouteredError) {
            $queryUpdateOrder = mysqli_query($_SESSION['connexion'],
                "UPDATE `order` SET order_status = 'payed', stripe_source = '', updatedAt = now() WHERE id = '$orderId'");
            if (!$queryUpdateOrder) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        }
    } else if ($action == 'refuse') {
        // detach stripe source pour ne pas pouvoir facturer l'user plus tard
        $queryGetOrder = mysqli_query($_SESSION['connexion'],
            "SELECT stripe_source, stripe_id FROM `order`, `user` WHERE id = '$orderId' and user_id = id_u");
        if (!$queryGetOrder) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
        $orderRow = $queryGetOrder->fetch_row();
        $stripeSource = $orderRow[0];
        $stripeCustomerId = $orderRow[1];

        Customer::deleteSource(
            $stripeCustomerId,
            $stripeSource
        );

        $queryUpdateOrder = mysqli_query($_SESSION['connexion'],
            "UPDATE `order` SET order_status = 'refused', stripe_source = '', updatedAt = now() WHERE id = '$orderId'");
        if (!$queryUpdateOrder) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
    } else if ($action == 'ready' or $action == 'served') {
        $queryUpdateOrder = mysqli_query($_SESSION['connexion'],
            "UPDATE `order` SET order_status = '$action', updatedAt = now() WHERE id = '$orderId'");
        if (!$queryUpdateOrder) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
    } else {
        $message['error'] = 'unknown action: ' . $action . '.';
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}
echo json_encode($message);
