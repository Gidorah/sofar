<?php
include "../connect.php";
require "../vendor/autoload.php";

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';
$id_p=$data['id_p'];

$nom_criteres=["crit1","crit2","crit3"];

if($data['action'] === 'getnotes'){


    $queries = "";

    for ($i = 0; ; $i++) {
        if ($i > 2) {
            break;
        }
        $critere=$nom_criteres[$i];
        $queries .= "SELECT AVG($critere) FROM `notes` WHERE `id_p`='$id_p'; ";
        for ($j = 1; ; $j++) {
            if ($j > 5) {
                break;
            }
            $queries.= "SELECT COUNT($critere) FROM `notes` WHERE $critere=$j AND `id_p`='$id_p'; ";
        }
    }

    $resultat="[";

    if (mysqli_multi_query($_SESSION['connexion'], $queries)) {
        do {
            /* sStockage du premier résultat */
            if ($result = mysqli_store_result($_SESSION['connexion'])) {
                while ($row = mysqli_fetch_row($result)) {
                    $resultat.=$row[0];
                }
                mysqli_free_result($result);
            }
            /* Affichage d'une séparation */
            if (mysqli_more_results($_SESSION['connexion'])) {
                $resultat.=",";
            }
        } while (mysqli_next_result($_SESSION['connexion']));
    }

    $resultat.="]";

    echo json_encode($resultat);
}

else if ($data['action'] === 'getcommentaires'){
    $offset = $data['offset'];
    $query = mysqli_query($_SESSION['connexion'], "SELECT 
       `commentaire`, `nom_u`, `prenom_u`, (`crit1`+`crit2`+`crit3`)/3 FROM `notes` JOIN `user` ON notes.`id_u`=user.`id_u` WHERE `id_p`='$id_p' AND `commentaire`!='' ORDER BY `date` DESC LIMIT 3 OFFSET $offset");

    if (!$query) {
        $message['status'] = mysqli_error($_SESSION['connexion']);
        echo json_encode(mysqli_error($_SESSION['connexion']));
    }

    $result = mysqli_fetch_all($query);

    echo json_encode($result);
}

else if ($data['action'] === 'verify'){
    $id_u = $data['id_u'];
    $q = mysqli_query($_SESSION['connexion'], "SELECT `crit1`, `crit2`, `crit3`, `commentaire` FROM notes WHERE `id_p`='$id_p'AND `id_u`='$id_u'");
    $nbr=$q->num_rows;

    if ($nbr!=0) {
        $result = mysqli_fetch_all($q);
        echo json_encode($result);
    }
    else{
        $message['status'] = 'pas_encore_dans_db';
        echo json_encode($message);
    }
}
