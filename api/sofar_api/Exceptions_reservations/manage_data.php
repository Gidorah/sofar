<?php

include "../connect.php";

require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');

$data = json_decode($input, true);

$message = array();
$result = array();

$heure = $data['heure'];
$jour = $data['jour'];
$type = $data['type'];

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $id_pro = $payload['user_id'];
    if ($data['action'] == 'insert') {

        $query = mysqli_query($_SESSION['connexion'], "INSERT INTO `exceptions_reservations`( `id_pro`, `jour`, `heure`, `type`) 
                                 VALUES ('$id_pro', '$jour', '$heure', '$type')");

        $message['status'] = 'creneau_ajoute';
        if (!$query) {
            $message['status'] = 'erreur_ajout';
        }

        echo json_encode($message);

    }

    else if ($data['action'] == "delete") {

        $query = mysqli_query($_SESSION['connexion'], "DELETE FROM exceptions_reservations WHERE jour='$jour' and heure='$heure' and type='$type'");
        $message['status'] = 'creneau_retire';
        if (!$query) {
            $message['status'] = 'erreur_suppression';
        }

        echo json_encode($message);

    }

}

?>
