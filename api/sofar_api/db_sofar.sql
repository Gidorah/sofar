-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 22 juin 2021 à 12:08
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_sofar`
--
CREATE DATABASE IF NOT EXISTS `db_sofar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_sofar`;

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_nom` varchar(30) NOT NULL,
  `article_prix` int(10) NOT NULL,
  `article_prixHH` decimal(10,2) DEFAULT NULL,
  `article_desc` varchar(200) NOT NULL,
  `article_disp` int(10) NOT NULL,
  `article_cat` varchar(20) NOT NULL,
  `id_pro` int(11) NOT NULL,
  PRIMARY KEY (`article_id`),
  KEY `fk_id_pros` (`id_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`article_id`, `article_nom`, `article_prix`, `article_prixHH`, `article_desc`, `article_disp`, `article_cat`, `id_pro`) VALUES
(1, 'castel', 6, NULL, 'biere fruitee', 20, '', 1),
(2, 'blonde', 5, NULL, 'biere blonde', 20, '', 1),
(3, 'Binouzes', 6, '4.20', 'ca se boit', 0, ' Boisson ', 3),
(11, 'coca', 5, '0.00', '', 1, ' Boisson ', 3),
(12, 'sirop', 10, '5.00', 'suucre', 1, ' Boisson ', 3);

-- --------------------------------------------------------

--
-- Structure de la table `carte bancaire`
--

DROP TABLE IF EXISTS `carte bancaire`;
CREATE TABLE IF NOT EXISTS `carte bancaire` (
  `numero_carte_u` int(30) NOT NULL,
  `cv_carte_u` int(10) NOT NULL,
  `date_carte_u` varchar(20) NOT NULL,
  `id_u` int(11) NOT NULL,
  PRIMARY KEY (`numero_carte_u`),
  KEY `fk_id_u` (`id_u`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `command_id` int(11) NOT NULL AUTO_INCREMENT,
  `total_com` int(20) NOT NULL,
  `command_heure` varchar(20) NOT NULL,
  `id_u` int(11) NOT NULL,
  `id_pro` int(11) NOT NULL,
  PRIMARY KEY (`command_id`),
  KEY `frk_id_u` (`id_u`),
  KEY `frk_id_pro` (`id_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `détails commande`
--

DROP TABLE IF EXISTS `détails commande`;
CREATE TABLE IF NOT EXISTS `détails commande` (
  `id_article_com` int(11) NOT NULL AUTO_INCREMENT,
  `command_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `quantité` int(10) NOT NULL,
  PRIMARY KEY (`id_article_com`),
  KEY `fk_command_id` (`command_id`),
  KEY `fk_article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `exceptions_reservations`
--

DROP TABLE IF EXISTS `exceptions_reservations`;
CREATE TABLE IF NOT EXISTS `exceptions_reservations` (
  `id_pro` int(5) NOT NULL,
  `jour` varchar(10) NOT NULL,
  `heure` varchar(20) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `id_ex` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_ex`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `exceptions_reservations`
--

INSERT INTO `exceptions_reservations` (`id_pro`, `jour`, `heure`, `type`, `id_ex`) VALUES
(3, '2021-06-16', '14:00-15:00', 'retrait', 1),
(3, '2021-06-16', '18:00-20:00', 'retrait', 2),
(3, '2021-06-18', '15:00-20:00', 'retrait', 3),
(4, '2021-06-19', 'Journee entiere', 'retrait', 56),
(4, '2021-07-17', '', 'ajout', 51),
(4, '2021-08-13', '', 'ajout', 54),
(4, '2021-12-24', '', 'ajout', 53);

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

DROP TABLE IF EXISTS `favoris`;
CREATE TABLE IF NOT EXISTS `favoris` (
  `if_favoris` int(11) NOT NULL AUTO_INCREMENT,
  `nom_fav` varchar(50) NOT NULL,
  `id_u` int(11) NOT NULL,
  `id_pro` int(11) NOT NULL,
  PRIMARY KEY (`if_favoris`),
  KEY `fk_id_u_fav` (`id_u`),
  KEY `fk_id_pro_fav` (`id_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `favoris`
--

INSERT INTO `favoris` (`if_favoris`, `nom_fav`, `id_u`, `id_pro`) VALUES
(10, 'Pub Mac Carthy', 62, 3),
(13, 'La Taverne de L\'Irlandais', 62, 1);

-- --------------------------------------------------------

--
-- Structure de la table `horaires`
--

DROP TABLE IF EXISTS `horaires`;
CREATE TABLE IF NOT EXISTS `horaires` (
  `id_pro` int(5) NOT NULL,
  `lundi` varchar(11) DEFAULT NULL,
  `mardi` varchar(11) DEFAULT NULL,
  `mercredi` varchar(11) DEFAULT NULL,
  `jeudi` varchar(11) DEFAULT NULL,
  `vendredi` varchar(11) DEFAULT NULL,
  `samedi` varchar(11) DEFAULT NULL,
  `dimanche` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id_pro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `horaires`
--

INSERT INTO `horaires` (`id_pro`, `lundi`, `mardi`, `mercredi`, `jeudi`, `vendredi`, `samedi`, `dimanche`) VALUES
(1, '18:00-23:00', '18:00-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00'),
(3, '18:00-23:00', '18:00-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '08:00-14:00'),
(4, '18:00-23:00', '09:00-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00'),
(5, '18:00-23:00', '18:00-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00'),
(6, '18:00-23:00', '18:00-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00', '14:30-23:00');

-- --------------------------------------------------------

--
-- Structure de la table `pro`
--

DROP TABLE IF EXISTS `pro`;
CREATE TABLE IF NOT EXISTS `pro` (
  `id_pro` int(11) NOT NULL AUTO_INCREMENT,
  `nom_pro` varchar(100) NOT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `siret` varchar(30) NOT NULL,
  `email_pro` varchar(50) NOT NULL,
  `mdp_pro` varchar(60) NOT NULL,
  `photo_pro` varbinary(1000) DEFAULT NULL,
  `date_ouvert_pro` varchar(30) DEFAULT NULL,
  `date_happy_pro` varchar(30) DEFAULT NULL,
  `debut_happy_pro` varchar(20) DEFAULT NULL,
  `fin_happy_pro` varchar(20) DEFAULT NULL,
  `description_pro` varchar(1000) DEFAULT NULL,
  `categorie_pro` varchar(20) DEFAULT NULL,
  `terrasse_pro` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pro`
--

INSERT INTO `pro` (`id_pro`, `nom_pro`, `adresse`, `siret`, `email_pro`, `mdp_pro`, `photo_pro`, `date_ouvert_pro`, `date_happy_pro`, `debut_happy_pro`, `fin_happy_pro`, `description_pro`, `categorie_pro`, `terrasse_pro`) VALUES
(1, 'La Taverne de L\'Irlandais', '8 Rue Mazagran, 54000 Nancy', '', '', '', NULL, '9:56-18:27', '17:00-19:00', '12h', '12h', 'Bar avec retransmission de matchs de foot et rugby, billard et restauration (pates, viandes et poissons).', 'bar', 1),
(3, 'Pub Mac Carthy', '6 Rue Guerrier de Dumast, 54000 Nancy', '32', 'batboss325@gmail.com', '$2y$10$2mSdLtbLfxOL17aav1fk9uv97jzn1Y6liB1iYM3/p8f34HmPDQYai', NULL, '9:00-21:00', '18:00-20:00', '12h', '12h', 'Ambiance animee dans ce pub irlandais comportant des billards et un babyfoot et servant des plats classiques.', 'boite de nuit', 0),
(4, 'Le Pinocchio', '9 Place Saint-Epvre, 54000 Nancy', '', 'sese240798@gmail.com', '$2y$10$8umBePsDXJz4XPueGf2F1OJcpebwsp1/TSyxz.7.DlUSjlUkfteW2', NULL, '8:00-2:00', '17:00-19:00', NULL, NULL, 'Super bar place st-epvre', 'restaurant', 1),
(5, 'Le Varadero', '27 Grande Rue, 54000 Nancy', '', '', '', NULL, '18:00-2:00', '18:00-20:00', NULL, NULL, 'Certainement un des bars les plus animes de Nancy !', 'bar', 0),
(6, 'Phi Sciences', 'rue du jardin botanique, 54600 Vandoeuvre-les-Nancy', '', '', '', NULL, '18:00-2:00', '18:00-20:00', NULL, NULL, 'Super bar de la fac de sciences', 'bar', 0);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_article_res` int(20) NOT NULL,
  `date_res` varchar(20) NOT NULL,
  `heure_res` varchar(20) NOT NULL,
  `nb_perso_res` int(10) NOT NULL,
  `validation_res` int(5) NOT NULL,
  `etat_res` varchar(100) NOT NULL,
  `id_pro` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `emplacement` varchar(20) NOT NULL,
  `consigne` varchar(100) NOT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `fk_id_pro` (`id_pro`),
  KEY `fk_id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`reservation_id`, `id_article_res`, `date_res`, `heure_res`, `nb_perso_res`, `validation_res`, `etat_res`, `id_pro`, `id_user`, `emplacement`, `consigne`) VALUES
(1, 0, '2021-06-11', '11:24:29', 1, 0, 'A venir', 3, 62, 'Interieur', ''),
(2, 0, '2021-06-11', '1125', 5, 0, 'A venir', 3, 62, 'Interieur', ''),
(3, 0, '2021-06-11', '12:08', 5, 1, 'PassÃ©e', 4, 62, 'Terrasse', 'Super resa aaaa'),
(4, 0, '2021-06-11', '12:08', 5, 0, 'PassÃ©e', 4, 62, 'Terrasse', 'Super resa aaaa'),
(13, 0, '2021-06-17', '14:30', 5, 0, 'A venir', 3, 62, 'Interieur', 'undefined'),
(15, 0, '2021-06-18', '16:00', 4, 1, 'A venir', 1, 72, 'Interieur', 'binouze au frais'),
(16, 0, '2021-06-19', '20:30', 5, 0, 'PassÃ©e', 6, 72, 'Interieur', 'undefined'),
(17, 0, '2021-06-18', '20:30', 10, 0, 'PassÃ©e', 3, 72, 'Interieur', ':)'),
(18, 0, '2021-06-24', '23:30', 1, 2, 'refus', 3, 72, 'Interieur', ':('),
(19, 0, '2021-06-18', '23:00', 4, 1, 'A venir', 3, 72, 'Interieur', 'undefined'),
(20, 0, '2021-06-30', '15:00', 4, 2, 'refut', 3, 72, 'Interieur', 'sdfsd'),
(21, 0, '2021-06-18', '15:00', 4, 0, 'A venir', 6, 72, 'Interieur', 'undefined'),
(22, 0, '2021-06-21', '18:45', 5, 1, 'PassÃ©e', 3, 72, 'Interieur', 'undefined'),
(23, 0, '2021-06-21', '20:00', 5, 1, 'A venir', 3, 72, 'Interieur', 'undefined'),
(24, 0, '2021-06-21', '18:00', 5, 1, 'A venir', 3, 72, 'Interieur', 'undefined'),
(25, 0, '2021-06-21', '19:00', 3, 1, 'A venir', 3, 72, 'Interieur', 'undefined'),
(26, 0, '2021-06-21', '23:00', 4, 0, 'A venir', 3, 72, 'Interieur', 'undefined'),
(27, 0, '2021-06-21', '20:00', 3, 0, 'A venir', 3, 72, 'Interieur', '');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_u` int(11) NOT NULL AUTO_INCREMENT,
  `mdp_u` varchar(60) NOT NULL,
  `nom_u` varchar(100) NOT NULL,
  `prenom_u` varchar(100) NOT NULL,
  `date_naissance_u` varchar(20) NOT NULL,
  `email_u` varchar(200) NOT NULL,
  `sexe_u` varchar(5) NOT NULL,
  `date_inscr` varchar(12) NOT NULL,
  `photo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_u`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_u`, `mdp_u`, `nom_u`, `prenom_u`, `date_naissance_u`, `email_u`, `sexe_u`, `date_inscr`, `photo`) VALUES
(55, 'mdp', 'sese', 'sese', '24/07/1998', 'mail', '', '', NULL),
(58, '$2y$10$F9r7Yw/Eyuftizo51swFvObmZixovktQQZzhNeU/vT53gHkk7uvim', 'a', 'aaa', '2002-06-07', 'aze@aze.fr', 'homme', '', NULL),
(59, '$2y$10$07692LKnQlJed0rmU7DyMO.Dn6QS5YAfF1RO.G.L.8L8H/3i3ehjC', 'a', 'aaa', '2002-06-07', 'aze@aze.fra', 'homme', '', NULL),
(60, '$2y$10$liRzFofParW5j3VL9UK.ee1.6y8Ovp.RTzf8N2rSAf6FHbkUDKsJ6', 'aa', 'aa', '2002-06-07', 'aa@a.fr', 'homme', '', NULL),
(61, '$2y$10$8OOJTpq5ibOkjXztg0DUjORQerOon6/h.Sn.PVgPQuO3FVqhIgjFu', 'aa', 'aa', '2002-06-07', 'aaa@a.fr', 'homme', '', NULL),
(62, '$2y$10$8umBePsDXJz4XPueGf2F1OJcpebwsp1/TSyxz.7.DlUSjlUkfteW2', 'BRIGATTI', 'Sebastien', '2002-06-07', 'sese240798@gmail.com', 'homme', '', NULL),
(63, '$2y$10$zYam5lMsWsFZLBSA2aslxeEj4cVf1la3WW9sThnzCGz4.amERSEDO', 'test', 'abbb', '1972-06-09', 'test@test.fr', 'homme', '2021/06/09', NULL),
(64, '$2y$10$Ht.UCpKyghybJB95W81TFOMlwA7FieR5124zetRBQQzTEC7FMWepy', 'Boby', 'Test', '1991-06-09', 'a@z.fr', 'femme', '2021/06/09', NULL),
(65, '$2y$10$pVtNovUiK3taJGdaZ93VTuTiFn3zlBk9AACON52e6zGsQTLYjSyqu', 'Boby', 'Test', '1991-06-09', 'aa@z.fr', 'femme', '2021/06/09', NULL),
(66, '$2y$10$2l6ZIyd9bCLGbAL1gf9vW.ODeFosdTBW0My6YYL3HW3ZZLPMyEYde', 'aazazea', 'zeaze', '1997-06-09', 'aa@aa.fr', 'homme', '2021/06/09', NULL),
(67, '$2y$10$kpUuie5RTwo90r4UQ25v/OtcnTlMPArR.MuxSP6Vuvz3xSBeUN0ni', 'ezae', 'za', '1999-06-09', 'aaaa@a.fr', 'homme', '2021/06/09', NULL),
(68, '$2y$10$VGbyAsTCnFvqz3CwvHL5B.nBmIbVbFBXo45GLDNIAOtmYKjaTZRrO', 'new', 'bonhomme', '1999-06-09', 'b@b.fr', 'autre', '2021/06/09', NULL),
(69, '$2y$10$mhK9fbu5FcRqq42TgoCw8OrOt0pXG20xi1bb4JqTIUCqS8R5fWo4e', 'asda', 'azdazd', '2001-06-09', 'azdazd@ff.fr', 'homme', '2021/06/09', NULL),
(70, '$2y$10$MNjkvDNq9fLSpcDwp3DdkeB/oEWgZcbjtTTlhJadEaYw2H5cOQ2Ga', 'asda', 'azdazd', '2001-06-09', 'azdazd@ff.frd', 'homme', '2021/06/09', NULL),
(71, '$2y$10$7W1CHD6B1WDpxkigwJfrIOJOsa6eePLKCAiuSDEYRMQ3rVDmdHtQO', 'asda', 'azdazd', '2001-06-09', 'azdazd@ff.com', 'homme', '2021/06/09', NULL),
(72, '$2y$10$2o1DzARVtIABcGcMrRTWbOhr0eIkmuJuiyA8Gwyiq5cHUxdKfwA0C', 'Requet', 'Bats', '2002-06-07', 'batboss325@gmail.com', 'homme', '2021/06/09', NULL),
(73, '$2y$10$sEC/jQ.vJn9tRBNkSPAp/efYMEqypg.hEHTZqmhh2ai2eWkRl.Tp6', 'name', 'firstname', '2001-06-09', 'new@mail.fr', 'homme', '2021/06/09', NULL),
(74, '$2y$10$SjU97t.qQP5HhGWsoPz0ReM3IgBSZJovnKoIX7ge7DbfYhaYOS5jS', 'yahou', 'adadad', '2002-06-09', 'tesazet@test.fr', 'homme', '2021/06/09', NULL),
(75, '$2y$10$wtkTGz0YoV6ytMTC16ZpJu0mNNa3NDzkvDNVCeSCMiVMeS83EI5cG', 'sd', 'sdfsdff', '2002-06-10', 'sdfsdfsdf@dfsd.gt', 'homme', '2021/06/10', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_id_pros` FOREIGN KEY (`id_pro`) REFERENCES `pro` (`id_pro`);

--
-- Contraintes pour la table `carte bancaire`
--
ALTER TABLE `carte bancaire`
  ADD CONSTRAINT `fk_id_u` FOREIGN KEY (`id_u`) REFERENCES `user` (`id_u`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `frk_id_pro` FOREIGN KEY (`id_pro`) REFERENCES `pro` (`id_pro`),
  ADD CONSTRAINT `frk_id_u` FOREIGN KEY (`id_u`) REFERENCES `user` (`id_u`);

--
-- Contraintes pour la table `détails commande`
--
ALTER TABLE `détails commande`
  ADD CONSTRAINT `fk_article_id` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`),
  ADD CONSTRAINT `fk_command_id` FOREIGN KEY (`command_id`) REFERENCES `commande` (`command_id`);

--
-- Contraintes pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD CONSTRAINT `fk_id_pro_fav` FOREIGN KEY (`id_pro`) REFERENCES `pro` (`id_pro`),
  ADD CONSTRAINT `fk_id_u_fav` FOREIGN KEY (`id_u`) REFERENCES `user` (`id_u`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_id_pro` FOREIGN KEY (`id_pro`) REFERENCES `pro` (`id_pro`),
  ADD CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_u`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
