<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $data['id'];
    $categorie = $data['categorie'];

    $query = mysqli_query($_SESSION['connexion'], "SELECT 
       `token` FROM `devices` WHERE `id_u`='$idu' AND `categorie`='$categorie'");

    $message['status'] = "devices_recuperes";

    if (!$query) {
        $message['status'] = mysqli_error($_SESSION['connexion']);
    }

    $result = mysqli_fetch_all($query);

    $message['result'] = $result;
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);


