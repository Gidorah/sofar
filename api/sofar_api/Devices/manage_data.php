<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $payload['user_id'];
    $token_device = $data['token_device'];
    $categorie = $data['categorie'];

    if ($data['action'] === 'insert') {

        $q = mysqli_query($_SESSION['connexion'], "SELECT * FROM devices WHERE `token`='$token_device'AND `id_u`='$idu' AND `categorie`='$categorie'");
        $nbr = $q->num_rows;

        if ($nbr != 0) {
            $message['status'] = 'already_in_db';
        } else {
            $query = mysqli_query($_SESSION['connexion'], "INSERT INTO `devices`( `token`, `id_u`, `categorie`) VALUES ('$token_device', '$idu', '$categorie')");
            $message['status'] = 'device_ajoute';
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
                $message['status'] = 'erreur_ajout';
            }
        }
    }

    if ($data['action'] === 'delete') {

        $query = mysqli_query($_SESSION['connexion'], "DELETE FROM `devices`
                                    WHERE `token`='$token_device'AND `id_u`='$idu' AND `categorie`='$categorie'");

        $message['status'] = 'device_retire';
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
            $message['status'] = 'erreur_delete';
        }
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);
