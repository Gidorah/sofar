<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
// Check for token validation
if ($validate) {
    $id = $payload['user_id'];

    $password = $data['password'];
    $newPassword = $data['new_password'];

    // Check taille du nouveau mdp
    if (strlen($newPassword) < 6) {
        $message['error'] = 'too-short';
    } else {
        $q = mysqli_query($_SESSION['connexion'], "SELECT mdp_u, email_u, prenom_u FROM user WHERE id_u=$id");

        if (!$q) {
            $message['error'] = '$q : ' . mysqli_error($_SESSION['connexion']);
        } else {
            // Check si le mdp actuel est bon

            $resultQ = mysqli_fetch_row($q);
            if (password_verify($password, $resultQ[0])) {
                // Tout est ok on peut update le pw
                $newPasswordEncrypted = password_hash($newPassword, PASSWORD_DEFAULT);

                $email_addr = $resultQ[1];
                $prenom = $resultQ[2];

                $queryUpdate = mysqli_query($_SESSION['connexion'], "UPDATE user SET mdp_u = '$newPasswordEncrypted' WHERE id_u=$id");
                if (!$queryUpdate) {
                    $message['error'] = '$queryUpdate : ' . mysqli_error($_SESSION['connexion']);
                } else {
                    // Tout est bon, on envoie un mail pour confirmer le changement de mdp
                    $mail = new PHPMailer();

                    // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                    $mail->isSMTP();
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                    $mail->Port = 587;
                    $mail->CharSet = 'UTF-8';

                    $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                    $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                    // Sender and recipient settings
                    $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                    $mail->addAddress($email_addr, $prenom);
//                    $mail->addReplyTo('sofar.api@gmail.com', 'SoFAR'); // to set the reply to

                    // Setting the email content
                    $mail->IsHTML(true);
                    $mail->Subject = "SoFAR: Changement de mot de passe";

                    ob_start();
                    include('./mail_template/pw_change/pw_change_html.tpl');
                    $mail->Body = ob_get_clean();
                    include('./mail_template/pw_change/pw_change_plain.tpl');
                    $mail->AltBody = ob_get_clean();


                    $result['send_status'] = $mail->send();

                }

            } else {
                $message['error'] = 'wrong-pw';
            }
        }
    }

} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);