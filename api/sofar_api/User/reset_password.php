<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
include "../connect.php";


require "../vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;


$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$result = array();

$q = mysqli_query($_SESSION['connexion'], "SELECT * FROM `user` WHERE `email_u`='$data' ");
if ($q) {
    $check = mysqli_num_rows($q);
    if ($check === 1) {

        // password generation
        $factory = new RandomLib\Factory;
        $generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
        $new_password = $generator->generateString(8);

        $new_password_encrypted = password_hash($new_password, PASSWORD_DEFAULT);
        //echo json_encode($new_password_encrypted);
        $query = mysqli_query($_SESSION['connexion'], "UPDATE user SET mdp_u='$new_password_encrypted' WHERE email_u='$data'");
        if ($query) {
            $query2 = mysqli_query($_SESSION['connexion'], "SELECT prenom_u, nom_u  FROM user WHERE email_u='$data' ");
            if ($query2) {
                $result = mysqli_fetch_all($query2);
                try {
                    $mail = new PHPMailer();

                    // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                    $mail->isSMTP();
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                    $mail->Port = 587;
                    $mail->CharSet = 'UTF-8';

                    $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                    $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                    // Sender and recipient settings
                    $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                    $mail->addAddress($data, $result[0][0]);

                    /* Set the subject. */
                    $mail->Subject = 'SoFAR: Reconfiguration de ton mot de passe';
                    $mail->isHTML(TRUE);

                    $name = $result[0][0];

                    ob_start();
                    include('./mail_template/pw_reset/pw_reset_html.tpl');
                    $mail->Body = ob_get_clean();
                    include('./mail_template/pw_reset/pw_reset_plain.tpl');
                    $mail->AltBody = ob_get_clean();

                    /* Finally send the mail. */
                    $message['send_status'] = $mail->send();
                } catch (Exception $e) {
                    /* PHPMailer exception. */
                    $e->errorMessage();
                    $message['erreur envoi'] = $e;
                } catch (\Exception $e) {
                    /* PHP exception (note the backslash to select the global namespace Exception class). */
                    $e->getMessage();
                    $message['exception'] = $e;
                }
            } else {
                $message['query2'] = 'error';
            }

        } else {
            $message['query'] = 'error';
            echo json_encode(mysqli_error($_SESSION['connexion']));
        }

    } else {
        $message['check'] = $check;
    }

} else {
    $message['q'] = 'error';
}

echo json_encode($message);


?>