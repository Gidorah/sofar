<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$result = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
if ($validate) {
    try {
        $id = $payload['user_id'];
        $image = $data['image'];

        $factory = new RandomLib\Factory;
        $generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
        $name = '';

        // Instantiate the S3 class and point it at the desired host
        $s3Client = S3Client::factory(array('credentials' => [
            'key' => AWS_KEY,
            'secret' => AWS_SECRET_KEY
        ],
            'region' => 'eu-west-3',
            'version' => 'latest'
        ));
        $s3Client->registerStreamWrapper();
        $baseURL = 's3://' . BUCKET_NAME;

        /*
         * On supprime l'ancienne image de l'user dans la banque d'image
         */
        $queryImage = mysqli_query($_SESSION['connexion'], "SELECT photo FROM user WHERE id_u=$id");
        if (!$queryImage) {
            $message['error'] = '$queryImage : ' . mysqli_error($_SESSION['connexion']);
        } else {
            $resultFormerPhoto = mysqli_fetch_row($queryImage);
            $formerPhoto = $resultFormerPhoto[0];
            if (file_exists($baseURL . '/profile_pictures/user/' . $formerPhoto)) {
                unlink($baseURL . '/profile_pictures/user/' . $formerPhoto);
            }
        }

        $exist = true;

        /*
         * Génération d'un nom unique pour l'image
         */
        while ($exist) {
            $name = $generator->generateString(10, '0123456789') . '.jpeg';
            $exist = file_exists($baseURL . '/profile_pictures/user/' . $name);
        }

        /*
         * On update le nom de l'image de l'user
         */
        $queryUpdate = mysqli_query($_SESSION['connexion'], "UPDATE user SET photo = '$name' WHERE id_u=$id");
        if (!$queryUpdate) {
            $message['error'] = '$queryUpdate : ' . mysqli_error($_SESSION['connexion']);
        } else {
            file_put_contents($baseURL . '/profile_pictures/user/' . $name, base64_decode($image));
            $message['status'] = 'success';
        }
    } catch (S3Exception $e) {
        echo $e->getMessage() . "\n";
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);