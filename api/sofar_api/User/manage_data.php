<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");


include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$result = array();


if ($data['action'] == "insert") {
    $mdp = $data['mdp'];
    $password_encrypted = password_hash($mdp, PASSWORD_DEFAULT);
//    $password_encrypted = $data['mdp'];
//    $password_encrypted=password_hash($mdp,PASSWORD_DEFAULT);
//    $password_encrypted = $data['mdp'];
    $email = $data['email'];
    $nom = addslashes($data['nom']);
    $prenom = addslashes($data['prenom']);
    $date_de_naissance = $data['date_de_naissance'];
    $sexe = $data['sexe'];
    $date_inscr = $data['inscription'];

    $query = mysqli_query($_SESSION['connexion'], "SELECT * FROM user WHERE email_u='$email' ");
    if ($query) {
        $check = mysqli_num_rows($query);
        if ($check !== 0) {
            $message['status'] = "existing-account";
            echo json_encode($message);
        } else {
            $q = mysqli_query($_SESSION['connexion'], "INSERT INTO `user`( `mdp_u`, `nom_u`, `prenom_u`, `date_naissance_u`, `email_u`, `sexe_u`, date_inscr) VALUES ( '$password_encrypted', '$nom', '$prenom', '$date_de_naissance','$email','$sexe', '$date_inscr')");
            $result['error'] = mysqli_error($_SESSION['connexion']);
            if ($q) {
                $result['status'] = "success";

                $mail = new PHPMailer();

                // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->Port = 587;
                $mail->CharSet = 'UTF-8';

                $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                // Sender and recipient settings
                $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                $mail->addAddress($email, $nom);
//                $mail->addReplyTo('sofar.api@gmail.com', 'SoFAR'); // to set the reply to

                // Setting the email content
                $mail->IsHTML(true);
                $mail->Subject = "Bienvenue chez SoFAR";

                ob_start();
                include('./mail_template/inscription/inscription_html.tpl');
                $mail->Body = ob_get_clean();
                include('./mail_template/inscription/inscription_plain.tpl');
                $mail->AltBody = ob_get_clean();

                $result['send_status'] = $mail->send();
                echo json_encode($result);
            } else {
                $result['status'] = "error";
                echo json_encode($result);
            }
        }
    } else {
        $message['query'] = "error";
    }
} else if ($data['action'] == "login") {


    $email = $data['email'];
    $mdp = $data['password'];

    $result['error'] = '';

    $q = mysqli_query($_SESSION['connexion'], "SELECT id_u, mdp_u, nom_u, prenom_u, date_naissance_u, email_u, sexe_u, date_inscr FROM `user` WHERE email_u='$email'");
    if (!$q) {
        $result['error'] = 'Impossible d\'exécuter la requête : ' . mysqli_error($_SESSION['connexion']);
    } else {
        $check = mysqli_num_rows($q);

        if ($check === 1) {
            $resultQ = mysqli_fetch_row($q);

            if (password_verify($mdp, $resultQ[1])) {

                $expiration = time() + 365 * 60 * 24 * 60;
                $issuer = 'localhost';

                $payload = [
                    'iat' => time(),
                    'user_id' => $resultQ[0],
                    'cat' => 'user',
                    'exp' => $expiration,
                    'iss' => $issuer
                ];

                $token = Token::customPayload($payload, $_SESSION['secret']);

                $result['user'] = array();
                $result['user']['id'] = $resultQ[0];
                $result['user']['nom'] = $resultQ[2];
                $result['user']['prenom'] = $resultQ[3];
                $result['user']['date_naissance'] = $resultQ[4];
                $result['user']['email'] = $resultQ[5];
                $result['user']['sexe'] = $resultQ[6];
                $result['date_inscr']['sexe'] = $resultQ[7];
                $result['user']['access_token'] = $token;
            } else {
                $result['error'] = 'email or password are wrong';
            }
        } else {
            $result['error'] = 'email or password are wrong';
        }
    }
    echo json_encode($result);

} else if ($data['action'] == "fetch") {


    $token = $data['token'];
    if ($token == null) {
        $message['error'] = 'no token provided';
        echo json_encode($message);
        exit();
    }

    $result['error'] = '';

    $pers = new User($token);

    if ($pers->isValid()) {
        $id = $pers->getPayload()['user_id'];
        $q = mysqli_query($_SESSION['connexion'], "SELECT id_u, nom_u, prenom_u, date_naissance_u, email_u, sexe_u, date_inscr, photo FROM `user` WHERE id_u='$id'");
        if (!$q) {
            $result['error'] = 'Impossible d\'exécuter la requête : ' . mysqli_error($_SESSION['connexion']);
        } else {
            $check = mysqli_num_rows($q);

            if ($check === 1) {
                $resultQ = mysqli_fetch_row($q);

                $result['user'] = array();
                $result['user']['id'] = $resultQ[0];
                $result['user']['nom'] = $resultQ[1];
                $result['user']['prenom'] = $resultQ[2];
                $result['user']['date_naissance'] = $resultQ[3];
                $result['user']['email'] = $resultQ[4];
                $result['user']['sexe'] = $resultQ[5];
                $result['user']['date_inscr'] = $resultQ[6];
                $result['user']['photo'] = '';

                /*
                 * fetch photo
                 */
                // Instantiate the S3 class and point it at the desired host
                $s3Client = S3Client::factory(array('credentials' => [
                    'key' => AWS_KEY,
                    'secret' => AWS_SECRET_KEY
                ],
                    'region' => 'eu-west-3',
                    'version' => 'latest'
                ));
                $s3Client->registerStreamWrapper();
                $baseURL = 's3://' . BUCKET_NAME;

                $photoPath = $baseURL . '/profile_pictures/user/' . $resultQ[7];
                if (file_exists($photoPath) && $resultQ[7] != '') {
                    $result['user']['photo'] = base64_encode(file_get_contents($photoPath));
                }


            } else {
                $result['error'] = 'more than one row for this id';
            }
        }
    } else {
        $result['error'] = 'authentification token is wrong';
    }

    echo json_encode($result);

}

?>
