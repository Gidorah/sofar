<?php

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$result = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
if ($validate) {

    $id = $payload['user_id'];

    $nom = addslashes($data['nom']);
    $prenom = addslashes($data['prenom']);
    $email = $data['email'];
    $sexe = $data['sexe'];

    // On récupère l'adresse email actuelle
    $queryFormerMail = mysqli_query($_SESSION['connexion'], "SELECT email_u FROM user WHERE id_u='$id'");
    if (!$queryFormerMail) {
        $message['sql'] = mysqli_error($_SESSION['connexion']);
    } else {
        $resultFormerMail = mysqli_fetch_row($queryFormerMail);
        $formerMail = $resultFormerMail[0];


        /*
         * On regarde si le mail a été changé
         * Si oui, il faut vérifier que le nouveau mail n'est pas déjà utilisé
         * Sinon, c'est ok
         */
        if (strcmp($email, $formerMail) == 0) {
            // Le mail n'a pas été changé

            $queryUpdate = mysqli_query($_SESSION['connexion'], "UPDATE user SET nom_u = '$nom', prenom_u = '$prenom', sexe_u = '$sexe' WHERE id_u='$id'");
            if (!$queryUpdate) {
                $message['sql'] = mysqli_error($_SESSION['connexion']);
            }

        } else {
            // Le mail a  été changé

            // On récupère les lignes comportant le mail actuel
            $queryMail = mysqli_query($_SESSION['connexion'], "SELECT * FROM user WHERE email_u='$email'");
            if (!$queryMail) {
                $message['sql'] = mysqli_error($_SESSION['connexion']);
            } else {
                $numberOfRowMail = mysqli_num_rows($queryMail);

                if ($numberOfRowMail != 0) {
                    // le mail n'est pas valide
                    $message['error'] = 'mail-exist';
                } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    // l'adresse email est invalide
                    $message['error'] = 'mail-invalid';
                } else {
                    // Le mail est valide

                    // On update l'user
                    $queryUpdate = mysqli_query($_SESSION['connexion'], "UPDATE user SET nom_u = '$nom', prenom_u = '$prenom', email_u = '$email', sexe_u = '$sexe' WHERE id_u='$id'");
                    if (!$queryUpdate) {
                        $message['sql'] = mysqli_error($_SESSION['connexion']);
                    } else {
                        // Le mail a été changé et est valide, on envoie un mail au nouveau mail

                        $mail = new PHPMailer();

                        // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                        $mail->isSMTP();
                        $mail->Host = 'smtp.gmail.com';
                        $mail->SMTPAuth = true;
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                        $mail->Port = 587;
                        $mail->CharSet = 'UTF-8';

                        $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                        $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                        // Sender and recipient settings
                        $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                        $mail->addAddress($email, $prenom);
//                        $mail->addReplyTo('sofar.api@gmail.com', 'SoFAR'); // to set the reply to

                        // Setting the email content
                        $mail->IsHTML(true);
                        $mail->Subject = "SoFAR: Changement de Mail";

                        ob_start();
                        include('./mail_template/mail_change/mail_change_html.tpl');
                        $mail->Body = ob_get_clean();
                        include('./mail_template/mail_change/mail_change_plain.tpl');
                        $mail->AltBody = ob_get_clean();

                        $result['send_status'] = $mail->send();
                    }
                }
            }
        }
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);