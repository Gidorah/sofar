<?php
ini_set('max_execution_time', 0);
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

ini_set('max_execution_time', 0);
$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
// Check for token validation
if ($validate) {
    $idPro = $payload['user_id'];

    $action = $data['action'];
    if ($action == 'create') {

        $factory = new RandomLib\Factory;
        $generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::MEDIUM));
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // Action de création d'un serveur/personnel

        // Création d'un login aléatoire pour le serveur
        $nameExist = true;
        $login = '';
        $i = 0;
        while ($nameExist) {
            $login = $generator->generateString(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $queryName = mysqli_query($_SESSION['connexion'], "SELECT * FROM personnel WHERE login = '$login'");
            if (!$queryName) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
                echo json_encode($message);
                return;
            }
            $nameExist = $queryName->num_rows == 0 ? false : true;
        }

        // Création d'un mdp
        $pw = $data['pw'];
        if ($pw == '') {
            // pas de mdp -> il faut en générer un
            $pw = $generator->generateString(6, $characters);
        }
        $password_encrypted = password_hash($pw, PASSWORD_DEFAULT);

        $alias = addslashes($data['alias']);

        $dateCrea = date('Y-m-d');

        $insertQuery = mysqli_query($_SESSION['connexion'],
            "INSERT INTO personnel (id_pro, login, mdp_per, alias, date_creation) VALUES ('$idPro', '$login', '$password_encrypted', '$alias', '$dateCrea')");
        if (!$insertQuery) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
            echo json_encode($message);
            return;
        }

        $message['result']['login'] = $login;
        $message['result']['password'] = $pw;

    } else if ($action == 'fetch-all') {
        // Fetch l'ensemble du personnel
        $query = mysqli_query($_SESSION['connexion'], "SELECT id_per, login, alias FROM personnel WHERE id_pro='$idPro'");
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        } else {
            $message['result'] = array();
            $i = 0;
            while ($row = mysqli_fetch_assoc($query)) {
                foreach ($row as $key => $value) {
                    $message['result'][$i][$key] = $value;
                }
                $i++;
            }
        }

    } else if ($action == 'fetch') {
        // Fetch un membre
        $idPer = $data['id_per'];
        $query = mysqli_query($_SESSION['connexion'], "SELECT id_per, login, alias FROM personnel WHERE id_pro='$idPro' and id_per = '$idPer'");
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        } else {
            $message['result'] = array();
            $row = mysqli_fetch_assoc($query);
            foreach ($row as $key => $value) {
                $message['result'][$key] = $value;
            }
        }

    } else if ($action == 'change-name') {
        // change l'alias d'un membre
        $idPer = $data['id_per'];
        $alias = addslashes($data['alias']);
        if (strlen($alias) == 0) {
            $message['error'] = 'new alias too short';
        } else {
            $query = mysqli_query($_SESSION['connexion'], "UPDATE personnel SET alias = '$alias' WHERE id_pro='$idPro' and id_per = '$idPer'");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        }

    } else if ($action == 'change-password') {
        // change le mdp d'un membre
        $idPer = $data['id_per'];
        $password = $data['pw'];
        if (strlen($password) < 6) {
            $message['error'] = 'new password too short';
        } else {
            $password_encrypted = password_hash($password, PASSWORD_DEFAULT);
            $query = mysqli_query($_SESSION['connexion'], "UPDATE personnel SET mdp_per = '$password_encrypted' WHERE id_pro='$idPro' and id_per = '$idPer'");
            if (!$query) {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
        }
    } else if ($action == 'delete') {
        // change le mdp d'un membre
        $idPer = $data['id_per'];
        $query = mysqli_query($_SESSION['connexion'], "DELETE FROM personnel WHERE id_pro='$idPro' and id_per = '$idPer'");
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }

    } else {
        // Si action non reconnu -> erreur
        $message['error'] = 'unknown action : ' . $action;
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}
echo json_encode($message);
