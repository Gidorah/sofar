<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
// Check for token validation
if ($validate) {
    $id = $payload['user_id'];
    $desc =addslashes($data['desc']);

    $query = mysqli_query($_SESSION['connexion'], "UPDATE `pro` SET
                    `description_pro`='$desc'
                    WHERE `id_pro`='$id'");
    if (!$query) {
        $message['status'] = json_encode(mysqli_error($_SESSION['connexion']));
    }
    $message['desc'] = $desc;
} else {
    $message['error'] = 'auth-token wrong signature';
}
echo json_encode($message);
?>
