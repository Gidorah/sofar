Bonjour <?= $name ?>,

Vous avez demandé à réinitialiser votre mot de passe.
Votre mot de passe provisoire est <?= $new_password ?>.
Vous pourrez le modifier lors de votre prochaine connexion.