<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
include "../connect.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once '../vendor/phpmailer/phpmailer/src/Exception.php';
require_once '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once '../vendor/phpmailer/phpmailer/src/SMTP.php';

$input = file_get_contents('php://input');

$data = json_decode($input, true);

$message = array();
$result = array();

$mail = new PHPMailer(TRUE);
$mail->CharSet = 'UTF-8';

$q = mysqli_query($_SESSION['connexion'], "SELECT * FROM `pro` WHERE `email_pro`='$data' ");
if ($q) {
    $result1 = mysqli_fetch_all($q);
    $check = mysqli_num_rows($q);
    $checkIfFirstCo = $result1[0][5]; // chercher la colonne mdp
    if ($check === 1 and $checkIfFirstCo === '') {
        $message['first-co'] = 'true';
        $new_password = rand(100000, 999999);
        $new_password_encrypted = password_hash($new_password, PASSWORD_DEFAULT);
        $message['pw'] = $new_password;
        $message['pwe'] = $new_password_encrypted;

        //echo json_encode($new_password_encrypted);
        $query = mysqli_query($_SESSION['connexion'], "UPDATE pro SET mdp_pro='$new_password_encrypted' WHERE email_pro='$data'");
        if ($query) {
            $query2 = mysqli_query($_SESSION['connexion'], "SELECT nom_pro FROM pro WHERE email_pro='$data' ");
            if ($query2) {
                $result = mysqli_fetch_all($query2);
                try {
                    // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                    $mail->isSMTP();
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                    $mail->Port = 587;
                    $mail->CharSet = 'UTF-8';

                    $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                    $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                    // Sender and recipient settings
                    $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                    $mail->addAddress($data, 'Receiver Name');
//                    $mail->addReplyTo('sofar.api@gmail.com', 'SoFAR'); // to set the reply to

                    // Setting the email content
                    $mail->IsHTML(true);
                    $mail->Subject = "SoFAR: Bienvenue !";

                    $name = $result[0][0];

                    ob_start();
                    include('./mail_template/first_co/first_co_html.tpl');
                    $mail->Body = ob_get_clean();
                    include('./mail_template/first_co/first_co_plain.tpl');
                    $mail->AltBody = ob_get_clean();

//                        /* Disable some SSL checks. */
//                        $mail->SMTPOptions = array(
//                            'ssl' => array(
//                            'verify_peer' => false,
//                            'verify_peer_name' => false,
//                            'allow_self_signed' => true
//                            )
//                        );


                    /* Finally send the mail. */
                    $message['send status'] = $mail->send();
                } catch (Exception $e) {
                    /* PHPMailer exception. */
                    $e->errorMessage();
                    $message['erreur envoi'] = $e;
                } catch (\Exception $e) {
                    /* PHP exception (note the backslash to select the global namespace Exception class). */
                    $e->getMessage();
                    $message['exception'] = $e;
                }
            } else {
                $message['query2'] = 'error';
            }

        } else {
            $message['query'] = 'error';
            echo json_encode(mysqli_error($_SESSION['connexion']));
        }

    } else {
        $message['check'] = $check;
        if ($check === 0) {
            $message['status'] = 'no-mail-exist';
        }
        if ($checkIfFirstCo !== '') {
            $message['first-co'] = 'false';
        }
    }

} else {
    $message['q'] = 'error';
}

echo json_encode($message);


?>