<?php

include "../connect.php";

require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once '../vendor/phpmailer/phpmailer/src/Exception.php';
require_once '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once '../vendor/phpmailer/phpmailer/src/SMTP.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;

$input = file_get_contents('php://input');


//require 'C:\wamp644\composer\vendor\autoload.php';

$data = json_decode($input, true);

$message = array();
$result = array();

if ($data['action'] == 'insert') {

    $email = $data['email'];
    $cat = addslashes($data['cat']);
    $nom = addslashes($data['nom']);
    $siret = $data['siret'];
    $adresse = addslashes($data['adresse']);

    $query = mysqli_query($_SESSION['connexion'], "SELECT * FROM pro WHERE email_pro='$email'");
    if ($query) {
        $check = mysqli_num_rows($query);
        if ($check > 0) {
            $message['status'] = 'existing-account';
            echo json_encode($message);
        } else {


            /* sofar.api@gmail.com
             * #8jo5q5N&kGi


             *
            $nom_dest='SoFAR '
            $dest=thomas2sofar@gmail.com
            $env='thomas2sofar@gmail.com';
            $key='pyxjzrcqynfooqqc';
            $nom_env='SoFAR Appli';*/

            $mail = new PHPMailer();
            // 1 = errors and messages
            // 2 = messages only

            try {

                // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                $mail->isSMTP();
                $mail->Host = 'smtp.gmail.com';
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->Port = 587;
                $mail->CharSet = 'UTF-8';

                $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                // Sender and recipient settings
                $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                $mail->addAddress('batboss325@gmail.com', 'Receiver Name');
//                $mail->addReplyTo('sofar.api@gmail.com', 'SoFAR'); // to set the reply to

                // Setting the email content
                $mail->IsHTML(true);
                $mail->Subject = "SoFAR: Nouveau client";

                $mail->Body = "<html>
                                   <p>Le " . $cat . "  " . $nom . " souhaite s'inscrire !</p>
                                   <p>Ses informations sont: </p>
                                   <p> Nom: " . $nom . " </p>
                                   <p> Siret: " . $siret . " </p>
                                   <p> Adresse: " . $adresse . " </p>
                                   <p> Email: " . $email . " </p>
                                   <p> Catégorie: " . $cat . " </p>
                              </html>";
                $mail->AltBody = " Le bar " . $nom . " souhaite s'inscrire !
                    Ses informations sont: 
                    Nom: " . $nom . " 
                    Siret: " . $siret . " 
                    Email: " . $email . " 
                    Adresse: " . $adresse . "
                    Catégorie: " . $cat . " ";


                /* Set the mail sender. */
                //$mail->setFrom('thomas2sofar@gmail.com', 'SoFAR');
//                $mail->setFrom($env, $nom_env);

                /* Add a recipient. */
//                $mail->addAddress($dest, $nom_dest);
                /* Set the subject. */
//                $mail->Subject = 'SoFAR: Nouveau client';
//
//                $mail->isHTML(TRUE);

//                $mail->Body = "<html>
//                                   <p>Le " . $cat . "  " . $nom . " souhaite s'inscrire !</p>
//                                   <p>Ses informations sont: </p>
//                                   <p> Nom: " . $nom . " </p>
//                                   <p> Siret: " . $siret . " </p>
//                                   <p> Adresse: " . $adresse . " </>
//                                   <p> Email: " . $email . " </p>
//                                   <p> Catégorie: " . $cat . " </p>
//                              </html> ";
//
//
//                $mail->AltBody = " Le bar " . $nom . " souhaite s'inscrire !
//               Ses informations sont:
//                Nom: " . $nom . "
//                Siret: " . $siret . "
//                Email: " . $email . "
//                Adresse: " . $adresse . "
//                Catégorie: " . $cat . " ";

//                $mail->isSMTP();

//                /* SMTP server address. */
//                $mail->Host = 'smtp.gmail.com';
//
//                /* Use SMTP authentication. */
//                $mail->SMTPAuth = TRUE;
//
//                /* Set the encryption system. */
//                $mail->SMTPSecure = 'tls';
//
//                /* SMTP authentication username. */
//                $mail->Username = $env;
//                //$mail->Username = 'thomas2sofar@gmail.com';
//
//
//                /* SMTP authentication password. */
//
////                $mail->Password = $key;
//                $mail->Password = '#8jo5q5N&kGi';
//
//
//                /* Set the SMTP port. */
//                $mail->Port = 587;

                /* Disable some SSL checks. */
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );


                /* Finally send the mail. */
                $message['send status'] = $mail->send();
            } catch (Exception $e) {
                /* PHPMailer exception. */
                $e->errorMessage();
                $message['erreur envoi'] = $e;
            } catch (\Exception $e) {
                /* PHP exception (note the backslash to select the global namespace Exception class). */
                $e->getMessage();
                $message['exception'] = $e;
            }


            $message['sql'] = mysqli_error($_SESSION['connexion']);

            echo json_encode($message);
        }
    } else {
        $message['query'] = 'error';
        $message['sql'] = mysqli_error($_SESSION['connexion']);
        echo json_encode($message);
    }

} else if ($data['action'] == "login") {

    $mdp = $data['mdp'];
    $email = $data['email'];

    $result['error'] = '';

    $q = mysqli_query($_SESSION['connexion'],
        "SELECT * FROM `pro` WHERE `email_pro`='$email'");

    if ($q) {
        $check = mysqli_num_rows($q);

        if ($check === 1) {
            $resultQ = mysqli_fetch_row($q);

            if (password_verify($mdp, $resultQ[5])) {
                $expiration = time() + 365 * 60 * 24 * 60;
                $issuer = 'localhost';

                $payload = [
                    'iat' => time(),
                    'user_id' => $resultQ[0],
                    'cat' => 'pro',
                    'exp' => $expiration,
                    'iss' => $issuer
                ];

                $token = Token::customPayload($payload, $_SESSION['secret']);

                $result['pro'] = array();
                $result['pro']['id_pro'] = $resultQ[0];
                $result['pro']['nom_pro'] = $resultQ[1];
                $result['pro']['adresse'] = $resultQ[2];
                $result['pro']['siret'] = $resultQ[3];
                $result['pro']['email_pro'] = $resultQ[4];
                $result['pro']['date_ouvert_pro'] = $resultQ[7];
                $result['pro']['date_happy_pro'] = $resultQ[8];
                $result['pro']['debut_happy_pro'] = $resultQ[9];
                $result['pro']['fin_happy_pro'] = $resultQ[10];
                $result['pro']['description_pro'] = $resultQ[11];
                $result['pro']['categorie_pro'] = $resultQ[12];
                $result['pro']['terrasse_pro'] = $resultQ[13];
                $result['pro']['access_token'] = $token;
            } else {
                $result['error'] = 'wrong-pw';
            }

        } else {
            $result['error'] = 'not-found';
        }

    } else {
        $result['error'] = 'Impossible d\'exécuter la requête : ' . mysqli_error($_SESSION['connexion']);
    }

    echo json_encode($result);

} else if ($data['action'] === 'search') {
    $type = $data['type'];
    $search = $data['search'];
    $offset = $data['offset'];
    $search = addslashes($search);
    $longitude_user=$data['coords_user'][0];
    $latitude_user=$data['coords_user'][1];
    $rayon = ($data['rayon']/1000);
    $type_tri = $data['tri'];
    if($type_tri==="Distance : par ordre croissant"){
        $tri = "distance ASC";
    }
    else if($type_tri==="Distance : par ordre décroissant"){
        $tri = "distance DESC";
    }
    else if($type_tri==="Note : par ordre croissant"){
        $tri = "note ASC";
    }
    else if($type_tri==="Note : par ordre décroissant"){
        $tri = "note DESC";
    }
    $str_time=date("H:i:s");
    $daysofweek=["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"];
    $dayofweek = date('w');
    $dayofweek = $daysofweek[$dayofweek-1];
    $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);

    sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
    $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
    $query = mysqli_query($_SESSION['connexion'], "SELECT `nom_pro`, `adresse`, `description_pro`, `date_ouvert_pro`, pro.`id_pro`, `categorie_pro`, `date_happy_pro`, `terrasse_pro`,
    (((acos(sin((".$latitude_user."*pi()/180)) * sin((`latitude`*pi()/180)) + cos((".$latitude_user."*pi()/180)) * cos((`latitude`*pi()/180)) * cos(((".$longitude_user."- `longitude`) * pi()/180)))) * 180/pi()) * 60 * 1.1515 * 1.609344) 
    as `distance`,  
    (AVG(`crit1`)+AVG(`crit2`)+AVG(`crit3`))/3 as note,
    ".$dayofweek.",
    
    @matin := LEFT(".$dayofweek.", LOCATE('/',".$dayofweek.") - 1),

    @soir := RIGHT(".$dayofweek.", LENGTH(".$dayofweek.")-LOCATE('/',".$dayofweek.")),
    
    @ouverture_matin := LEFT(@matin, LOCATE('-',@matin) - 1),
    @ouverture_soir := LEFT(@soir, LOCATE('-',@soir) - 1),
    
    @fermeture_matin := RIGHT(@matin, LENGTH(@matin)-LOCATE('-',@matin)),
    @fermeture_soir := RIGHT(@soir, LENGTH(@soir)-LOCATE('-',@soir)),
    
    @ouverture_matin_secondes := LEFT(@ouverture_matin, LOCATE(':',@ouverture_matin ) - 1)*60*60 + RIGHT(@ouverture_matin, LENGTH(@ouverture_matin)-LOCATE(':',@ouverture_matin))*60,
    @ouverture_soir_secondes := LEFT(@ouverture_soir, LOCATE(':',@ouverture_soir ) - 1)*60*60 + RIGHT(@ouverture_soir, LENGTH(@ouverture_soir)-LOCATE(':',@ouverture_soir))*60,
    
    @fermeture_matin_secondes := LEFT(@fermeture_matin, LOCATE(':',@fermeture_matin ) - 1)*60*60 + RIGHT(@fermeture_matin, LENGTH(@fermeture_matin)-LOCATE(':',@fermeture_matin))*60,
    @fermeture_soir_secondes := LEFT(@fermeture_soir, LOCATE(':',@fermeture_soir ) - 1)*60*60 + RIGHT(@fermeture_soir, LENGTH(@fermeture_soir)-LOCATE(':',@fermeture_soir))*60,
    
    
    ($time_seconds BETWEEN @ouverture_matin_secondes AND @fermeture_matin_secondes) OR ($time_seconds BETWEEN @ouverture_soir_secondes AND @fermeture_soir_secondes) as ouvert,
    
    `need_resa_details`,
    
    latitude,
    
    longitude,
    service,
    commander_dispo
                             
    FROM `pro` LEFT JOIN `notes` ON pro.`id_pro`=notes.`id_p` 
    JOIN `horaires` ON pro.`id_pro`=horaires.`id_pro`   
    WHERE (`categorie_pro` LIKE '%$type%') AND (`nom_pro` LIKE '%$search%') GROUP BY pro.`id_pro` HAVING `distance` <= $rayon
    ORDER BY ouvert DESC, ".$tri." LIMIT 4 OFFSET $offset");

    $response = mysqli_fetch_all($query);
    echo json_encode($response);


} else if ($data['action'] == "fetch") {


    $token = $data['token'];
    if ($token == null) {
        $message['error'] = 'no token provided';
        echo json_encode($message);
        exit();
    }

    $result['error'] = '';

    $payload = Token::getPayload($token, $_SESSION['secret']);

    $validate = Token::validate($token, $_SESSION['secret']);

    $id = $payload['user_id'];

    if ($validate) {

        $q = mysqli_query($_SESSION['connexion'], "SELECT * FROM pro WHERE id_pro='$id'");
        if (!$q) {
            $result['error'] = 'Impossible d\'exécuter la requête : ' . mysqli_error($_SESSION['connexion']);
        } else {
            $check = mysqli_num_rows($q);

            if ($check === 1) {
//                $resultQ = mysqli_fetch_row($q);

                while ($row = mysqli_fetch_assoc($q)) {
                    foreach ($row as $key => $value) {
                        if ($key == 'photo_pro') {
                            $result['pro']['photo'] = '';

                            /*
                             * fetch photo
                             */
                            // Instantiate the S3 class and point it at the desired host
                            $s3Client = S3Client::factory(array('credentials' => [
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET_KEY
                            ],
                                'region' => 'eu-west-3',
                                'version' => 'latest'
                            ));
                            $s3Client->registerStreamWrapper();
                            $baseURL = 's3://' . BUCKET_NAME;

                            $photoPath = $baseURL . '/profile_pictures/pro/' . $value;
                            if (file_exists($photoPath) && $value != '') {
                                $result['pro']['photo'] = base64_encode(file_get_contents($photoPath));
                            }
                        } else if (!in_array($key, array('mdp_pro'))) {
                            $result['pro'][$key] = $value;
                        }
                    }
                }

//                $result['pro'] = array();
//                $result['pro']['id_pro'] = $resultQ[0];
//                $result['pro']['nom_pro'] = $resultQ[1];
//                $result['pro']['adresse'] = $resultQ[2];
//                $result['pro']['siret'] = $resultQ[3];
//                $result['pro']['email_pro'] = $resultQ[4];
//                $result['pro']['date_ouvert_pro'] = $resultQ[7];
//                $result['pro']['date_happy_pro'] = $resultQ[8];
//                $result['pro']['debut_happy_pro'] = $resultQ[9];
//                $result['pro']['fin_happy_pro'] = $resultQ[10];
//                $result['pro']['description_pro'] = $resultQ[11];
//                $result['pro']['categorie_pro'] = $resultQ[12];
//                $result['pro']['terrasse_pro'] = $resultQ[13];



            } else {
                $result['error'] = 'more than one row for this id';
            }
        }
    } else {
        $result['error'] = 'authentification token is wrong';
    }

    echo json_encode($result);

} else if ($data['action'] == "fetch_photo") {

    $id_pro = $data['id_pro'];

    $q = mysqli_query($_SESSION['connexion'], "SELECT photo_pro FROM pro WHERE id_pro='$id_pro'");
    if (!$q) {
        $result['error'] = 'Impossible d\'exécuter la requête : ' . mysqli_error($_SESSION['connexion']);
    } else {
        $resultQ = mysqli_fetch_row($q);


        $result['pro'] = array();
        $result['pro']['id'] = $id_pro;
        $result['pro']['photo'] = '';
        /*
         * fetch photo
         */
        // Instantiate the S3 class and point it at the desired host
        $s3Client = S3Client::factory(array('credentials' => [
            'key' => AWS_KEY,
            'secret' => AWS_SECRET_KEY
        ],
            'region' => 'eu-west-3',
            'version' => 'latest'
        ));
        $s3Client->registerStreamWrapper();
        $baseURL = 's3://' . BUCKET_NAME;

        $photoPath = $baseURL . '/profile_pictures/pro/' . $resultQ[0];
        if (file_exists($photoPath) && $resultQ[0] != '') {
            $result['pro']['photo'] = base64_encode(file_get_contents($photoPath));
        }
    }
    echo json_encode($result);
}
