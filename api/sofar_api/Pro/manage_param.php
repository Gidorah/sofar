<?php

include "../connect.php";
require "../vendor/autoload.php";
use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
// Check for token validation
if ($validate) {
    $idPro = $payload['user_id'];

    $action = $data['action'];
    if ($action == 'update_need_resa_details') {
        $needResaDetails = $data['need_resa_details'];
        $queryUpdate = mysqli_query($_SESSION['connexion'], "UPDATE pro SET need_resa_details = '$needResaDetails' WHERE id_pro='$idPro'");
        if (!$queryUpdate) {
            $message['error'] = '$queryUpdate : ' . mysqli_error($_SESSION['connexion']);
        }
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);