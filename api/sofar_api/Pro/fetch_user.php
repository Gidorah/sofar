<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;


$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();

$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);
// Check for token validation
if ($validate) {
    $id = $data['user_id'];
    $query = mysqli_query($_SESSION['connexion'], "SELECT id_u, nom_u, prenom_u, date_naissance_u, sexe_u, date_inscr, photo FROM `user` WHERE id_u='$id'");
    if (!$query) {
        $message['status'] = mysqli_error($_SESSION['connexion']);
    } else {
        $row = mysqli_fetch_assoc($query);
        foreach ($row as $key => $value) {
            $message['user'][$key] = $value;
            if ($key == 'photo') {
                $message['user']['photo'] = '';
                /*
                 * fetch photo
                 */
                // Instantiate the S3 class and point it at the desired host
                $s3Client = S3Client::factory(array('credentials' => [
                    'key' => AWS_KEY,
                    'secret' => AWS_SECRET_KEY
                ],
                    'region' => 'eu-west-3',
                    'version' => 'latest'
                ));
                $s3Client->registerStreamWrapper();
                $baseURL = 's3://' . BUCKET_NAME;

                $photoPath = $baseURL . '/profile_pictures/user/' . $value;
                if (file_exists($photoPath) && $value != '') {
                    $message['user']['photo'] = base64_encode(file_get_contents($photoPath));
                }
            }
        }
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}
echo json_encode($message);