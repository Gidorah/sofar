<?php
header('Access-Control-Allow-Origin: *');


header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\AwsException;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $payload['user_id'];

    $current_timestamp = time();

    mysqli_query($_SESSION['connexion'], "UPDATE `reservation` SET `etat_res`='Passée' WHERE `id_user` = $idu AND  `etat_res`!='Passée'
AND (UNIX_TIMESTAMP(`date_res`)+ (LEFT(`heure_res`, LOCATE(':',`heure_res`) - 1)+1)*60*60+ + RIGHT(`heure_res`, LENGTH(`heure_res`)-LOCATE(':',`heure_res`))*60)<".$current_timestamp);


    $query = mysqli_query($_SESSION['connexion'], "SELECT * FROM `reservation` WHERE `id_user` = $idu ORDER BY reservation_id DESC");

    if ($query) {

        $res = array();
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            foreach ($row as $key => $value) {
                $res[$i][$key] = $value;
            }
            $i++;
        }

        $distinctPro = array();

        $s3Client = S3Client::factory(array('credentials' => [
            'key' => AWS_KEY,
            'secret' => AWS_SECRET_KEY
        ],
            'region' => 'eu-west-3',
            'version' => 'latest'
        ));
        $s3Client->registerStreamWrapper();
        $baseURL = 's3://' . BUCKET_NAME;

        for ($i = 0; $i < count($res); $i++) {
            $id_pro = intval($res[$i]['id_pro']);

            if (!array_key_exists($id_pro, $distinctPro)){
                $q2 = mysqli_query($_SESSION['connexion'], "SELECT nom_pro, photo_pro FROM `pro` WHERE id_pro='$id_pro' ");
                $row = mysqli_fetch_assoc($q2);
                $nom_pro = $row['nom_pro'];
                $photo_pro = $row['photo_pro'];

                $res[$i]['pro'] = $nom_pro;

                $pro['nom_pro'] = $nom_pro;
                $pro['photo_pro'] = '';

                $photoPath = $baseURL . '/profile_pictures/pro/' . $photo_pro;
                if (file_exists($photoPath) && $photo_pro != '') {
                    $pro['photo_pro'] = base64_encode(file_get_contents($photoPath));
                }

                $distinctPro[$id_pro] = $pro;
            }
        }

        $res['pros'] = $distinctPro;
    } else {
        $message['status'] = 'error';
    }
}
echo json_encode($res);

