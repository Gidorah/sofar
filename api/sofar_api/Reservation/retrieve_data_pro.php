<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$user = new User($token);

if ($user->isValid()) {
    $payload = $user->getPayload();
    $categorie = $payload['cat'];
    $idpro = null;
    if ($categorie == 'pro') {
        $idpro = $payload['user_id'];
    } elseif ($categorie == 'personnel') {
        $idpro = $payload['pro_id'];
    }

    //ICI ON RECUPERE LE TIMESTAMP ACTUEL ET ON LE COMPARE AU TIMESTAMP DES RESERVATIONS POUR UPDATE L'ETAT A PASSEE

    $current_timestamp = time();

    mysqli_query($_SESSION['connexion'], "UPDATE `reservation` SET `etat_res`='Passée' WHERE `id_pro` = $idpro AND  `etat_res`!='Passée'
AND (UNIX_TIMESTAMP(`date_res`)+ (LEFT(`heure_res`, LOCATE(':',`heure_res`) - 1)+1)*60*60 + RIGHT(`heure_res`, LENGTH(`heure_res`)-LOCATE(':',`heure_res`))*60)<" . $current_timestamp);

    $query = mysqli_query($_SESSION['connexion'], "SELECT * FROM `reservation` WHERE `id_pro` = $idpro ORDER BY reservation_id DESC");


    if ($query) {
        $message['result'] = array();
        $i = 0;
        while ($row = mysqli_fetch_assoc($query)) {
            foreach ($row as $key => $value) {
                $message['result'][$i][$key] = $value;
            }

            $idArt = intval($message['result'][$i]['id_article_res']);
            $qArt = mysqli_query($_SESSION['connexion'], "SELECT  article_nom FROM `articles` WHERE article_id='$idArt' ");

            if ($qArt) {
                $check = mysqli_num_rows($qArt);
                if ($check > 0) {
                    $res = mysqli_fetch_all($qArt)[0][0];
                } else {
                    $res = null;
                }
                $message['result'][$i]['nom_article_res'] = $res;

                $idu = intval($message['result'][$i]['id_user']);
                $qUser = mysqli_query($_SESSION['connexion'], "SELECT  `nom_u`, prenom_u, photo  FROM `user` WHERE `id_u`='$idu' ");

                if ($qUser) {
                    $ResUser = mysqli_fetch_row($qUser);


                    $nom = $ResUser[0];
                    $prenom = $ResUser[1];
                    $photo = $ResUser[0];

                    $message['result'][$i]['user_nom'] = $nom;
                    $message['result'][$i]['user_prenom'] = $prenom;
                    $message['result'][$i]['user_photo'] = '';

                    /*
                     * fetch list participants
                     */
                    $queryGetPro = mysqli_query($_SESSION['connexion'],
                        "SELECT need_resa_details FROM pro WHERE id_pro = '$idpro'");
                    if (!$queryGetPro) {
                        $message['error'] = mysqli_error($_SESSION['connexion']);
                    } else {
                        $needResaDetails = mysqli_fetch_row($queryGetPro)[0];
                        $nbPers = $message['result'][$i]['nb_perso_res'];
                        $idRes = $message['result'][$i]['reservation_id'];
                        $participants = array();

                        if ($needResaDetails == 1 && $nbPers > 1) {
                            $queryParticipants = mysqli_query($_SESSION['connexion'],
                                "SELECT * FROM reservation_participants WHERE reservation_id = '$idRes'");
                            if (!$queryParticipants) {
                                $message['error'] = mysqli_error($_SESSION['connexion']);
                            } else {
                                $j = 0;
                                while ($row2 = mysqli_fetch_assoc($queryParticipants)) {
                                    foreach ($row2 as $key2 => $value2) {
                                        $participants[$j][$key2] = $value2;
                                    }
                                    $j++;
                                }
                            }
                        }
                        $message['result'][$i]['participants'] = $participants;

                    }

                } else {
                    $message['error'] = mysqli_error($_SESSION['connexion']);
                }
            } else {
                $message['error'] = mysqli_error($_SESSION['connexion']);
            }
            $i++;
        }
    } else {
        $message['error'] = mysqli_error($_SESSION['connexion']);
    }

} else {
    $message['error'] = 'error token';
}

echo json_encode($message);


