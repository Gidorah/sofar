<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;



$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$user = new User($token);

if ($user->isValid()) {
    $payload = $user->getPayload();
    $categorie = $payload['cat'];
    $idPro = null;
    if ($categorie == 'pro') {
        $idPro = $payload['user_id'];
    } elseif ($categorie == 'personnel') {
        $idPro = $payload['pro_id'];
    }
    $id = $data['id'];
    $etat = $data['etat'];
    $valide = intval($data['valide']);
    $message = array();
    $result = array();
    $query = mysqli_query($_SESSION['connexion'], "UPDATE `reservation` SET 
                    etat_res='$etat',
                    validation_res='$valide'
                    WHERE `reservation_id`='$id' and id_pro = '$idPro' ");
    if (!$query) {
        $message['status'] = 'error';
        echo json_encode($message);
        echo json_encode(mysqli_error($_SESSION['connexion']));

    } else {
        if ($valide == 1 and $etat == "A venir") {
            // Reservation accepté -> mail de confirmation
            $query2 = mysqli_query($_SESSION['connexion'], "SELECT prenom_u, nom_u, email_u, nom_pro, date_res, heure_res, nb_perso_res FROM user, reservation, pro WHERE `reservation_id`='$id' and id_user = id_u and pro.id_pro = '$idPro' ");
            if ($query2) {
                $result = mysqli_fetch_row($query2);
                try {
                    $mail = new PHPMailer();

                    // Server settings
//                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // for detailed debug output
                    $mail->isSMTP();
                    $mail->Host = 'smtp.gmail.com';
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                    $mail->Port = 587;
                    $mail->CharSet = 'UTF-8';

                    $mail->Username = 'sofar.api@gmail.com'; // YOUR gmail email
                    $mail->Password = '#8jo5q5N&kGi'; // YOUR gmail password

                    // Sender and recipient settings
                    $mail->setFrom('sofar.api@gmail.com', 'SoFAR');
                    $mail->addAddress($result[2], $result[0]);
//                    $mail->addReplyTo('sofar.api@gmail.com', 'SoFAR'); // to set the reply to

                    /* Set the subject. */
                    $mail->Subject = 'SoFAR: Confirmation de ta réservation';
                    $mail->isHTML(TRUE);

                    $name = $result[0];
                    $proName = $result[3];
                    $date = $result[4];
                    $time = $result[5];
                    $nbrPerso = $result[6];

                    ob_start();
                    include('./mail_template/confirm_user_res/confirm_user_res_html.tpl');
                    $mail->Body = ob_get_clean();
                    include('./mail_template/confirm_user_res/confirm_user_res_plain.tpl');
                    $mail->AltBody = ob_get_clean();

                    /* Finally send the mail. */
                    $message['send_status'] = $mail->send();
                } catch (Exception $e) {
                    /* PHPMailer exception. */
                    $e->errorMessage();
                    $message['erreur envoi'] = $e;
                } catch (\Exception $e) {
                    /* PHP exception (note the backslash to select the global namespace Exception class). */
                    $e->getMessage();
                    $message['exception'] = $e;
                }
            } else {
                $message['error'] = 'querry2' . json_encode(mysqli_error($_SESSION['connexion']));
            }
        }
    }
} else {
    $message['error'] = 'wrong token';
}

echo json_encode($message);
