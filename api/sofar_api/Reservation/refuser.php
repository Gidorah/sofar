<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$user = new User($token);

if ($user->isValid()) {
    $payload = $user->getPayload();
    $categorie = $payload['cat'];
    $idPro = null;
    if ($categorie == 'pro') {
        $idPro = $payload['user_id'];
    } elseif ($categorie == 'personnel') {
        $idPro = $payload['pro_id'];
    }
    $idRes = $data['id'];

    $query= mysqli_query($_SESSION['connexion'], "UPDATE reservation SET etat_res = 'refus', validation_res = 2 WHERE id_pro = '$idPro' and reservation_id = '$idRes'");
    if(!$query) {
        $message['error']=(mysqli_error($_SESSION['connexion']));
    }
} else {
    $message['error'] = 'wronk token';
}

echo json_encode($message);
