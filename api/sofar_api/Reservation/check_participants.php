<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];

if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $payload['user_id'];

    $message['participants'] = array();

    $participants = $data['participants'];
    foreach ($participants as $participant) {
        $email = $participant['email'];
        $query = mysqli_query($_SESSION['connexion'],
            "SELECT * FROM user WHERE email_u = '$email'");
        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }
        if (mysqli_num_rows($query) != 1) {
            /*
             * L'email ne correspond pas a un compte So FAR
             */
            $participant['checked'] = false;
        } else {
            $participant['checked'] = true;
        }
        array_push($message['participants'], $participant);
    }
} else {
    $message['error'] = 'auth-token wrong signature';
}

echo json_encode($message);
