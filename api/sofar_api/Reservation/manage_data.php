<?php
include "../connect.php";
require "../vendor/autoload.php";

use ReallySimpleJWT\Token;

$input = file_get_contents('php://input');
$data = json_decode($input, true);
$message = array();
$message['error'] = '';

$token = $data['token'];
if ($token == null) {
    $message['error'] = 'no token provided';
    echo json_encode($message);
    exit();
}
$payload = Token::getPayload($token, $_SESSION['secret']);
$validate = Token::validate($token, $_SESSION['secret']);

if ($validate) {
    $idu = $payload['user_id'];

    if ($data['action'] === 'insert') {

        $elt = $data['elt'];

        $nbPers = $elt['nbPers'];
        $dateRes = $elt['dateRes'];
        $heureRes = $elt['heureRes'];
        $idArtRes = $elt['idArtRes'];
        $validRes = $elt['validRes'];
        $etatRes = $elt['etatRes'];
        $idp = $elt['idp'];
        $emplacement = $elt['emplacement'];
        $consigne = addslashes($elt['consigne']);

        $queryGetPro = mysqli_query($_SESSION['connexion'],
            "SELECT need_resa_details FROM pro WHERE id_pro = '$idp'");
        if (!$queryGetPro) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        } else {
            $needResaDetails = mysqli_fetch_row($queryGetPro)[0];
            if ($needResaDetails == 1 && $nbPers > 1) {
                /*
                 * On est dans le cas ou pour réserver, l'établissement demande le détails des participants
                 */
                if (array_key_exists('participants', $elt)) {
                    $participants = $elt['participants'];
                    /*
                     * Check si tout les participants existe dans la DB So FAR
                     */
                    $allCheck = true;
                    foreach ($participants as $participant) {
                        $email = $participant['email'];
                        $query = mysqli_query($_SESSION['connexion'],
                            "SELECT * FROM user WHERE email_u = '$email'");
                        if (!$query) {
                            $message['error'] = mysqli_error($_SESSION['connexion']);
                        }
                        if (mysqli_num_rows($query) != 1) {
                            /*
                             * L'email ne correspond pas a un compte So FAR
                             */
                            $allCheck = false;
                        } else {
                            $row = mysqli_fetch_assoc($query);
                            foreach ($row as $key => $value) {
                                if (!in_array($key, array('mdp_pro'))) {
                                    $participant[$key] = $value;
                                }
                            }
                        }
                        $participants[$participant['i']] = $participant;
                    }
                    if (!$allCheck) {
                        $message['error'] = 'Some participants does not have a So FAR Account';
                    } else {
                        /*
                         * Jusqu'ici tout est ok, les participants existent
                         */
                        $query = mysqli_query($_SESSION['connexion'], "INSERT INTO `reservation`( `id_article_res`, `date_res`, `heure_res`,
                                 `nb_perso_res`, `validation_res`, `etat_res`, `id_pro`, `id_user`, `emplacement`, `consigne`)
                                 VALUES ('$idArtRes', '$dateRes', '$heureRes', '$nbPers', '$validRes',
                                 '$etatRes', '$idp', '$idu', '$emplacement', '$consigne')");

                        $message['status'] = 'success';
                        if (!$query) {
                            $message['status'] = 'error';
                            $message['error'] = mysqli_error($_SESSION['connexion']);
                        } else {

                            $resaId = mysqli_insert_id($_SESSION['connexion']);
//                            $resaId = 12;

                            foreach ($participants as $participant) {
                                $nom = $participant['nom_u'];
                                $prenom = $participant['prenom_u'];
                                $idPart = $participant['id_u'];
                                $sqlInsertResParticipant = "INSERT INTO `reservation_participants`
                                 (`reservation_id`, `id_u`, `nom_u`, `prenom_u`) 
                                 VALUES ('$resaId', '$idPart', '$nom', '$prenom');";

//                                $message['error'] = $idPart . ' - ' . $nom . ' - ' . $prenom;

                                $queryResPart = mysqli_query($_SESSION['connexion'], $sqlInsertResParticipant);
                                if (!$queryResPart) {
                                    $message['error'] = mysqli_error($_SESSION['connexion']);
                                }
                            }

                        }
                    }
                } else {
                    $message['error'] = 'missing participants list';
                }
            } else {


                $query = mysqli_query($_SESSION['connexion'], "INSERT INTO `reservation`( `id_article_res`, `date_res`, `heure_res`,
                                 `nb_perso_res`, `validation_res`, `etat_res`, `id_pro`, `id_user`, `emplacement`, `consigne`) 
                                 VALUES ('$idArtRes', '$dateRes', '$heureRes', '$nbPers', '$validRes', 
                                 '$etatRes', '$idp', '$idu', '$emplacement', '$consigne')");

                $message['status'] = 'success';
                if (!$query) {
                    $message['status'] = 'error';
                    $message['error'] = mysqli_error($_SESSION['connexion']);
                }
            }
        }

    } else if ($data['action'] === 'cancel') {
        /*
         * Réservation annulée par l'user
         */

        $idRes = $data['id_res'];

        $query = mysqli_query($_SESSION['connexion'], "DELETE FROM reservation WHERE reservation_id='$idRes' and id_user='$idu'");

        if (!$query) {
            $message['error'] = mysqli_error($_SESSION['connexion']);
        }


    }
} else {
    $result['error'] = 'authentification token is wrong';
}

echo json_encode($message);
