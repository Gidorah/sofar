<?php

include_once(dirname(__FILE__).'/vendor/autoload.php');
use ReallySimpleJWT\Token;

$_SESSION['secret'] = 'H9Fe@MAt4$fi';

class User
{
    private $token;

    /**
     * User constructor.
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
        if ($token == null) {
            return null;
        }
    }

    public function isValid()
    {
        return Token::validate($this->token, $_SESSION['secret']);
    }

    public function getPayload()
    {
        $payload = Token::getPayload($this->token, $_SESSION['secret']);
        if ($this->isValid()) {
            return $payload;
        } else {
            return null;
        }
    }

    public static function createToken(int $id, string $categorie)
    {


        return $token;
    }

}
