# Front So FAR

Le front a été développé à l'aide du framework **Ionic** et **Angular**.

## Redirection vers API

Les requêtes faites à l'API sont redirigée par la variable ``BASE_API_URL`` du fichier [global.ts](./src/app/global.ts).


## Android

###  Build
Build l'apk non signée :

```bash 
$ ionic cordova build android --prod --release
```

Signature du jar (passphrase for keystore: sofar21):

```bash
$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore sofar-key.keystore ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk sofar-key
```

Optimisation de l'APK :

```bash
$ zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk SoFAR.apk
```
