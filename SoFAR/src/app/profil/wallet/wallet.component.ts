import { Component, OnInit } from '@angular/core';
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss'],
})
export class WalletComponent implements OnInit {

  constructor(
      private modalController: ModalController
  ) { }

  ngOnInit() {}

  async onCancel() {
    await this.modalController.dismiss();
  }

}
