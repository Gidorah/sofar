import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilPageRoutingModule } from './profil-routing.module';

import { ProfilPage } from './profil.page';
import {CommandeClientPageModule} from "../commande-client/commande-client.module";
import {PasswordChangerComponent} from "./password-changer/password-changer.component";
import {WalletComponent} from "./wallet/wallet.component";
import {ImageCropperModule} from "ngx-image-cropper";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ImageCropperModule,
        ProfilPageRoutingModule,
        CommandeClientPageModule,
        ReactiveFormsModule
    ],
  declarations: [ProfilPage, PasswordChangerComponent, WalletComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfilPageModule {}
