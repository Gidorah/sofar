import {Component, OnInit} from '@angular/core';
import {
    ToastController,
    AlertController,
    ActionSheetController,
    ModalController
} from '@ionic/angular';

import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {PasswordChangerComponent} from './password-changer/password-changer.component';
import {GlobalVariable} from "../global";
import {UserService} from "../user/user.service";
import {User} from "../user/user";
import {WalletComponent} from "./wallet/wallet.component";
import {ImageCroppedEvent} from "ngx-image-cropper";


@Component({
    selector: 'app-profil',
    templateUrl: './profil.page.html',
    styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {
    user: User = null;
    userUnchanged: User = null;
    cameraData: string;
    base64Image = 'data:image/jpeg;base64,';
    isUser = false;
    croppedImg = null;
    show;

    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;


    constructor(public http: HttpClient,
                public router: Router,
                public alert: AlertController,
                public toast: ToastController,
                private camera: Camera,
                public actionSheetCtrl: ActionSheetController,
                public modalController: ModalController,
                private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getUser().subscribe((user: User) => {
            if (user) {
                this.user = user;
                this.base64Image = 'data:image/jpeg;base64,' + user.photo;
                this.croppedImg = 'data:image/jpeg;base64,' + user.photo;
            }
        });
    }

    imageCropped(event: ImageCroppedEvent){
        this.croppedImg = event.base64 ;
    }

    /**Récupération des informations de l'utilisateur connecté à chaque entrée dans la page */
    ionViewWillEnter() {
        this.userService.fetchUser().then(obs => obs.subscribe());
    }

    /** Ouverture de la caméra et récupération de la photo prise */
    openCamera() {
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 350,
            targetHeight: 350,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imageData) => {
            this.cameraData = imageData;
            this.croppedImg = 'data:image/jpeg;base64,' + imageData;
            this.changePhoto();
        }, (err) => {
            // Handle error
            console.log(err);
        });
    }

    /**Ouverture de la galerie et récupération de la photo */
    openGallery() {
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 350,
            targetHeight: 350,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imageData) => {
            // this.show=1;
            this.cameraData = imageData;
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            this.croppedImg ='data:image/jpeg;base64' + imageData;
            console.log("*************************************", this.base64Image);
            this.changePhoto();
            console.warn(this.base64Image);
        }, (err) => {
            // Handle error
            console.log(err);
        });
    }

    onFileSelected(event){

    }

    /**Alert pour modifier sa photo de profil */
    async presentActionSheet() {
        const actionSheet = await this.actionSheetCtrl.create({
            header: 'Modifie ta photo de profil !',
            buttons: [
                {
                    text: 'Galerie',
                    icon: 'image',
                    handler: () => {
                        this.openGallery();
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',

                }
            ]
        });
        await actionSheet.present();
    }

    /**Changement de la photo de l'utilisateur */
    changePhoto() {
        this.user.photo = this.cameraData;
        this.userService.changePhoto(this.user.photo).then();
    }

    /** Création de toast avec message et durée personnalisables */
    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    /** Alert confirmation de déconnexion */
    async deconnexion() {
        const alert = await this.alert.create({
            header: 'Es-tu sûr(e) de vouloir te déconnecter ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: () => {
                        this.userService.logout().then(() => {
                            this.router.navigate(['/connexion-client'], { replaceUrl: true });
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async updateProfil() {
        if (this.user.email === '') {
            await this.showToast('Un email est requis', 1000);
        } else if (this.user.nom === '') {
            await this.showToast('Un nom est requis', 1000);
        } else if (this.user.prenom === '') {
            await this.showToast('Un prenom est requis', 1000);
        } else if (this.user.sexe === '') {
            await this.showToast('Une civilité est requise', 1000);
        } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.user.email)) {
            await this.showToast('Le mail est invalide', 1000);
        } else {
            this.userService.updateUser().then(obs => {
                obs.subscribe(async (res: any) => {
                    if (res.error === '') {
                        await this.showToast('Profil mis à jour !', 1000);
                        this.show=0;
                    } else if (res.error === 'mail-exist') {
                        await this.showToast('Cet email est déjà utilisé', 2000);
                    } else {
                        await this.showToast('Une erreur s\'est produite !', 2000);
                    }
                });
            });
        }
    }

    async changePassword() {
        const modal = await this.modalController.create({
            component: PasswordChangerComponent
        });

        modal.onDidDismiss().then(data => {
            console.log(data);
        });

        await modal.present();
    }

    async openWallet() {
        const modal = await this.modalController.create({
            component: WalletComponent
        });
        await modal.present();
    }
}
