import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "./user";
import {GlobalVariable} from "../global";
import {tap} from "rxjs/operators";
import {Favori} from "./favori";
import {NotificationService} from "../notification.service";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private user: BehaviorSubject<User> = new BehaviorSubject(null);

    private favoris: BehaviorSubject<Favori[]> = new BehaviorSubject<Favori[]>(new Array<Favori>());

    constructor(
        private httpClient: HttpClient,
        private storage: Storage,
        private notificationService: NotificationService
    ) {
        this.storage.create().then((s: Storage) => {
            this.storage.get('USER').then((user: User) => {
                this.user.next(user);
            });
        });
    }


    login(user: any, stayConnected: boolean): Observable<any> {
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/manage_data.php', user, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    if (res.user) {
                        await this.storage.set('ACCESS_TOKEN_USER', res.user.access_token);
                        await this.storage.set('USER_STAY_CONNECTED', true);
                        this.user.next({
                            id: res.user.id,
                            nom: res.user.nom,
                            prenom: res.user.prenom,
                            sexe: res.user.sexe,
                            email: res.user.email,
                            date_naissance: res.user.date_naissance,
                            photo: res.user.photo
                        });
                        this.notificationService.initPushNotification().subscribe((token)=>{
                            this.notificationService.addorremovedevice(res.user.access_token,token,"utilisateur","insert");
                        });
                        await this.storage.set('USER', this.user.value);
                    }
                } else {
                    console.error('error login user: ', res.error);
                }
            })
        );
    }

    async logout() {
        let token_user = await this.storage.get('ACCESS_TOKEN_USER');
        this.notificationService.initPushNotification().subscribe((token)=>{
            this.notificationService.addorremovedevice(token_user,token,"utilisateur","delete");
        });
        await this.storage.remove('ACCESS_TOKEN_USER');
        await this.storage.remove('USER_STAY_CONNECTED');
        await this.storage.remove('USER');
        this.user.next(null);
    }

    async getAutoConnect() {
        return this.storage.get('USER_STAY_CONNECTED');
    }

    async fetchUser(): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            action: 'fetch'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    if (res.user) {
                        this.user.next({
                            id: res.user.id,
                            nom: res.user.nom,
                            prenom: res.user.prenom,
                            sexe: res.user.sexe,
                            email: res.user.email,
                            date_naissance: res.user.date_naissance,
                            date_inscr: res.user.date_inscr,
                            photo: res.user.photo
                        });
                        await this.storage.set('USER', this.user.value);
                        console.table(this.user);
                    }
                } else {
                    console.error('error fetching user: ', res.error);
                }
            })
        );
    }

    async getTokenObs(): Promise<any> {
        return this.storage.get('ACCESS_TOKEN_USER');
    }

    getUser(): Observable<User> {
        return this.user;
    }

    getUserValue(): User {
        return this.user.value;
    }

    async updateUser(): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            email: this.user.value.email,
            nom: this.user.value.nom,
            prenom: this.user.value.prenom,
            sexe: this.user.value.sexe
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/update_user.php', body, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    const tok = await this.getTokenObs();
                    const body = {
                        token: tok,
                        email: this.user.value.email,
                        // nom: this.user.value.nom,
                        //prenom: this.user.value.prenom,
                        //sexe: this.user.value.sexe
                        photo: this.user.value.photo
                    };
                    return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/update_photo.php', body, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).pipe(
                        tap(async (res: any) => {
                            if (res.error === '') {
                                console.log('user updated successfully.');
                            } else {
                                console.log('error while updating user: ', res.error);
                            }
                        })
                    );
                    console.log('user updated successfully.');
                } else {
                    console.log('error while updating user: ', res.error);
                }
            })
        );
    }

    changePassword(form: {
        token: string,
        password: string,
        new_password: string
    }): Observable<any> {
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/update_password.php', form,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
    }

    isUserConnected(): boolean {
        return this.user.value !== null;
    }

    async getHoraires(id) {
        const horaires = [];
        const data = await this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Horaires/retrieve_data.php', JSON.stringify(id), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).toPromise();
        for (let i = 1; i < 8; i++) {
            if (data[0][i] === "ferme") {
                horaires.push("Fermé");
            } else {
                horaires.push(data[0][i]);
            }
        }
        return horaires;
    }

    async fetchFavoris(): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/retrieve_data.php', {token: tok},
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(tap((data: any) => {
            console.log(data);
            const fav: Favori[] = new Array<Favori>();
            if (data.error === '') {
                data.result.forEach(item => {
                    fav.push({
                        name: item.nom_fav,
                        description: item.description_pro,
                        categorie: item.categorie_pro,
                        id: item.id_pro,
                        name_pro: item.nom_pro,
                        adresse: item.adresse,
                        ouvert: item.date_ouvert_pro,
                        happy: item.date_happy_pro,
                        terrasse: item.terrasse_pro,
                        //pro_service: item.service,
                        img: item.photo_pro === '' ? "../assets/images/logo_dark.svg" : 'data:image/jpeg;base64,' + item.photo_pro,
                        need_resa_details: item.need_resa_details === '1',
                        commander_dispo: item.commander_dispo === '1'
                    });
                });
                this.favoris.next(fav);
            } else {
                console.log('Error fetching favoris:', data.error);
            }
        }, (error: any) => {
            console.log('Error fetching favoris:', error);
        })).pipe(tap(async () => {
            for (const fav of this.favoris.value) {
                fav.horaires_semaine = await this.getHoraires(fav.id);
                fav.horaires = fav.horaires_semaine[((new Date()).getDay() - 1) % 7];
            }
        }));
    }

    getFavoris(): Observable<Favori[]> {
        return this.favoris;
    }

    async deleteFavori(idPro: number): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            idp: idPro,
            action: 'delete'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(tap((res: any) => {
            if (res.error === '') {
                let favs = this.favoris.value;
                favs = favs.filter((fav: Favori) => fav.id !== idPro);
                this.favoris.next(favs);
            }
        }));
    }

    async renameFavori(idPro: number, name: string): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            idp: idPro,
            nom: name,
            action: 'update'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
    }

    async addFavori(idPro: number, name: string): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            idp: idPro,
            nom: name,
            action: 'insert'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(tap(async (res: any) => {
            if (res.error === '') {
                await this.fetchFavoris();
            }
        }));
    }

    changePhoto(image: string) {
        return this.getTokenObs().then(tok => {
            const user = {
                token: tok,
                image: image
            };
            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/update_photo.php', user,
                {
                    headers : {
                        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).pipe(
                tap(async (res: any) => {
                    console.log('changePhoto()');
                    const userCopy = this.user.value;
                    userCopy.photo = image;
                    this.user.next(userCopy);
                })
            ).subscribe();
        });
    }

}
