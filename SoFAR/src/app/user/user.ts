export interface User {
    id: number;
    nom: string;
    prenom: string;
    sexe: string;
    email: string;
    date_naissance: string;
    date_inscr?: string;
    //photo?: string;
    photo: string;
}
