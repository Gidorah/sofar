export interface Favori {
    name: string;
    description: string;
    categorie: string;
    id: number;
    name_pro: string;
    adresse: string;
    ouvert: string;
    happy: string;
    terrasse: boolean;
    horaires?: string;
    horaires_semaine?: string[];
    img: string;
    need_resa_details?: boolean;
    commander_dispo: boolean;
}
