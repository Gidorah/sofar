import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsPersonnelPageRoutingModule } from './tabs-personnel-routing.module';

import { TabsPersonnelPage } from './tabs-personnel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsPersonnelPageRoutingModule
  ],
  declarations: [TabsPersonnelPage]
})
export class TabsPersonnelPageModule {}
