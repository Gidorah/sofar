import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {TabsPersonnelPage} from './tabs-personnel.page';

const routes: Routes = [
    {
        path: '',
        component: TabsPersonnelPage,
        children: [
            {
                path: 'mon-compte-personnel',
                loadChildren: () => import('../mon-compte-personnel/mon-compte-personnel.module').then(m => m.MonComptePersonnelPageModule)
            },
            {
                path: 'services',
                loadChildren: () => import('../services/services.module').then(m => m.ServicesPageModule)
            },
            {
                path: '',
                redirectTo: 'services',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPersonnelPageRoutingModule {
}
