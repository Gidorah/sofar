import {Component, OnInit} from '@angular/core';
import {PersonnelService} from "../personnel.service";

@Component({
    selector: 'app-tabs-personnel',
    templateUrl: './tabs-personnel.page.html',
    styleUrls: ['./tabs-personnel.page.scss'],
})
export class TabsPersonnelPage implements OnInit {

    constructor(private persService: PersonnelService) {
    }

    ngOnInit() {
        this.persService.fetchPers().then(obs => obs.subscribe());
    }

}
