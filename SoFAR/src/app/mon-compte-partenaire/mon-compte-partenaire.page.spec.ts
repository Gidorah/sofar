import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MonComptePartenairePage } from './mon-compte-partenaire.page';

describe('MonComptePartenairePage', () => {
  let component: MonComptePartenairePage;
  let fixture: ComponentFixture<MonComptePartenairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonComptePartenairePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MonComptePartenairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
