import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";
import {StorageService} from "../../storage.service";
import {ProService} from "../../pro/pro.service";

@Component({
    selector: 'app-password-changer',
    templateUrl: './password-changer.component.html',
    styleUrls: ['./password-changer.component.scss'],
})
export class PasswordChangerComponent implements OnInit {

    pw = '';
    newPw = '';
    pwRepeat = '';

    token = '';

    constructor(private modalController: ModalController,
                public toast: ToastController,
                private router: Router,
                private proService: ProService,
                private loadingController: LoadingController) {
    }

    ngOnInit() {
        this.proService.getTokenProObs().then(res => this.token = res);
    }

    isPwValid(pw: string): boolean {
        return pw.length >= 6;
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    onCancel() {
        this.modalController.dismiss();
    }

    async onSubmit() {
        const loading = await this.loadingController.create({
            cssClass: 'loading-class',
            message: 'Veuillez patienter...',
        });
        if (this.pw === '') {
            this.showToast('Renseignez votre mot de passe', 2000);
        } else if (!this.isPwValid(this.newPw)) {
            this.showToast('Le nouveau mot de passe n\'est pas valide', 3000);
        } else if (this.newPw === this.pw) {
            this.showToast('Veuillez renseignez un mot de passe différent de l\'ancien', 3000);
        } else if (this.newPw !== this.pwRepeat) {
            this.showToast('Les mots de passes renseignés ne sont pas les mêmes', 3000);
        } else {
            // tout est valide
            await loading.present();
            this.proService.changePasswordPro({
                token: this.token,
                password: this.pw,
                new_password: this.newPw
            }).subscribe((res: any) => {
                const err = res.error;
                loading.dismiss();
                if (err === '') {
                    this.showToast('Mot de passe modifié', 2000);
                    this.onCancel();
                } else if (err === 'too-short') {
                    this.showToast('Le nouveau mot de passe n\'est pas valide', 3000);
                } else if (err === 'wrong-pw') {
                    this.showToast('L\'ancien mot de passe est faux', 3000);
                } else {
                    this.showToast('Oups ! Erreur du réseau', 1500);
                }
            });
        }
    }

    getPw($event) {
        this.pw = $event.target.value;
    }

    getNewPw($event) {
        this.newPw = $event.target.value;
    }

    getPwRepeat($event) {
        this.pwRepeat = $event.target.value;
    }
}
