import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ModalController, ToastController, AlertController, ActionSheetController} from '@ionic/angular';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router, NavigationExtras} from '@angular/router';

import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {PasswordChangerComponent} from './password-changer/password-changer.component';
import {HoursChangerComponent} from "./hours-changer/hours-changer.component";
import {GlobalVariable} from "../global";
import {ProService} from "../pro/pro.service";
import {Pro} from "../pro/pro";

@Component({
    selector: 'app-mon-compte-partenaire',
    templateUrl: './mon-compte-partenaire.page.html',
    styleUrls: ['./mon-compte-partenaire.page.scss'],
})

export class MonComptePartenairePage implements OnInit {
    pro: Pro = null;
    cameraData: string;
    base64Image: string;
    imageProfil: any;

    constructor(public http: HttpClient,
                public router: Router,
                public proService: ProService,
                public alert: AlertController,
                public toast: ToastController,
                private camera: Camera,
                public actionSheetCtrl: ActionSheetController,
                public modalController: ModalController,
                public alertController: AlertController) {
    }

    ngOnInit() {
        this.proService.getPro().subscribe((pro: Pro) => {
            if (pro) {
                console.log('pro: ', pro);
                this.pro = pro;
                this.base64Image = 'data:image/jpeg;base64,' + pro.photo;
            }
        });
    }

    /** Récupération des informations du pro connecté à chaque entrée dans la page */
    ionViewWillEnter() {
        this.proService.fetchPro().then(obs => obs.subscribe());
    }

    /** Ouverture de la caméra et récupération de la photo prise */
    openCamera() {
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 150,
            targetHeight: 150,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
            this.cameraData = imageData;
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            this.changePhoto();
        }, (err) => {
            // Handle error
            console.log(err);
        });
    }

    /**Ouverture de la galerie et récupération de la photo */
    openGallery() {
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 150,
            targetHeight: 150,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
            this.cameraData = imageData;
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            this.changePhoto();
        }, (err) => {
            // Handle error
            console.log(err);
        });
    }

    /**Alert pour modifier sa photo de profil */
    async presentActionSheet() {
        const actionSheet = await this.actionSheetCtrl.create({
            header: 'Modifie ta photo de profil !',
            buttons: [
                {
                    text: 'Camera',
                    icon: 'camera',
                    handler: () => {
                        this.openCamera();
                    }
                },
                {
                    text: 'Galerie',
                    icon: 'image',
                    handler: () => {
                        this.openGallery();
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        await actionSheet.present();
    }

    /**Changement de la photo du pro*/
    changePhoto() {
        const options: CameraOptions = {
            quality: 100,
            targetWidth: 1000,
            targetHeight: 1000,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imageData) => {
            this.cameraData = imageData;
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            this.pro.photo = this.cameraData;

            this.proService.changePhotoPro(this.pro.photo).then();
            // this.storageservice.changePhotoPro(this.cameraData).then(r => {
            // });
        }, (err) => {
            // Handle error
            console.log(err);
        });
        // this.pro[0][6] = this.cameraData;
        // this.modifyUser();
    }

    /**Mis à jour des informations du pro et du storage */
    // modifyUser() {
    //     const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
    //     this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/update_pro.php', JSON.stringify(this.pro[0]), {
    //         headers: {
    //             'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    //         }
    //     }).subscribe(data => {
    //             this.storageservice.setPro('storage_pro', data);
    //             this.pro = this.storageservice.getPro();
    //             this.base64Image = 'data:image/jpeg;base64,' + this.pro[0][6];
    //             this.showToast('Modification réussie', 300);
    //         },
    //         (error: any) => {
    //             console.log(error);
    //         });
    // }

    /**Création de toasts avec message et durée personnalisables */
    async showToast(message, duration) {
        const toast = await this.toast.create({
            message: message,
            duration: duration,
        });
        await toast.present();
    }

    /**Déconnexion du pro et suppession de ses données du storage */
    logout() {
        this.proService.logoutPro().then(() => {
            this.router.navigate(['/connexion-client'], { replaceUrl: true });
        });
    }

    /**Alert confirmation de déconnexion */
    async deconnexion() {
        let alert = await this.alert.create({
            header: 'Es-tu sûr(e) de vouloir te déconnecter ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: () => {
                        this.logout();
                    }
                }
            ]
        })
        await alert.present();
    }

    /**Envoi de mails de confirmation de changement de mots de passe ou de mail */
    confirm(file) {
        console.log(this.pro);
        let headers: any = new HttpHeaders({'Content-Type': 'application/json'});
        this.http.post(file, JSON.stringify(this.pro), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe(data => {
            },
            (error: any) => {
                console.log(error);
            });
    }

    async changePassword() {
        const modal = await this.modalController.create({
            component: PasswordChangerComponent
        });

        modal.onDidDismiss().then(data => {
            console.log(data);
        });

        await modal.present();
    }

    async changeHoraires() {
        const modal = await this.modalController.create({
            component: HoursChangerComponent
        });

        modal.onDidDismiss().then(data => {
            console.log(data);
        });

        await modal.present();
    }

    /*
    Présente le mail de service pro de SoFar pour changer les infos d'un pro
     */
    async contactSF() {
        const alert = await this.alertController.create({
            cssClass: 'alert',
            header: 'Contact',
            subHeader: 'Changement de vos informations professionnelles',
            message: 'Veuillez contacter SoFAR à l\'adresse sofar.api@gmail.com .', // TODO : adresse
            buttons: ['OK']
        });

        await alert.present();

        const {role} = await alert.onDidDismiss();
        console.log('onDidDismiss resolved with role', role);
    }

    @ViewChild('desc') myDesc: ElementRef;

    resize() {
        var element = this.myDesc['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
        var scrollHeight = element.scrollHeight;
        element.style.height = scrollHeight + 'px';
        this.myDesc['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
    }

    changeDesc() {
        if (this.pro.description_pro.length > 1000) {
            this.showToast('La description est trop longue', 3000);
        } else if (this.pro.description_pro.length < 10) {
            this.showToast('La description est trop courte', 3000);
        } else {
            this.proService.getTokenProObs().then(tok => {
                const body = {
                    token: tok,
                    desc: this.pro.description_pro
                };
                this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/update_desc.php', body, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe((data: any) => {
                    if (data.error === '') {
                        this.showToast('Changement effectué !', 1000);
                    } else {
                        this.showToast('Une erreur s\'est produite !', 3000);
                    }
                    this.proService.fetchPro().then(obs => obs.subscribe());
                });
            });
        }
    }
}

