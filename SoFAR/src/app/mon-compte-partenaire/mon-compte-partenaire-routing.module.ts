import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonComptePartenairePage } from './mon-compte-partenaire.page';

const routes: Routes = [
  {
    path: '',
    component: MonComptePartenairePage,
 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonComptePartenairePageRoutingModule {}
