import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";
import {StorageService} from "../../storage.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GlobalVariable} from "../../global";
import {ProService} from "../../pro/pro.service";

@Component({
    selector: 'app-hours-changer',
    templateUrl: './hours-changer.component.html',
    styleUrls: ['./hours-changer.component.scss'],
})
export class HoursChangerComponent implements OnInit {

    jours: Array<{
        nom: string,
        ouverture: string,
        fermeture: string,
        ferme: boolean,
        demi:boolean,
        ouverture_soir: string,
        fermeture_soir: string,
    }> = [];

    token = '';

    jours_semaine = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']

    id: string = "";


    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;

    constructor(private modalController: ModalController,
                public toast: ToastController,
                private router: Router,
                private proService: ProService,
                private loadingController: LoadingController,
                public http: HttpClient) { }

    ngOnInit() {
        this.proService.getTokenProObs().then(res => this.token = res);

        this.id = this.proService.getProValue().id_pro;

        let ouverture;
        let fermeture;
        let ouverture_soir;
        let fermeture_soir;

        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Horaires/retrieve_data.php', JSON.stringify(this.id), {
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe(
            (data) => {
                this.jours = [];
                for(let i=1;i<8;i++){
                    ouverture=data[0][i].split("-")[0];
                    fermeture=data[0][i].split("-")[1];
                    let ferme;
                    let demi;
                    if(data[0][i]=="ferme"){
                        ferme=true;
                        ouverture="00:00";
                        fermeture="00:00";
                    }
                    else{
                        ferme=false;
                        if(data[0][i].split("/")==data[0][i]){
                            ouverture=data[0][i].split("-")[0];
                            fermeture=data[0][i].split("-")[1];
                            ouverture_soir="00:00";
                            fermeture_soir="00:00";
                            demi=false;
                        }              
                        else{
                            let matin = data[0][i].split("/")[0];
                            let soir = data[0][i].split("/")[1];
                            ouverture=matin.split("-")[0];
                            fermeture=matin.split("-")[1];
                            ouverture_soir=soir.split("-")[0];
                            fermeture_soir=soir.split("-")[1];
                            demi=true;
                        }
                    }
                    this.jours.push({
                        // On utilise l'algorithme de tri défini en dessous
                        nom:this.jours_semaine[i-1],
                        ouverture:ouverture,
                        fermeture:fermeture,
                        ouverture_soir:ouverture_soir,
                        fermeture_soir:fermeture_soir,
                        ferme:ferme,
                        demi:demi,
                    });
                }

            }, (error: any) => {
                console.log(error);
            })
    }

    onCancel() {
        this.modalController.dismiss();
    }

    async onSubmit() {
        let days=[];

        for(let i=0;i<7;i++){
            if(this.jours[i].ferme==true){
                days.push("ferme");
            }
            else if(this.jours[i].demi==true){
                days.push(this.jours[i].ouverture+"-"+this.jours[i].fermeture+"/"+this.jours[i].ouverture_soir+"-"+this.jours[i].fermeture)
            }
            else{
                days.push(this.jours[i].ouverture+"-"+this.jours[i].fermeture)
            }

        }

        let jours = [{
            id_pro: this.id,
            lundi: days[0],
            mardi: days[1],
            mercredi: days[2],
            jeudi: days[3],
            vendredi: days[4],
            samedi: days[5],
            dimanche: days[6],
            action:"changement",
        }];
        console.log(jours[0]);
        this.postHtml(jours[0]);
    }

    postHtml(element) {
        const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Horaires/manage_data.php', JSON.stringify(element), {
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe(data => {
            if(data['status']==='horaires_modifies'){
                this.showToast('Horaires modifiés !',1000);
            }
            else{
                this.showToast('Erreur',1000);
            }
        }, (error: any) => {
            console.log(error);
        });

    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }
}
