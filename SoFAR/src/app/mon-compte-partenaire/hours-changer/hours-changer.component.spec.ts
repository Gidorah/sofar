import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HoursChangerComponent } from './hours-changer.component';

describe('HoursChangerComponent', () => {
  let component: HoursChangerComponent;
  let fixture: ComponentFixture<HoursChangerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoursChangerComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HoursChangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
