import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonComptePartenairePageRoutingModule } from './mon-compte-partenaire-routing.module';

import { MonComptePartenairePage } from './mon-compte-partenaire.page';
import {PasswordChangerComponent} from "./password-changer/password-changer.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MonComptePartenairePageRoutingModule
  ],
  declarations: [MonComptePartenairePage, PasswordChangerComponent]
})
export class MonComptePartenairePageModule {}
