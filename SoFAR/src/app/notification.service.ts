import { Injectable } from '@angular/core';
import * as Pinpoint from "aws-sdk/clients/pinpoint";
import {Push, PushObject, PushOptions} from "@ionic-native/push/ngx";
import {AlertController} from "@ionic/angular";
import {UserService} from "./user/user.service";
import {Subject} from "rxjs";
import {GlobalVariable} from "./global";
import {tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
const AWS = require('aws-sdk');

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
      private httpClient: HttpClient,
      public push: Push,
      public alertCtrl: AlertController) {
  }

//La région AWS - on met londres parce que pinpoint est pas dispo à paris
  region = 'eu-west-2';

// l'ID de projet amazon pinpoint
  applicationId = '4aa3bf48f2624167a41346e442d6b5d0';

// Action quand on ouvre la notif :
// OPEN_APP (opens the app or brings it to the foreground)
// DEEP_LINK (opens the app to a specific page or interface)
// URL (opens a specific URL in the device's web browser.)
  action = 'OPEN_APP';

// URL si on ouvre une page internet
  url = '';

  priority = 'normal';

  ttl = 30;

  silent = false;

  CreateMessageRequest(title, message, token) {
    let messageRequest = {
      'Addresses': {
        [token]: {
          'ChannelType' : 'GCM'
        }
      },
      'MessageConfiguration': {
        'GCMMessage': {
          'Action': this.action,
          'Body': message,
          'Priority': this.priority,
          'SilentPush': this.silent,
          'Title': title,
          'TimeToLive': this.ttl,
          'Url': this.url
        }
      }
    };
    return messageRequest
  }

  ShowOutput(data,token){
    let status
    if (data["MessageResponse"]["Result"][token]["DeliveryStatus"]
        == "SUCCESSFUL") {
      status = "Message sent! Response information: ";
    } else {
      status = "The message wasn't sent. Response information: ";
    }
    console.log(status);
    console.dir(data, { depth: null });
  }

  SendMessage(title, message, token_u, categorie, tok) {
    this.getdevice(token_u,categorie,tok).subscribe((devices)=>{
      for(let device of devices){
        let messageRequest = this.CreateMessageRequest(title,message, device);
        AWS.config.update({ region: this.region,
          accessKeyId:"AKIAYEID5D2O6BYIRB4Z",
          secretAccessKey: "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",
        });

        let pinpoint : Pinpoint = new Pinpoint();
        let params = {
          "ApplicationId": this.applicationId,
          "MessageRequest": messageRequest
        };

        let _this = this;
        pinpoint.sendMessages(params, function(err, data) {
          if (err) console.log(err);
          else _this.ShowOutput(data,device);
        });
      }
    });
  }

  initPushNotification(){

    const options: PushOptions = {
      android: {
        senderID: "1050873025782"
      }
    };

    const pushObject: PushObject = this.push.init(options);

    let token = "";
    var subject = new Subject<string>();

    pushObject.on('registration').subscribe((data: any) => {
        token= data.registrationId;
        subject.next(token);
    });

    pushObject.on('notification').subscribe(async (data: any) => {
      console.log(data);
      if (data.additionalData.foreground) {
        let confirmAlert = await this.alertCtrl.create({
          header: 'Nouvelle notification : ',
          message: data.message,
          buttons: [{
            text: 'Ignorer',
            role: 'cancel'
          }, {
            text: 'Voir',
            handler: () => {
              //TODO: Your logic here
            }
          }]
        });
        await confirmAlert.present();
      }
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

    return subject.asObservable();
  }

  addorremovedevice(token_u,token_d,categorie,action){
    const body = {
      token: token_u,
      token_device: token_d,
      categorie: categorie,
      action: action
    };
    this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Devices/manage_data.php', body, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).subscribe(data => console.log(data['status']));
  }

  getdevice(id,categorie,tok){
    var subject = new Subject<string>();
    const body = {
      id: id,
      token:tok,
      categorie: categorie
    };
    this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Devices/retrieve_data.php', body, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).subscribe(data => {
      subject.next(data['result']);
      console.log(data);
    });
    return subject;
  }

}
