import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {BehaviorSubject, Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GlobalVariable} from "./global";

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(private  httpClient: HttpClient, public storage: Storage) {
    }

    /*
    Récupère la photo d'un pro a partir de son ID
     */
    fetchProPhoto(idPro: number) {
        const body = {
            id_pro: idPro,
            action: 'fetch_photo'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
    }

}
