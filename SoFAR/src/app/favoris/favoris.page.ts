import {Component, OnInit} from '@angular/core';
import {ModalController, AlertController} from '@ionic/angular';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {InfosResultatPage} from "../infos-resultat/infos-resultat.page";
import {GlobalVariable} from "../global";
import {User} from "../user/user";
import {UserService} from "../user/user.service";
import {Favori} from "../user/favori";

@Component({
    selector: 'app-favoris',
    templateUrl: './favoris.page.html',
    styleUrls: ['./favoris.page.scss'],
})
export class FavorisPage implements OnInit {
    id: any;
    user: User = null;

    favoris: Favori[];

    favs: Array<{ nom: string, idu: number, idp: number, action }> = [];
    rec: Array<{ name: string, description: string, categorie: string, id: number, name_pro: string,
        adresse: string, ouvert: string, happy: string, terrasse: boolean, horaires: any, img: any,
        commander_dispo: any }> = [];

    constructor(public modalCtrl: ModalController,
                private route: ActivatedRoute,
                private router: Router,
                public http: HttpClient,
                public alert: AlertController,
                private userService: UserService) {

    }

    getHtml() {
        this.userService.getTokenObs().then(tok => {
            const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/retrieve_data.php', {token: tok},
                {
                    headers : {
                        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe((data: any) => {
                console.log(data);
                this.rec = [];
                // for (let i = 0; data[i]; i++) {
                //     this.rec.push({
                //         name: data[i].nom_fav,
                //         description: data[i].description_pro,
                //         categorie: data[i].categorie_pro,
                //         id: data[i].id_pro,
                //         name_pro: data[i].nom_pro,
                //         adresse: data[i].adresse,
                //         ouvert: data[i].date_ouvert_pro,
                //         happy: data[i].date_happy_pro,
                //         terrasse: data[i].terrasse_pro,
                //     });
                // }
                if (data.error === '') {
                    data.result.forEach(async item => {
                        const horaires = await this.getHoraires(item.id_pro);
                        this.rec.push({
                            name: item.nom_fav,
                            description: item.description_pro,
                            categorie: item.categorie_pro,
                            id: item.id_pro,
                            name_pro: item.nom_pro,
                            adresse: item.adresse,
                            ouvert: item.date_ouvert_pro,
                            happy: item.date_happy_pro,
                            terrasse: item.terrasse_pro,
                           // pro_service: item.service,
                            horaires: horaires,
                            img: item.photo_pro === '' ? "../assets/images/logo_dark.svg" : 'data:image/jpeg;base64,' + item.photo_pro,
                            commander_dispo: item.commander_dispo
                        });
                    });
                } else {
                    console.log('Error');
                }
            }, (error: any) => {
                console.log(error);
            });
        });
    }

    async getHoraires(id) {
        let horaires = [];
        const data = await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Horaires/retrieve_data.php', JSON.stringify(id), {
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).toPromise();
        for (let i = 1; i < 8; i++) {
            if (data[0][i] == "ferme") {
                horaires.push("Fermé");
            } else {
                horaires.push(data[0][i]);
            }
        }
        return horaires;
    }

    ngOnInit() {
        this.userService.getUser().subscribe((user: User) => {
            this.user = user;
        });
        this.userService.getFavoris().subscribe((fav: Favori[]) => {
            this.favoris = fav;
        });
    }

    /**
     * Appel du contenu de la base de données à chaque fois on ouvre la page sur le tab
     */
    ionViewWillEnter() {
        if (this.user) {
            // this.getHtml();
            this.userService.fetchFavoris().then(obs => obs.subscribe());
        }
    }

    /**
     * renomme l'item en local puis envoie les informations à la base de données
     */
    async rename(event, item) {
        event.stopPropagation();
        const alert = await this.alert.create({
            header: 'Renommer',
            inputs: [
                {
                    name: 'nom',
                    type: 'text',
                    placeholder: 'Saisir le nouveau nom'
                }
            ],
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    handler: () => {
                        console.log('Confirm Cancel')
                    }
                },
                {
                    text: 'Valider',
                    handler: (alertData) => {
                        if (alertData.nom !== '') {
                            item.name = alertData.nom;
                            // this.favs = [{
                            //     nom: item.name,
                            //     idu: this.user.id,
                            //     idp: item.id,
                            //     action: 'update'
                            // }];

                            this.userService.renameFavori(item.id, item.name).then(obs => obs.subscribe());

                            // this.postHtml(this.favs[0]);
                        }

                    }
                }
            ]
        });
        return alert.present();

    }

    /**
     * Suppression directe dans la base de données puis recharge son contenu sur la page
     */
    async delete(event, item: Favori) {
        event.stopPropagation();
        const alert = await this.alert.create({
            header: 'Voulez vous vraiment supprimer ce favoris?',

            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    handler: () => {
                        console.log('Confirm Cancel')
                    }
                },
                {
                    text: 'Supprimer',
                    handler: () => {

                        this.userService.deleteFavori(item.id).then(obs => obs.subscribe());
                        // this.getHtml();
                    }
                }
            ]
        });
        return alert.present();
    }

    /**
     * Pour gérer les fonctionnalités renommer et supprimer
     */
    postHtml(element) {
        const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/manage_data.php', JSON.stringify(element),
            {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe(data => {
            console.log(data);
        }, (error: any) => {
            console.log(error);
        });

    }

    async clickCards(item) {
        const modal = await this.modalCtrl.create({
            component: InfosResultatPage,
            componentProps: {item: item},
            cssClass: "modal_recherche"
        });
        return await modal.present();
    }

    goRecherchePage() {
        this.router.navigate(['/tabs-client/recherche']);
    }
}
