import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {RedirectionClientComponent} from "./redirection-client/redirection-client.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'connexion-client',
    pathMatch: 'full'
  },
  {
    path: 'connexion-client',
    loadChildren: () => import('./connexion-client/connexion-client.module').then( m => m.ConnexionClientPageModule)
  },
  {
    path: 'connexion-partenaire',
    loadChildren: () => import('./connexion-partenaire/connexion-partenaire.module').then( m => m.ConnexionPartenairePageModule)
  },
  {
    path: 'redirection-client',
    component: RedirectionClientComponent
  },
  {
    path: 'inscription-client',
    loadChildren: () => import('./inscription-client/inscription-client.module').then( m => m.InscriptionClientPageModule)
  },
  {
    path: 'inscription-partenaire',
    loadChildren: () => import('./inscription-partenaire/inscription-partenaire.module').then( m => m.InscriptionPartenairePageModule)
  },
  {
    path: 'mon-compte-partenaire',
    loadChildren: () => import('./mon-compte-partenaire/mon-compte-partenaire.module').then( m => m.MonComptePartenairePageModule)
  },
  {
    path: 'recherche',
    loadChildren: () => import('./recherche/recherche.module').then( m => m.RecherchePageModule)
  },
  {
    path: 'resultat-recherche',
    loadChildren: () => import('./resultat-recherche/resultat-recherche.module').then( m => m.ResultatRecherchePageModule)
  },
  {
    path: 'infos-resultat',
    loadChildren: () => import('./infos-resultat/infos-resultat.module').then( m => m.InfosResultatPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'calendrier',
    loadChildren: () => import('./calendrier/calendrier.module').then( m => m.CalendrierPageModule)
  },
  {
    path: 'tabs-partenaire',
    loadChildren: () => import('./tabs-partenaire/tabs-partenaire.module').then( m => m.TabsPartenairePageModule)
  },
  {
    path: 'tabs-personnel',
    loadChildren: () => import('./tabs-personnel/tabs-personnel.module').then( m => m.TabsPersonnelPageModule)
  },
  {
    path: 'favoris',
    loadChildren: () => import('./favoris/favoris.module').then( m => m.FavorisPageModule)
  },
  {
    path: 'favoris',
    loadChildren: () => import('./favoris/favoris.module').then( m => m.FavorisPageModule)
  },
  {
    path: 'commande-client',
    loadChildren: () => import('./commande-client/commande-client.module').then( m => m.CommandeClientPageModule)
  },
  {
    path: 'tabs-client',
    loadChildren: () => import('./tabs-client/tabs-client.module').then( m => m.TabsClientPageModule)
  },
  {
    path: 'calendar-event',
    loadChildren: () => import('./calendar-event/calendar-event.module').then( m => m.CalendarEventPageModule)
  },
  {
    path: 'gestion-personnel',
    loadChildren: () => import('./gestion-personnel/gestion-personnel.module').then( m => m.GestionPersonnelPageModule)
  },
  {
    path: 'tabs-personnel',
    loadChildren: () => import('./tabs-personnel/tabs-personnel.module').then( m => m.TabsPersonnelPageModule)
  },
  {
    path: 'mon-compte-personnel',
    loadChildren: () => import('./mon-compte-personnel/mon-compte-personnel.module').then( m => m.MonComptePersonnelPageModule)
  },
  {
    path: 'services',
    loadChildren: () => import('./services/services.module').then( m => m.ServicesPageModule)
  },
  {
    path: 'carte-pro',
    loadChildren: () => import('./carte-pro/carte-pro.module').then( m => m.CarteProPageModule)
  },
  {
    path: 'commander',
    loadChildren: () => import('./commander/commander.module').then( m => m.CommanderPageModule)
  },
];



@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
