import {Component, OnInit} from '@angular/core';
import {StorageService} from '../storage.service';
import {Router} from "@angular/router";
import {UserService} from "../user/user.service";
import {Platform} from "@ionic/angular";

@Component({
    selector: 'app-tabs-client',
    templateUrl: './tabs-client.page.html',
    styleUrls: ['./tabs-client.page.scss'],
})
export class TabsClientPage implements OnInit {

    constructor(
        private router: Router,
        private platform: Platform,
        private userService: UserService
    ) {
    }

    ngOnInit() {
        this.userService.fetchUser().then(obs => obs.subscribe());
        this.userService.fetchFavoris().then(obs => obs.subscribe());
    }

    ionViewWillEnter() {
    }
}
