import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsClientPage } from './tabs-client.page';

const routes: Routes = [
  {
    path: '',
    component: TabsClientPage,
    children: [
      {
        path: 'recherche',
        loadChildren: () => import('../recherche/recherche.module').then(m => m.RecherchePageModule)
      },
      {
        path: 'commande-client',
        loadChildren: () => import('../commande-client/commande-client.module').then(m => m.CommandeClientPageModule)
      },
      {
        path: 'favoris',
        loadChildren: () => import('../favoris/favoris.module').then(m => m.FavorisPageModule)
      },
      {
        path: 'profil',
        loadChildren: () => import('../profil/profil.module').then(m => m.ProfilPageModule)
      },   
      {
        path: '',
        redirectTo: 'recherche',
        pathMatch: 'full'
      }
    ],
    },
    {
      path: '',
      redirectTo: 'recherche',
      pathMatch: 'full'
    }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsClientPageRoutingModule {}
