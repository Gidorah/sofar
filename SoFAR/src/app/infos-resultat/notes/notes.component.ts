import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {Ionic4DatepickerModalComponent} from "@logisticinfotech/ionic4-datepicker";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../../storage.service";
import {GlobalVariable} from "../../global";
import {InfosResultatPage} from "../infos-resultat.page";
import {UserService} from "../../user/user.service";

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {

  id;

  id_pro;

  nom_bar;

  criteres: Array<{ nom: string , note:number}> = [];

  note=3;

  commentaire="";

  action;

  constructor(private modalCtrl: ModalController,
              navParams: NavParams,
              public storageservice: StorageService,
              public userService: UserService,
              public http: HttpClient,
              public toast: ToastController,
              public alertController: AlertController) {
    this.criteres.push({nom:GlobalVariable.CRITERE1, note:navParams.get('note1')});
    this.criteres.push({nom:GlobalVariable.CRITERE2, note:navParams.get('note2')});
    this.criteres.push({nom:GlobalVariable.CRITERE3, note:navParams.get('note3')});
    this.id_pro = navParams.get('id');
    this.nom_bar = navParams.get('nom_bar');
    if(navParams.get('commentaire')!=undefined){
      this.commentaire=navParams.get('commentaire');
    }
    this.action=navParams.get('action');
  }

  onCancel() {
    this.modalCtrl.dismiss();
  }

  async showToast(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    await toast.present();
  }

  async ngOnInit() {
    this.id = this.userService.getUserValue().id;
  }

  async onSubmit_notes() {
      this.postHtml_notes();
  }

  async postHtml_notes() {
      const body = {
        id_u: this.id,
        id_p:this.id_pro,
        crit1: this.criteres[0].note,
        crit2: this.criteres[1].note,
        crit3: this.criteres[2].note,
        commentaire: this.commentaire,
        action: this.action,
      }
      if(body.crit1==undefined || body.crit2==undefined || body.crit3==undefined ){
        this.presentAlert();
      }
      else{
        console.log("ENVOI DE LA NOTE");
      }
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Notes/manage_data.php', body, {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      } ).subscribe(async (data: any) => {
        if (data.status === 'note_envoyee') {
          await this.showToast('Note envoyée !');
          this.modalCtrl.dismiss();
        }
        else if (data.status ===  'already_in_db'){
          await this.showToast('Vous avez déjà noté cet établissement!');
          this.modalCtrl.dismiss();
        }
        else {
          await this.showToast('Une erreur est survenue');
        }
        console.log(data);
      }, (error: any) => {
        console.log(error);
      });


  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Attention',
      message: 'Au moins une des notes n\'est pas remplie',
      buttons: ['OK']
    });

    await alert.present();
  }


}
