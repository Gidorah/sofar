import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfosResultatPage } from './infos-resultat.page';

describe('InfosResultatPage', () => {
  let component: InfosResultatPage;
  let fixture: ComponentFixture<InfosResultatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfosResultatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfosResultatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
