import { Component, OnInit } from '@angular/core';
import {AlertController, ModalController, NavParams, ToastController} from '@ionic/angular';
import { CommanderPage } from '../commander/commander.page';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ReserverPage } from '../reserver/reserver.page';
import { FavorisPage } from '../favoris/favoris.page';
import { Router, CanActivate, ActivatedRouteSnapshot, NavigationExtras } from '@angular/router';
import { StorageService } from '../storage.service';
import {Geolocation} from "@ionic-native/geolocation";
import {MapComponent} from "../map/map.component";
import {GlobalVariable} from "../global";
import {ResultatRecherchePage} from "../resultat-recherche/resultat-recherche.page";
import {NotesComponent} from "./notes/notes.component";
import {UserService} from "../user/user.service";


@Component({
  selector: 'app-infos-resultat',
  templateUrl: './infos-resultat.page.html',
  styleUrls: ['./infos-resultat.page.scss'],
})
export class InfosResultatPage implements OnInit {

  item: {
    name: string, adresse:string, ouvert:string, description: string, categorie: string, image: string, id: number, service: string, terrasse: number, fav: number, horaires: string, img:any, horaires_semaine: string[], commander_dispo:any};
  horaires: any = [];
  fav: Array<{nom : string, idu : number, idp: number, action}> =[];
  event: any;
  public items: any = [];
  public items1: any = [];
  public criteres: any = [];
  public criteria: any = [];
  isReadMore = true;
  jours=["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"];
  note_generale;
  aucune_note;
  commentaires: Array<{nom : string, commentaire : string, note: number}> =[];
  id_u;
  offset;
  desactive=false;
  servicep;

  commanderDispo = false;


  constructor(public modalCtrl: ModalController,
              navParams: NavParams,
              private router: Router,
              public http: HttpClient,
              public service: StorageService,
              public userService: UserService,
              public toast: ToastController,
              private alert: AlertController) {
    // console.log(this.service.getUser());
    this.item = navParams.get('item');
    this.horaires= this.item.horaires_semaine;
    console.log('oziinzoein item: ', this.item);
    this.commanderDispo = this.item.commander_dispo;
    console.log('oziinzoein commanderDispo: ', this.commanderDispo);
    /**
     * Itérateur sur l'accordeon dans description
     */
    this.items = [
      { expanded: false}
    ];
    this.items1= [
      { expanded: false}
    ];
    /**
     * Itérateur sur l'accordeon dans notes
     */
    this.criteria = [
      { expanded: false }
    ];
    if (this.userService.getUserValue()){
      this.id_u = this.userService.getUserValue().id;
    }
    // this.id_u=this.service.getUser()['id'];

  }
  /**
   * retour à resultat-recherche
   */
  pop() {
    this.modalCtrl.dismiss();
  }
  async ngOnInit() {
    await this.getNotes();
    this.offset=0;
    await this.getCom(true);
  }
  /**
   * envoie l'objet etablissement à la page reserver [on s'interesse aux attributs : nom, id, catégorie
   * et terrasse]
   */
  async go_to_reserve(){
    console.log("reserve");
    const modal = await this.modalCtrl.create({component: ReserverPage,
      componentProps: { item: this.item },
      cssClass:"modal_recherche"
    });
    return await modal.present();
  }
  /**
   * envoie l'objet etablissement à la page commander [on s'interesse aux attributs : nom, id, service]
   */
  async go_to_command(){
    const modal = await this.modalCtrl.create({
      component: CommanderPage,
      componentProps: {
        id_pro: this.item.id,
        name: this.item.name,
        service: this.item.service
      },
      id: 'commander-modal'
    });
    console.log('ID=',this.item.id,'NOM=', this.item.name,'SERVICE', this.item.service);
    await modal.present();
  }
  time(event){
    this.event=event;
    console.log(event.selectedTime);
  }
  /**
   * fonctionnalité du bouton 'M'y amener'
   * liaison avec Google Maps en Native : pas encore implémentée
   */
  async go_to_map(item: { name: string; adresse: string; description: string; categorie: string; image: string; id: number; terrasse: number }){
    console.log("map");
    const modal = await this.modalCtrl.create({component: MapComponent ,
      componentProps: { item: this.item },
      cssClass:"modal_recherche"
    });
    return await modal.present();
  }
  /**
   * renseigne l'objet fav en local puis envoie les données à la base
   */
  async add_to_fav(item) {
      this.userService.addFavori(item.id, item.name).then(obs => obs.subscribe(data => {
          if (data.status === 'fav_ajoute') {
              this.showToast('Favori ajouté !');
              this.item.fav = 1;
          } else if (data.status === 'fav_retire') {
              this.showToast('Favori retiré !');
              this.item.fav = 0;
          } else if (data.status === 'already_in_db') {
              this.showToast('Cet établissement est déjà dans tes favoris!');
          } else {
              this.showToast('Erreur');
          }
      }, (error: any) => {
          console.log(error);
      }));

  }

  async remove_fav(item){

    this.fav = [{
      nom: item.name,
      idu: this.userService.getUserValue().id,
      idp: item.id,
      action: 'delete'
    }];
    console.log(this.fav);
    this.postHtml(this.fav[0]);
    console.log(this.fav[0]);
    //this.pop();

  }
  postHtml(element) {
    console.log(element);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Favoris/manage_data.php', JSON.stringify(element), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).subscribe(data => {
      if(data['status']==='fav_ajoute'){
        this.showToast('Favori ajouté !');
        this.item.fav=1;
      }
      else if(data['status']==='fav_retire'){
        this.showToast('Favori retiré !');
        this.item.fav=0;
      }
      else if (data['status']=='already_in_db'){
        this.showToast('Cet établissement est déjà dans tes favoris!');
      }
      else{
        this.showToast('Erreur');
      }
    }, (error: any) => {
      console.log(error);
    });

  }
  /**
   * Un accordeon est un criterion qui correspond à une liste d'items sur la page
   * Ainsi l'appel de la fonction sur le label 'En savoir plus' permet de dérouler tous ceux de la liste items
   */
  expandItem(criterion, items): void {
    if (criterion.expanded) {
      criterion.expanded = false;
    } else {
      items.map(listItem => {
        if (criterion === listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }

  showText() {
    this.isReadMore = !this.isReadMore
  }

  async showToast(message){
    let toast = await this.toast.create({
      message:message,
      duration: 2000
    });
    await toast.present();
  }

  async go_to_notes(){
    const body = {
      id_p:this.item.id,
      id_u:this.id_u,
      action: "verify",
    }
    const data = await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Notes/retrieve_data.php', JSON.stringify(body), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).toPromise();
    if(data['status']!="pas_encore_dans_db"){
      await this.alert_note(data[0]);
    }
    else{
      const modal = await this.modalCtrl.create({
        component: NotesComponent,
        componentProps: {
          id: this.item.id,
          nom_bar: this.item.name,
          action:"insert",
        },
        cssClass: 'modal_recherche'
      });
      modal.onDidDismiss().then((data) => {
        this.update_notes_com();
      });
      await modal.present();
    }
  }

  async getNotes() {
    this.criteres=[];
    const body = {
      id_p:this.item.id,
      action: "getnotes",
    }
    const data = await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Notes/retrieve_data.php', JSON.stringify(body), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).toPromise();
    if(data[1]==","){ //ie si il n'y a pas encore de notes
      this.aucune_note=true;
    }
    else{
      this.aucune_note=false;
      let notes=JSON.parse(data.toString());
      this.note_generale=(notes[0]+notes[6]+notes[12])/3;
      this.criteres.push({nom:GlobalVariable.CRITERE1, note:notes[0]});
      this.criteres.push({nom:GlobalVariable.CRITERE2, note:notes[6]});
      this.criteres.push({nom:GlobalVariable.CRITERE3, note:notes[12]});
    }
  }

  async getCom(init) {
    if (init==true){
      this.commentaires=[];
    }
    const body = {
      id_p:this.item.id,
      offset:this.offset,
      action: "getcommentaires",
    }
    await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Notes/retrieve_data.php', JSON.stringify(body), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).subscribe(async data=>{
      console.log(data);
      let i;
      for (i = 0; data[i]; i++) {
        this.commentaires.push({
          nom:data[i][2]+"."+data[i][1][0],
          commentaire:data[i][0],
          note:data[i][3],
        })
      }
      if(i<3){
        this.desactive=true;
      }
    });
  }

  async alert_note(notes) {
    const alert = await this.alert.create({
      header: 'Tu as déjà noté cet établissement',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
        },
        {
          text: 'Modifier',
          handler: async () => {
            const modal = await this.modalCtrl.create({
              component: NotesComponent,
              componentProps: {
                id: this.item.id,
                nom_bar: this.item.name,
                note1:notes[0],
                note2:notes[1],
                note3:notes[2],
                commentaire:notes[3],
                action:"update",
              },
              cssClass: 'modal_recherche'
            });
            modal.onDidDismiss().then((data) => {
              this.update_notes_com();
            });
            await modal.present();
          }
        }
      ]
    });
    await alert.present();
  }

  loadData(e){
    setTimeout(() => {
      this.offset+=3;
      this.getCom(false);
      e.target.complete();
    }, 500);
  }

  async update_notes_com(){
    await this.getNotes();
    this.offset=0;
    await this.getCom(true);
  }

}
