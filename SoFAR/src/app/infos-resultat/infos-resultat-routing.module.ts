import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfosResultatPage } from './infos-resultat.page';
import { FavorisPage } from '../favoris/favoris.page';

const routes: Routes = [
  {
    path: '',
    component: InfosResultatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfosResultatPageRoutingModule {}
