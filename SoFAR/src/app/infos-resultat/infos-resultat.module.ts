import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfosResultatPageRoutingModule } from './infos-resultat-routing.module';

import { InfosResultatPage } from './infos-resultat.page';
import { RouterModule } from '@angular/router';
import {RecherchePageModule} from "../recherche/recherche.module";
import {NotesComponent} from "./notes/notes.component";
import {IonicRatingModule} from "ionic4-rating";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        InfosResultatPageRoutingModule,
        RouterModule.forChild([
            {
                path: '',
                component: InfosResultatPage
            }
        ]),
        RecherchePageModule,
        IonicRatingModule
    ],
  declarations: [InfosResultatPage, NotesComponent]
})
export class InfosResultatPageModule {}
