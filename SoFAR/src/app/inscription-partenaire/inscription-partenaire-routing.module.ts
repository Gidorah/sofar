import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriptionPartenairePage } from './inscription-partenaire.page';

const routes: Routes = [
  {
    path: '',
    component: InscriptionPartenairePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptionPartenairePageRoutingModule {}
