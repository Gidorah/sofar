import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriptionPartenairePageRoutingModule } from './inscription-partenaire-routing.module';

import { InscriptionPartenairePage } from './inscription-partenaire.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscriptionPartenairePageRoutingModule
  ],
  declarations: [InscriptionPartenairePage]
})
export class InscriptionPartenairePageModule {}
