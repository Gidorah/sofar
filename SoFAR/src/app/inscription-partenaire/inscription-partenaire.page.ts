import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {ToastController, AlertController} from '@ionic/angular';
import {GlobalVariable} from "../global";

@Component({
    selector: 'app-inscription-partenaire',
    templateUrl: './inscription-partenaire.page.html',
    styleUrls: ['./inscription-partenaire.page.scss'],
})
export class InscriptionPartenairePage implements OnInit {

    pro: any = {
        nom: "",
        email: "",
        siret: "",
        action: "",
        cat: "",
        adresse: "",
    };

    oktext=GlobalVariable.OKTEXT;
    canceltext=GlobalVariable.CANCELTEXT;

    constructor(public router: Router,
                public alert: AlertController,
                public toast: ToastController,
                public http: HttpClient,) {
    }

    ngOnInit() {
    }

    /**Récupération des champs du formulaire du nouveau client pro et test de validité pour un envoi d'email à soFar  */
    insert() {
        if (!(this.pro['nom'] !== "" && this.pro['email'] !== "" && this.pro['cat'] !== "" && this.pro['adresse'] !== "" && this.pro['siret'] !== "")) {
            this.showToast('Tous les champs sont requis');
        } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.pro["email"])) {
            this.showToast("Le mail est invalide");
        } else {
            let headers: any = new HttpHeaders({'Content-Type': 'application/json'});
            this.pro['action'] = 'insert';
            this.http.post(GlobalVariable.BASE_API_URL + "/sofar_api/Pro/manage_data.php", JSON.stringify(this.pro), {
                headers : {
                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe(data => {
                console.log(data['send status']);
                    if (data['send status'] === true) {
                        this.showAlert();
                    } else if (data['status'] === 'existing-account') {
                        this.showToast('Un compte a déjà été crée avec cet email.');
                    } else {
                        this.showToast('Une erreur est survenue !');
                    }
                },
                (error: any) => {
                    console.log(error);
                });
        }
    }

    async showToast(message) {
        let toast = await this.toast.create({
            message: message,
            duration: 2000
        });
        await toast.present();
    }

    /**Alert pour notifier le pro que SoFar le recontate au plus vite */
    async showAlert() {
        let alert = await this.alert.create({
            header: 'On revient !',
            message: 'L\'équipe SoFar te recontacte au plus vite pour activer ton compte ',
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.router.navigate(['/connexion-client']);
                    }
                }
            ]
        })
        await alert.present();
    }

}
