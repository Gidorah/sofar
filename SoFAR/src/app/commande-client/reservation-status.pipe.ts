import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'reservationStatus'
})
export class ReservationStatusPipe implements PipeTransform {

    transform(status: string): string {
        if (status === '0') {
            return 'En attente de confirmation';
        } else if (status === '1') {
            return 'Confirmée !';
        } else if (status === '2') {
            return 'Déclinée';
        }
        return '';
    }

}
