import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from "@ionic/angular";

@Component({
    selector: 'app-details-commande',
    templateUrl: './details-commande.component.html',
    styleUrls: ['./details-commande.component.scss'],
})
export class DetailsCommandeComponent implements OnInit {

    order;

    constructor(
        private navParams: NavParams,
        private modalController: ModalController,
    ) {
    }

    ngOnInit() {
        this.order = this.navParams.get('order');
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

}
