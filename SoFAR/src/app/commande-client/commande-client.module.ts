import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommandeClientPageRoutingModule } from './commande-client-routing.module';

import { CommandeClientPage } from './commande-client.page';
import {RedirectionClientComponent} from "../redirection-client/redirection-client.component";
import { ReservationStatusPipe } from './reservation-status.pipe';
import {DetailsCommandeComponent} from "./details-commande/details-commande.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CommandeClientPageRoutingModule
    ],
    exports: [
        RedirectionClientComponent
    ],
    declarations: [CommandeClientPage, RedirectionClientComponent, ReservationStatusPipe,
        DetailsCommandeComponent]
})
export class CommandeClientPageModule {}
