import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommandeClientPage } from './commande-client.page';

describe('CommandeClientPage', () => {
  let component: CommandeClientPage;
  let fixture: ComponentFixture<CommandeClientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandeClientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommandeClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
