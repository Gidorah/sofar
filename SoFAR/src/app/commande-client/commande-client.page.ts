import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, ToastController} from '@ionic/angular';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../storage.service';
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {User} from "../user/user";
import {UserService} from "../user/user.service";
import {Produit} from "../commander/produit";
import {BasketDetailsComponent} from "../commander/basket-details/basket-details.component";
import {DetailsCommandeComponent} from "./details-commande/details-commande.component";
import {CommanderService} from "../commander/commander.service";


@Component({
    selector: 'app-commande-client',
    templateUrl: './commande-client.page.html',
    styleUrls: ['./commande-client.page.scss'],
})
export class CommandeClientPage implements OnInit {

    segmentRC = '1';
    segmentC = '3';
    segmentR = '6';

    user: User = null;

    chargement;
    chargement_resa;

    pros: Array<{
        id: number,
        nom: string,
        adresse: string,
    }> = [];
    commandes: Array<{
        infos: {
            createdAt: Date,
            id: string,
            items: { id: string, article_id: string, quantity: string, article_nom: string, article_prix: string, article_taxe: string, order_id: string, total: string, pro_service: string, },
            order_status: string,
            pourboire: string,
            pro_id: string,
            pro_nom: string,
            pro_service: string,
            stripe_source: string,
            taxe: string,
            total: string,
            updatedAt: string,
            user_id: string,
            user_nom: string,
            user_prenom: string,
        }
        image: string
    }> = [];
    commandesEnCours: any;
    commandesEnPreparation: any;
    commandesServies: any;
    basket: Array<{
        id: string,
        nom: string,
        prix: string,
        description: string,
        dispo: string,
        cat: string,
        id_pro: number,
        quantite: number,
        photo: string,
        id_comm: number,
    }> = [];
    reservations: Array<{
        id: string,
        article: string,
        date: string,
        heure: string,
        nbPerso: number,
        validation: number,
        etat: string,
        id_user: number,
        id_pro: number,
        nom_pro: string,
        img: any
    }> = [];

    reservationsAVenir: any;
    reservationsPasses: any;

    constructor(public modalCtrl: ModalController,
                public http: HttpClient,
                public storageservice: StorageService,
                private router: Router,
                private alertController: AlertController,
                private toast: ToastController,
                private userService: UserService,
                private commanderService: CommanderService) {
    }

    ngOnInit() {
        this.userService.getUser().subscribe((user: User) => {
            this.user = user;
        });
    }

    /** Récupération des commandes et réservations de l'utlisateur */
    ionViewWillEnter() {
        if (this.user) {
            console.log('Getting reservation of :', this.user);
            this.getCommandes();
            this.getReservations();
        }
    }

    /**Changement de section */
    segmentChanged(event: any, segment) {
        segment = event.target.value;
    }

    /**création d'une commande avec les articles à recommander */
    async recommander(commande) {
        /*for (let i = 0; detail[i]; i++) {
            this.basket.push({
                id: detail[i][0],
                nom: detail[i][1],
                prix: detail[i][2],
                description: detail[i][3],
                dispo: detail[i][4],
                cat: detail[i][5],
                id_pro: detail[i][7],
                quantite: detail[i][11],
                photo: 'data:image/jpeg;base64,' + detail[i][6],
                id_comm: 0,
            })
        }
        let item = {
            id: idPro
        }*/
        let panier_recommander;
        this.commanderService.idPro = commande.pro_id;
        this.commanderService.initPanier().subscribe((panier: Produit[]) => {
            panier_recommander = panier;
        });
        for (let prod of commande.items) {
            const produit: Produit = {
                quantity: prod.quantity,
                article: {
                    article_categorie: 0,
                    article_desc: "",
                    article_dispo: true,
                    article_id: prod.article_id,
                    article_nom: prod.article_nom,
                    article_photo: "",
                    article_prix: prod.article_prix,
                    article_taxe: prod.article_taxe,
                    id_pro: commande.pro_id
                }
            };
            this.commanderService.addToPanier(produit);
        }
        const modal = await this.modalCtrl.create({
            component: BasketDetailsComponent
        });
        return await modal.present();
    }

    /**Récupération des commandes de la base */
    getCommandes() {
        this.chargement = true;
        this.userService.getTokenObs().then(async (tok) => {
            const body = {
                token: tok,
                action: 'fetch-all'
            };
            await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/retrieve_data.php', JSON.stringify(body)).subscribe(
                async (data: any) => {
                    this.commandes = [];
                    for (let i = 0; data['orders'][i]; i++) {
                        let image = "../assets/images/logo_dark.svg";
                        if (data.pros[data.orders[i].pro_id].photo_pro !== "") {
                            image = 'data:image/jpeg;base64,' + data.pros[data.orders[i].pro_id].photo_pro;
                        }
                        this.commandes.push({
                            infos: data['orders'][i],
                            image: image,
                        });
                    }
                    this.commandesEnPreparation = this.commandes.filter(commande => commande.infos.order_status === 'payed' || commande.infos.order_status === 'creation');
                    this.commandesServies = this.commandes.filter(commande => commande.infos.order_status === 'served' || commande.infos.order_status === 'ready' || commande.infos.order_status === 'refused');
                    this.chargement = false;
                }, (error: any) => {
                    console.log(error);
                });
        });
    }

    /**Récupération des réservations de la base */
    getReservations() {
        this.chargement_resa = true;
        this.userService.getTokenObs().then(token => {
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/retrieve_data.php', {token: token}, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe(
                async (data: any) => {
                    this.reservations = [];
                    for (let i = 0; data[i]; i++) {
                        let image = "../assets/images/logo_dark.svg";
                        // let res:any=await this.storageservice.fetchProPhoto(data[i].id_pro).toPromise();
                        if (data.pros[data[i].id_pro].photo_pro !== "") {
                            image = 'data:image/jpeg;base64,' + data.pros[data[i].id_pro].photo_pro;
                        }
                        console.log('taille image' + data.pros[data[i].id_pro].nom_pro + ' --- ' + image.length);
                        this.reservations.push({
                            id: data[i].reservation_id,
                            article: data[i].nom_article,
                            date: data[i].date_res,
                            heure: data[i].heure_res,
                            nbPerso: data[i].nb_perso_res,
                            validation: data[i].validation_res,
                            etat: data[i].etat_res,
                            id_user: data[i].id_user,
                            id_pro: data[i].id_pro,
                            nom_pro: data.pros[data[i].id_pro].nom_pro,
                            img: image
                        });
                    }
                    this.reservationsAVenir = this.reservations.filter(reservation => reservation.etat === 'A venir' || reservation.etat === 'refus');
                    this.reservationsPasses = this.reservations.filter(reservation => reservation.etat === 'Passée');
                    this.chargement_resa = false;
                }, (error: any) => {
                    console.log(error);
                });
        });
    }

    async cancelRes(res: any) {
        const alert = await this.alertController.create({
            header: 'Annuler ?',
            message: 'Voulez-vous annuler votre réservation ?',
            buttons: [
                {
                    text: 'Non',
                    role: 'cancel',
                    handler: () => {
                        alert.dismiss();
                    }
                }, {
                    text: 'Oui',
                    handler: () => {
                        this.userService.getTokenObs().then(tok => {
                            const body = {
                                token: tok,
                                id_res: res.id,
                                action: 'cancel'
                            };
                            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/manage_data.php', body, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).subscribe((data: any) => {
                                this.getReservations();
                                if (data.error === '') {
                                    this.showToast('Réservation annulée !', 2000);
                                } else {
                                    this.showToast('Une erreur est survenue', 2000);
                                }
                            });
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async showToast(msg, temps) {
        const toast = await this.toast.create({
            message: msg,
            duration: temps
        });
        await toast.present();
    }

    refreshCommandes(event: any) {
        this.getCommandes();
        event.target.complete();
    }

    refreshReservations(event: any) {
        this.getReservations();
        event.target.complete();
    }

    async showdetails(commande) {
        const modal = await this.modalCtrl.create({
            component: DetailsCommandeComponent,
            componentProps: {order: commande}
        });

        await modal.present();
    }
}
