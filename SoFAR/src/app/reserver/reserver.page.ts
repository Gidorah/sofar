import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams, ToastController} from '@ionic/angular';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from '../storage.service';
import {Ionic4DatepickerModalComponent} from "@logisticinfotech/ionic4-datepicker";
import {GlobalVariable} from "../global";
import {UserService} from "../user/user.service";
import {User} from "../user/user";
import {NotificationService} from "../notification.service";


@Component({
    selector: 'app-reserver',
    templateUrl: './reserver.page.html',
    styleUrls: ['./reserver.page.scss'],
})
export class ReserverPage implements OnInit {
    public images: any = [];
    public MyDate;
    NbPers: Array<number>;
    places: Array<string>;
    IsBar: boolean;
    item: { name: string, description: string, categorie: string, image: string, id: number, terrasse: number, need_resa_details: boolean };
    // reserve : objet décrivant une réservation
    reserve: any = {};
    // contient les boissons disponibles à la ventes pour la première consommation
    carte: any = {};
    nbPers: number = 0;
    consigne = "";
    minTime = "";
    maxTime = "";
    delais;

    minTime_date: any;
    maxTime_date: any;

    disabledDates: Array<String> = new Array<String>();

    minutes = [0, 15, 30, 45];
    heures: Array<number> = [];

    date;

    datePickerObj: any;

    heure_limite;

    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;

    otherParticipants: Array<{
        email: string;
        i: number;
        checked: boolean;
    }>;
    otherParticipantsChecked: boolean;


    constructor(public modalCtrl: ModalController, navParams: NavParams,
                public toastCtrl: ToastController,
                public http: HttpClient, public toast: ToastController,
                public storageservice: StorageService,
                private userService: UserService,
                private notificationService: NotificationService) {
        this.item = navParams.get('item');
    }

    /**
     * accordéon permet de cacher la carte de dispoition de table du restaurant
     */
    expandItem(criterion, items): void {
        if (criterion.expanded) {
            criterion.expanded = false;
        } else {
            items.map(listItem => {
                if (criterion === listItem) {
                    listItem.expanded = !listItem.expanded;
                } else {
                    listItem.expanded = false;
                }
                return listItem;
            });
        }
    }

    /**
     * récupérer l'id de l'article réservé avec la variable évenementielle
     */

    async onInputCarte(event) {
        const saisie = event.srcElement.value;
        if (!saisie) {
            return;
        }
        this.reserve['idArtRes'] = saisie.split(' ')[0];
    }

    /**
     * récupérer la date de la réservation avec la variable évenementielle
     */
    async onInputdateRes(event) {
        const saisie = event.srcElement.value;
        if (!saisie) {
            return;
        }

        this.reserve['dateRes'] = this.date;
    }

    /**
     * récupérer le nombre de personnes ayant réservé avec la variable évenementielle
     */
    async onInputnbPers(event) {
        const saisie = event.srcElement.value;
        if (!saisie) {
            return;
        }
        this.otherParticipants = [];
        if (this.item.need_resa_details) {
            for (let i = 0; i < saisie - 1; i++) {
                this.otherParticipants.push({
                    email: '',
                    i,
                    checked: false
                });
            }
        }
        this.otherParticipantsChecked = false;
        this.reserve['nbPers'] = saisie;
    }

    async onInputemplacement(event) {
        const saisie = event.srcElement.value;
        if (!saisie) {
            return;
        }
        this.reserve['emplacement'] = saisie;
    }

    async ngOnInit() {
        this.reserve.nbPers = 1;
        this.otherParticipants = [];
        this.otherParticipantsChecked = false;
        let infos = await this.getHtml_delais(this.item['id']);
        this.delais = infos[0][1];
        this.heure_limite = infos[0][2];
        let date_actuelle = new Date();
        this.minTime_date = this.getdate(date_actuelle);
        this.reserve['dateRes'] = this.minTime_date;
        date_actuelle.setDate(date_actuelle.getDate() + 365); //On affiche le calendrier jusqu'à un an après
        this.maxTime_date = this.getdate(date_actuelle);

        this.IsBar = true;

        //Desactiver toutes les dates au dela du délais (on le fait manuellement pour pouvoir réactiver certaines d'entre elles)

        let jour_a_desactiver = 365 - this.delais;

        let date = new Date();
        let annee;
        let jour;
        let mois;
        let jour_string;
        let mois_string;

        let date_jours_ferme = date;

        for (let i = 0; i < this.delais; i++) {
            let jour_semaine = date_jours_ferme.getDay();

            let horaires;

            if (jour_semaine == 0) {// dimanche
                horaires = this.item['horaires_semaine'][6];
            } else {
                horaires = this.item['horaires_semaine'][jour_semaine - 1];
            }

            if (horaires == "Fermé") {
                this.disabledDates.push(this.getdate(date_jours_ferme));

            }
            date_jours_ferme.setDate(date_jours_ferme.getDate() + 1);
        }
        if (parseInt(this.heure_limite.split(":")[0]) <= new Date().getHours()) {
            if (parseInt(this.heure_limite.split(":")[0]) == new Date().getHours()) {
                if (parseInt(this.heure_limite.split(":")[1]) <= new Date().getMinutes()) {
                    this.disabledDates.push(this.getdate(new Date()));
                }
            } else {
                this.disabledDates.push(this.getdate(new Date()));
            }

        }

        await this.supprimer_jours();

        await this.ajouter_jours();

        let date1 = new Date(this.reserve['dateRes']);

        while (this.isDisabledDate(this.getdate(date1))) {
            date1.setDate(date1.getDate() + 1)
            this.reserve['dateRes'] = this.getdate(date1);
        }

        let inputdate = this.reserve['dateRes'];

        if (jour_a_desactiver > 0) {
            date = new Date();
            date.setDate(date.getDate() + parseInt(this.delais)); //on désactive les dates après le délais choisi
            for (let i = 0; i < jour_a_desactiver; i++) {
                date.setDate(date.getDate() + 1);
                this.disabledDates.push(this.getdate(date));
            }
        }

        this.supprimer_heures(inputdate);

        this.maj_heures();


        this.datePickerObj = {
            inputDate: inputdate, // default new Date()
            fromDate: new Date(inputdate), // default null
            toDate: new Date(this.maxTime_date), // default null
            showTodayButton: false, // default true
            //closeOnSelect: true, // default false
            disableWeekDays: [], // default []
            mondayFirst: true, // default false
            closeLabel: 'Retour', // default 'Close'
            setLabel: 'Valider',  // default 'Set'
            todayLabel: 'Aujourdhui', // default 'Today
            disabledDates: this.disabledDates, // default []
            titleLabel: 'Selectionner une date', // default null
            monthsList: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
            weeksList: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
            dateFormat: 'YYYY-MM-DD', // default DD MMM YYYY
            clearButton: false, // default true
            //momentLocale: 'fr-FR', // Default 'en-US'
            yearInAscending: true, // Default false
            //btnCloseSetInReverse: true, // Default false
            btnProperties: {
                expand: 'block', // Default 'block'
                fill: '', // Default 'solid'
                size: '', // Default 'default'
                disabled: '', // Default false
                strong: '', // Default false
                color: 'red' // Default ''
            },
            arrowNextPrev: {
                nextArrowSrc: 'assets/images/next.svg',
                prevArrowSrc: 'assets/images/previous.svg'
            }, // This object supports only SVG files.
            /*highlightedDates: [
              { date: new Date('2021-06-16'), color: '#ee88bf', fontColor: '#fff' },
              { date: new Date('2021-06-15'), color: '#50f2b1', fontColor: '#fff' }
            ], // Default [],*/
        };

        this.NbPers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        this.reserve['idArtRes'] = '0';
        //disposition avec les numéros de table
        //this.places=['A l\' intérieur', 'A l\'extérieur', 'Table 1', 'Table 2', 'Table 3', 'Table 4']
        this.places = ['A l\' intérieur', 'A l\'extérieur'];
        if (this.item['terrasse'] == 0) {
            this.reserve['emplacement'] = "Interieur";
        }
        this.reserve['idArtRes'] = '0';

        const element = {
            id: this.item["id"],
        };
        // this.carte = this.getHtml(element);
        //itérateur pour l'accordéon
        this.images = [
            {expanded: false}
        ];

    }

    pop() {
        this.modalCtrl.dismiss();
    }

    isBar(categ) {
        return categ == 'bar';
    }

    /**
     * traitement de l'objet reserve avant le post vers la base
     */
    async validation() {

        if (this.reserve['nbPers'] === undefined) {
            this.showToast('Le nombre de personne est non saisi');
        } else if (this.reserve['dateRes'] === undefined) {
            this.showToast('La date est non saisie');
        } else if (this.reserve['heureRes'] === undefined) {
            this.showToast('L\'heure est non saisie');
        } else if (this.reserve['emplacement'] === undefined) {
            this.showToast('L\'emplacement est non saisi');
        } else {
            if (this.consigne == '') {
                this.reserve['consigne'] = "Aucune"
            } else {
                this.reserve['consigne'] = this.consigne;
            }
            this.reserve['validRes'] = '0';
            this.reserve['etatRes'] = 'A venir';

            this.reserve['idp'] = this.item['id'];
            // this.reserve['idu'] = this.storageservice.getUser()["id"];
            this.reserve['idu'] = this.userService.getUserValue().id;

            if (this.item.need_resa_details && this.reserve.nbPers > 1) {
                await this.checkOtherP();
                this.reserve.participants = this.otherParticipants;
            }
            this.userService.getTokenObs().then(tok => {
                this.notificationService.SendMessage("Nouvelle demande de réservation !", "Le " + this.reserve['dateRes'] + " à " + this.reserve['heureRes'] + " !", this.item["id"], "professionel", tok);
            });

            this.postHtml(this.reserve);


        }


    }

    postHtml(element) {
        this.userService.getTokenObs().then(tok => {
            console.log(tok);
            const body = {
                token: tok,
                elt: element,
                action: 'insert'
            };
            const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/manage_data.php', body, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe(async (data: any) => {
                if (data.error === '') {
                    await this.showToast('Réservation renseignée');
                    this.pop();
                } else {
                    await this.showToast('Une erreur est survenue');
                }
            }, (error: any) => {
                console.log(error);
            });
        });


    }

    supprimer_elem(heures: Array<number>, element: number) {
        for (var i = 0; i < this.heures.length; i++) {

            if (this.heures[i] === element) {
                this.heures.splice(i, 1);
                i--;
            }
        }
        return heures
    }

    /**
     * Notifier si la réservation a été effectuée
     */
    async showToast(message) {
        let toast = await this.toast.create({
            message: message,
            duration: 2000
        });
        await toast.present();
    }

    async getHtml_except(item) {
        return await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Exceptions_reservations/retrieve_data.php', JSON.stringify(item), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).toPromise();

    }

    async supprimer_heures(jour) {
        let id = this.item["id"];

        let blocked;

        const body = {
            id: id,
            action: "heures",
            jour: jour,
            type: 'retrait'
        }

        blocked = await this.getHtml_except(body);

        console.log(blocked);

        for (let j of blocked) {
            let debut_creneau: number = parseInt(j[1].split(":")[0]);
            let fin_creneau: number = parseInt(j[1].split("-")[1].split(":")[0]);
            for (let element = debut_creneau; element < fin_creneau; element++) {
                this.heures = this.supprimer_elem(this.heures, element);
            }
        }


        let heure_mini;
        if (this.heures[0] < 10) {
            heure_mini = "0" + this.heures[0].toString();
        } else {
            heure_mini = this.heures[0].toString();
        }

        if (heure_mini<=parseInt(this.minTime.split(":")[0])) {
            this.reserve['heureRes'] = this.minTime;
        } else {
            this.reserve['heureRes'] = heure_mini + ":00";
        }
    }

    async supprimer_jours() {
        let id = this.item["id"];

        let blocked;

        const body = {
            id: id,
            action: "jours",
            type: 'retrait'
        }

        blocked = await this.getHtml_except(body);

        for (let j of blocked) {
            this.disabledDates.push(j[0]);
        }

    }

    async ajouter_jours() {
        let id = this.item["id"];

        let blocked;

        const body = {
            id: id,
            action: "jours",
            type: 'ajout'
        }

        blocked = await this.getHtml_except(body);

        for (let j of blocked) {
            this.retirer(j[0])
        }

    }

    async openDatePicker() {
        this.heures = [];
        for (let i = parseInt(this.minTime); i < (parseInt(this.maxTime) + 1); i++) {
            this.heures.push(i);
        }
        const datePickerModal = await this.modalCtrl.create({
            component: Ionic4DatepickerModalComponent,
            cssClass: 'li-ionic4-datePicker',
            componentProps: {
                'objConfig': this.datePickerObj,
                'selectedDate': this.reserve['dateRes'],
            }
        });
        await datePickerModal.present();

        datePickerModal.onDidDismiss().then((data) => {
            if (data.data.date != "Invalid date") {
                this.reserve['dateRes'] = data.data.date;
                this.supprimer_heures(this.reserve['dateRes']);
                this.maj_heures();
            }
        });
    }

    retirer(jour) {
        for (let i = 0; i < this.disabledDates.length; i++) {
            if (this.disabledDates[i] === jour) {
                this.disabledDates.splice(i, 1);
            }
        }
    }

    maj_heures() {
        this.heures = [];
        let jour_semaine = (new Date(this.reserve['dateRes'])).getDay(); //getDay renvoie 0 pour dimanche, 1 pour lundi, ..., 6 pour samedi

        let ouverture;
        let fermeture;
        let ouverture_soir;
        let fermeture_soir;
        let horaires;
        let horaires_veille;

        if (jour_semaine == 0) {// dimanche
            horaires = this.item['horaires_semaine'][6];
        } else {
            horaires = this.item['horaires_semaine'][jour_semaine - 1];
        }

        //--------- On fait ça pour regarder si la veille fermait après minuit (auquel cas on ajoute les horaires correspondants)

        if (jour_semaine-1 == 0) {// lundi (càd la veille c'était dimanche)
            horaires_veille = this.item['horaires_semaine'][6];
        } else if(jour_semaine == 0) {
            horaires_veille = this.item['horaires_semaine'][5];
        }
        else{
            horaires_veille = this.item['horaires_semaine'][jour_semaine - 2];
        }

        let fermeture_veille="42";

        if(horaires_veille!="Fermé"){
            if (horaires_veille.split("/") != horaires_veille) {
                fermeture_veille=horaires_veille.split("/")[1].split("-")[1];
            }
            else{
                fermeture_veille=horaires_veille.split("-")[1];
            }
        }

        //-----------

        ouverture = horaires.split("-")[0];
        fermeture = horaires.split("-")[1];
        if(parseInt(fermeture)<9){
            fermeture="23:45";
        }

        if(parseInt(fermeture_veille)<10){
            this.minTime = "00:00";
            for (let i = 0; i < (parseInt(fermeture_veille)); i++) {
                this.heures.push(i);
            }
        }
        else{
            this.minTime = ouverture;
        }
        this.maxTime = fermeture;
        if(this.reserve['dateRes']==this.getdate(new Date())){
            let date_secondes = (new Date().getHours())*60*60 + (new Date().getMinutes())*60;
            let ouverture_secondes = parseInt(ouverture.split(":")[0])*60*60+parseInt(ouverture.split(":")[1])*60;

            if(date_secondes>ouverture_secondes){
                let heures;
                let minutes_number=new Date().getMinutes()+15;
                let heures_number=new Date().getHours();
                let minutes;
                if(minutes_number<15){
                    minutes='00';
                }
                else if(minutes_number < 30){
                    minutes='15';
                }
                else if(minutes_number < 45){
                    minutes='30';
                }
                else if(minutes_number < 60){
                    minutes='45';
                }
                else{
                    minutes='00';
                    heures_number=heures_number+1;
                }

                if(heures_number<10){
                    heures="0"+heures_number;
                }
                else{
                    heures=heures_number;
                }

                this.minTime = heures+":"+minutes;
            }
        }


        for (let i = parseInt(ouverture); i < (parseInt(fermeture) + 1); i++) {
            this.heures.push(i);
        }

        if (horaires.split("/") != horaires) {
            ouverture_soir = horaires.split("/")[1].split("-")[0];
            fermeture_soir = horaires.split("/")[1].split("-")[1];
            if(parseInt(fermeture_soir)<10){
                fermeture_soir="23:45";
            }
            this.maxTime = fermeture_soir;
            for (let i = parseInt(ouverture_soir); i < (parseInt(fermeture_soir) + 1); i++) {
                this.heures.push(i);
            }
        }

    }

    getdate(date_actuelle) {
        let annee_actuelle = date_actuelle.getFullYear();
        let mois_actuel = (date_actuelle.getMonth() + 1).toString();
        if (mois_actuel.length < 2) {
            mois_actuel = "0" + mois_actuel.toString();
        }
        let jour_actuel = (date_actuelle.getDate()).toString();
        if (jour_actuel.length < 2) {
            jour_actuel = "0" + jour_actuel.toString();
        }
        return (annee_actuelle + "-" + mois_actuel + "-" + jour_actuel);

    }

    isDisabledDate(date) {
        for (let i = 0; i < this.disabledDates.length; i++) {
            if (this.disabledDates[i] === date) {
                return true;
            }
        }
        return false;
    }

    async getHtml_delais(item) {
        return await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Delais/retrieve_data.php', JSON.stringify(item), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).toPromise();
    }

    async checkOtherP() {
        console.log('other participants:', this.otherParticipants);
        /*
        Check si toutes les adresses email rentrées sont syntaxiquement correctes
         */
        const user: User = this.userService.getUserValue();
        for (const op of this.otherParticipants) {
            if (!this.isEmail(op.email)) {
                await this.showToast('Rentrer une adresse correcte pour le participant ' + (op.i + 1));
                return;
            } else {
                /*
                Check si l'email rentré n'est pas celui de l'utilisateur prenant la réservation
                 */
                if (user.email === op.email) {
                    await this.showToast('Vous ne pouvez pas rentrer votre propre email (participant ' + (op.i + 1) + ').');
                    return;
                }
            }
        }
        /*
        Check si pas de doublon de mail
         */
        for (const op1 of this.otherParticipants) {
            for (const op2 of this.otherParticipants) {
                if (op1.email === op2.email && op1.i !== op2.i) {
                    await this.showToast('Les participants ' + (op1.i + 1) + ' et ' + (op2.i + 1) + ' ont le même email.');
                    return;
                }
            }
        }
        /*
        Check avec le serveur si les adresses correspondent à un compte So FAR existant
         */
        this.userService.getTokenObs().then((tok: string) => {
            const body = {
                token: tok,
                participants: this.otherParticipants
            };
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/check_participants.php', body, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe((data: any) => {
                if (data.error === '') {
                    /*
                    On est si tout les participants sont valides (leur attributs checked = true)
                     */
                    let everyOneChecked = true;
                    for (const op of data.participants) {
                        this.otherParticipants[op.i].checked = op.checked;
                        everyOneChecked = everyOneChecked && op.checked;
                    }
                    this.otherParticipantsChecked = !!everyOneChecked;
                    if (!this.otherParticipantsChecked) {
                        this.showToast('Certains participants ont un adresse email inconnu ou incorrecte');
                    }
                } else {
                    console.log('Une erreur est survenue', data.error);
                }
            }, (error: any) => {
                console.log('Error in reserver.checkOtherP():', error);
            });
        });
    }

    isEmail(email) {
        const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return re.test(email);
    }
}
