import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionPersonnelPage } from './gestion-personnel.page';

const routes: Routes = [
  {
    path: '',
    component: GestionPersonnelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionPersonnelPageRoutingModule {}
