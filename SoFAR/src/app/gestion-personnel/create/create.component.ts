import {Component, OnInit} from '@angular/core';
import {AlertController, LoadingController, ModalController, ToastController} from "@ionic/angular";
import {GlobalVariable} from "../../global";
import {HttpClient} from "@angular/common/http";
import {StorageService} from "../../storage.service";
import {ProService} from "../../pro/pro.service";

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {

    name = '';
    password = '';

    constructor(
        private modalController: ModalController,
        private toast: ToastController,
        private loadingController: LoadingController,
        private http: HttpClient,
        private proService: ProService,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    async onSubmit() {
        const loading = await this.loadingController.create({
            cssClass: 'loading-class',
            message: 'Veuillez patienter...',
        });
        if (this.name === '') {
            this.showToast('Le nom n\'est pas valide', 2000);
        } else if (this.name.length > 20) {
            this.showToast('Le nom est trop long', 2000);
        } else if (this.password.length > 20) {
            this.showToast('Le mot de passe est trop long', 2000);
        } else if (this.password !== '' && this.password.length < 6) {
            this.showToast('Le mot de passe est trop court (min 6 caractères)', 4000);
        } else {
            // tout est valide
            this.proService.getTokenProObs().then(tok => {
                const body = {
                    token: tok,
                    action: 'create',
                    alias: this.name,
                    pw: this.password
                };
                this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_personnel.php', body, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe((res: any) => {
                    const err = res.error;
                    loading.dismiss();
                    if (err === '') {
                        this.showToast('Serveur enregistré', 2000);
                        this.finalDialog(res.result.login, res.result.password);
                    } else {
                        this.showToast('Oups ! Un problème est survenu', 1500);
                    }
                });
            });
        }
    }

    async finalDialog(login: string, password: string) {
        const alert = await this.alert.create({
            header: 'Le compte serveur à été créé avec succès',
            message: `
                <p>Le compte est accessible à l'aide des identifiants suivants :</p>
                <ul>
                    <li>ID: ` + login + `</span> </li>
                    <li>Mot de passe: ` + password + `</span></li>
                </ul>
                <p>Attention: le mot de passe ne sera plus visible après la fermeture de cette fenêtre.</p>
            `,
            buttons: [
                {
                    text: 'OK',
                    handler: (alertData) => {
                        this.onCancel();
                    }
                }
            ]
        });
        await alert.present();
    }

    getName(event) {
        this.name = event.target.value;
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    getPassword(event) {
        this.password = event.target.value;
    }
}
