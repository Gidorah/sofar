import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GestionPersonnelPageRoutingModule } from './gestion-personnel-routing.module';

import { GestionPersonnelPage } from './gestion-personnel.page';
import {CreateComponent} from './create/create.component';
import {ModifyComponent} from './modify/modify.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GestionPersonnelPageRoutingModule
  ],
  declarations: [GestionPersonnelPage, CreateComponent, ModifyComponent]
})
export class GestionPersonnelPageModule {}
