import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, ToastController} from "@ionic/angular";
import {CreateComponent} from "./create/create.component";
import {StorageService} from "../storage.service";
import {GlobalVariable} from "../global";
import {HttpClient} from "@angular/common/http";
import {ModifyComponent} from "./modify/modify.component";
import {ProService} from "../pro/pro.service";

@Component({
    selector: 'app-gestion-personnel',
    templateUrl: './gestion-personnel.page.html',
    styleUrls: ['./gestion-personnel.page.scss'],
})
export class GestionPersonnelPage implements OnInit {

    personnels: [{ id_per, login, alias }];

    constructor(
        private modalCtrl: ModalController,
        private storageService: StorageService,
        private http: HttpClient,
        private toast: ToastController,
        private alert: AlertController,
        private proService: ProService
    ) {
    }

    ionViewWillEnter() {
        this.fetchPers();
    }

    fetchPers() {
        this.proService.getTokenProObs().then(tok => {
            const body = {
                token: tok,
                action: 'fetch-all'
            };
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_personnel.php', body, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe((res: any) => {
                const err = res.error;
                if (err === '') {
                    this.personnels = res.result;
                    console.log('Personnels : ', this.personnels);
                } else {
                    this.showToast('Oups ! Un problème est survenu', 1500);
                }
            });
        });
    }

    async createPer() {
        const modal = await this.modalCtrl.create({
            component: CreateComponent
        });
        modal.onDidDismiss().then(data => {
            this.fetchPers();
        });
        await modal.present();
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    async openPers(idPer) {
        const modal = await this.modalCtrl.create({
            component: ModifyComponent,
            componentProps: {id_per: idPer}
        });
        modal.onDidDismiss().then(data => {
            this.fetchPers();
        });
        await modal.present();
    }

    async howAccessPers() {
        const alert = await this.alert.create({
            header: 'Accès à un compte serveur',
            message: `
                <p>Pour accéder à un compte serveur, il faut renseigner :</p>
                <ul>
                    <li>L'ID du compte serveur</span> </li>
                    <li>Le mot de passe associé</span></li>
                </ul>
                <p>sur la fenêtre de connexion partenaire.</p>
            `,
            buttons: [
                {
                    text: 'OK',
                    role: 'cancel'
                }
            ]
        });
        await alert.present();
    }

    ngOnInit(): void {
    }
}
