import {Component, OnInit} from '@angular/core';
import {GlobalVariable} from "../../global";
import {AlertController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {StorageService} from "../../storage.service";
import {HttpClient} from "@angular/common/http";
import {ProService} from "../../pro/pro.service";

@Component({
    selector: 'app-modify',
    templateUrl: './modify.component.html',
    styleUrls: ['./modify.component.scss'],
})
export class ModifyComponent implements OnInit {

    idPer = '';

    pers: { id_per, login, alias };

    constructor(
        private modalCtrl: ModalController,
        private proService: ProService,
        private http: HttpClient,
        private toast: ToastController,
        private navParams: NavParams,
        private modalController: ModalController,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
        this.idPer = this.navParams.get('id_per');
        this.fetchPers(this.idPer);
    }

    fetchPers(idPer) {
        this.proService.getTokenProObs().then(tok => {
            const body = {
                token: tok,
                id_per: idPer,
                action: 'fetch'
            };
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_personnel.php', body, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe((res: any) => {
                const err = res.error;
                if (err === '') {
                    this.pers = res.result;
                    console.log('Personnel : ', this.pers);
                } else {
                    this.showToast('Oups ! Un problème est survenu', 1500);
                }
            });
        });
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    onCancel() {
        this.modalController.dismiss();
    }

    async changeName() {
        const alert = await this.alert.create({
            header: 'Entrer un nouveau nom',
            inputs: [
                {
                    name: 'alias',
                    value: '',
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Confirmer',
                    handler: (alertData) => {
                        if (alertData.alias === '') {
                            this.showToast('Le nom proposé est trop court', 2000);
                        } else if (alertData.alias.length > 20) {
                            this.showToast('Le nom proposé est trop long', 2000);
                        } else {
                            this.proService.getTokenProObs().then(tok => {
                                const body = {
                                    token: tok,
                                    id_per: this.pers.id_per,
                                    action: 'change-name',
                                    alias: alertData.alias
                                };
                                this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_personnel.php', body, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                    }
                                }).subscribe((res: any) => {
                                    const err = res.error;
                                    if (err === '') {
                                        this.showToast('Nom modifié avec succès', 2000);
                                        this.pers.alias = body.alias;
                                    } else {
                                        this.showToast('Oups ! Un problème est survenu', 1500);
                                    }
                                });
                            });
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

    async changePassword() {
        const alert = await this.alert.create({
            header: 'Entrer un nouveau mot de passe',
            inputs: [
                {
                    name: 'pw',
                    value: '',
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Confirmer',
                    handler: (alertData) => {
                        if (alertData.pw === '' || alertData.pw.length < 6) {
                            this.showToast('Le mot de passe proposé est trop court', 2000);
                        } else {
                            this.proService.getTokenProObs().then(tok => {
                                const body = {
                                    token: tok,
                                    id_per: this.pers.id_per,
                                    action: 'change-password',
                                    pw: alertData.pw
                                };
                                this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_personnel.php', body, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                    }
                                }).subscribe((res: any) => {
                                    const err = res.error;
                                    if (err === '') {
                                        this.showToast('Mot de passe modifié avec succès', 2000);
                                    } else {
                                        this.showToast('Oups ! Un problème est survenu', 1500);
                                    }
                                });
                            });
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

    async deletePers() {
        const alert = await this.alert.create({
            header: 'Supprimer le compte serveur',
            message: 'Voulez vous supprimer le compte serveur de ' + this.pers.alias + ' ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Confirmer',
                    handler: (alertData) => {
                        this.proService.getTokenProObs().then(tok => {
                            const body = {
                                token: tok,
                                id_per: this.pers.id_per,
                                action: 'delete'
                            };
                            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_personnel.php', body, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).subscribe((res: any) => {
                                const err = res.error;
                                if (err === '') {
                                    this.showToast('Compte supprimé avec succès', 2000);
                                    this.onCancel();
                                } else {
                                    this.showToast('Oups ! Un problème est survenu', 1500);
                                }
                            });
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
}
