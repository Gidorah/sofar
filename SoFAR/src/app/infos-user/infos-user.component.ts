import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {StorageService} from "../storage.service";
import {GlobalVariable} from "../global";
import {HttpClient} from "@angular/common/http";
import {formatDate} from "@angular/common";
import {ProService} from "../pro/pro.service";
import {ProStatusService} from "../pro-status.service";

@Component({
    selector: 'app-infos-user',
    templateUrl: './infos-user.component.html',
    styleUrls: ['./infos-user.component.scss'],
})
export class InfosUserComponent implements OnInit {

    userId: number;
    user: any;
    age: number;

    constructor(private modalController: ModalController,
                private navParams: NavParams,
                private proService: ProService,
                private http: HttpClient,
                private loadingController: LoadingController,
                private toast: ToastController,
                private proStatusService: ProStatusService
    ) {
    }

    async ngOnInit() {
        // const loading = await this.loadingController.create({
        //     cssClass: 'loading-class',
        //     message: 'Veuillez patienter...',
        // });
        // await loading.present();
        this.userId = this.navParams.get('user_id');
        this.fetchUser().then(() => {
            // loading.dismiss();
        });
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    async fetchUser() {
        const tok = await this.proStatusService.getToken();
        if (!tok) {
            return;
        }
        const body = {
            token: tok,
            user_id: this.userId
        };
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/fetch_user.php', body, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe((data: any) => {
            if (data.error === '') {
                this.user = data.user;
                console.log('info user', this.user);
                this.age = this.getAgeInYears(this.user.date_naissance_u);
            } else {
                this.showToast('Une erreur s\'est produite!', 1000);
                if (!this.user) {
                    this.onCancel();
                }
            }
        });
    }

    async showToast(msg, dur) {
        const toast = await this.toast.create({
            message: msg,
            duration: dur,
        });
        await toast.present();
    }

    /*
    Renvoi l'age
     */
    getAgeInYears(birthday: Date): number {
        const now = new Date();
        const bd = new Date(birthday);
        // @ts-ignore
        return (new Date(now - bd)).getFullYear() - 1970;
    }
}
