import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalendrierPageRoutingModule } from './calendrier-routing.module';
import { NgCalendarModule} from 'ionic2-calendar';
import { CalendarEventPageModule } from '../calendar-event/calendar-event.module';

import { registerLocaleData } from '@angular/common';
import localeFR from '@angular/common/locales/fr';
registerLocaleData(localeFR);


import { CalendrierPage } from './calendrier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalendrierPageRoutingModule,
    NgCalendarModule,
    CalendarEventPageModule
  ],
  declarations: [CalendrierPage],
  providers:[
    { provide: LOCALE_ID, useValue: 'fr' }
  ]
})
export class CalendrierPageModule {}
