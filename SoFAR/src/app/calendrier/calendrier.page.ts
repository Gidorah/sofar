import { Component, OnInit, ViewChild, LOCALE_ID, Inject } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar';
import { AlertController, ModalController } from '@ionic/angular';
import { CalendarEventPage} from '../calendar-event/calendar-event.page';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from '../storage.service';
import {GlobalVariable} from "../global";
import {ProService} from "../pro/pro.service";

@Component({
  selector: 'app-calendrier',
  templateUrl: './calendrier.page.html',
  styleUrls: ['./calendrier.page.scss'],
})

export class CalendrierPage implements OnInit {
  eventSource = [];
  viewTitle: string;
  clicked = false;
  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
  // idPro = this.storageservice.getPro()[0][0];
  idPro = this.proService.getProValue().id_pro;
  status: any;

  @ViewChild(CalendarComponent) myCal: CalendarComponent;


  public items: any = [];
  public a: string;

  constructor(
    public http: HttpClient,
    private alertCtrl: AlertController,
    // pour changer la langue de l'instance du calendrier
    @Inject(LOCALE_ID) private locale: string,
    private modalCtrl: ModalController,
    public storageservice: StorageService,
    private proService: ProService
  ) {
    this.getStatus();
    this.items = [1, 2, 3, 4, 5, 6, 7];
    this.a = " - à ";
  }

  ngOnInit() {
  }
/**
  * bouton pour se déplacer vers le mois suivant
  */
  next() {
    this.myCal.slideNext();
  }
 /**
  * bouton pour se déplacer vers le mois précédent
  */
  back() {
    this.myCal.slidePrev();
  }
  /**
  * Pour changer le titre du mois courant
  */

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
 /**
  * Vide la liste des évènements pour l'utilisateur courant
  */

  removeEvents() {
    this.eventSource = [];
  }

  clickHappy() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.getStatus();
    console.log(this.status);
    if (this.status === '0') {
      const now = new Date().toISOString();
      const day = now.split('T')[0];
      const hour = now.split('T')[1].split('.')[0];
      const tab = ({
        dateHH: day,
        debutHH: hour,
        finHH: '99999999999',
        statusHH: 1,
        idp: this.idPro,
        action: 'update'
      });
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Calendrier/manage_data.php', JSON.stringify(tab), {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).subscribe(data => {
        console.log(data);
      }, (error: any) => {
        console.log(error);
      });
    }
    if (this.status === '1') {
      const tab = ({
        dateHH: null,
        debutHH: null,
        finHH: null,
        statusHH: 0,
        idp: this.idPro,
        action: 'update'
      });
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Calendrier/manage_data.php', JSON.stringify(tab), {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).subscribe(data => {
        console.log(data);
      }, (error: any) => {
        console.log(error);
      });
    }
  }

  getStatus() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Calendrier/retrieve_data.php', JSON.stringify(this.idPro), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).subscribe(data => {
      console.log(data);
      this.status = data[0][3];
    }, error => {
      console.log(error);
    });
  }
  /**
   * reçoit l'élément event(title, description, startTime, endTime)
   */
  async details() {
    const modal = await this.modalCtrl.create({
      component: CalendarEventPage,
      cssClass: 'calendar-event',
      backdropDismiss: false
    });
    await modal.present();
    modal.onDidDismiss().then((result) => {
      if (result.data && result.data.event) {
        const event = result.data.event;
        if (event.allDay) {
          console.log('all day');
          const start = event.startTime;
          event.startTime = new Date(
            Date.UTC(
              start.getUTCFullYear(),
              start.getUTCMonth(),
              start.getUTCDate()
            )
          );
          event.endTime = new Date(
            Date.UTC(
              start.getUTCFullYear(),
              start.getUTCMonth(),
              start.getUTCDate() + 1
            )
          );
        }
        console.log('n est pas entré dans le if');
        console.log(result.data.event);
        console.log(event.startTime);
        console.log(event.endTime);
        console.log(this.eventSource);
        this.eventSource.push(result.data.event);
        console.log(this.eventSource);
        this.myCal.loadEvents();
      }
    });
  }

}
