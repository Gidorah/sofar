import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from '../storage.service';
import {ToastController, Platform, AlertController} from '@ionic/angular';
import {GlobalVariable} from "../global";
import {UserService} from "../user/user.service";

@Component({
    selector: 'app-connexion-client',
    templateUrl: './connexion-client.page.html',
    styleUrls: ['./connexion-client.page.scss'],
})
export class ConnexionClientPage implements OnInit {
    email = '';
    mdp = '';

    constructor(
        private router: Router,
        public toast: ToastController,
        public http: HttpClient,
        public platform: Platform,
        public storageservice: StorageService,
        public alert: AlertController,
        private userService: UserService
    ) {
    }

    /**Fonction qui connecte l'utilisateur à l'application */
    login() {
        // Vérification que tous les champs sont complétés
        if (this.email !== '' && this.mdp !== '') {
            const user = {
                email: this.email,
                password: this.mdp,
                action: 'login'
            };
            this.userService.login(user, true).subscribe(data => {
                    if (data.error === '') {
                        this.router.navigate(['/tabs-client'], {replaceUrl: true});
                    } else if (data.error === 'wrong-pw') {
                        this.showToast(' Désolé ! Le mail ou le mot de passe est invalide.');
                    } else {
                        this.showToast(' Une erreur est survenu');
                    }
                },
                (error: any) => {
                    console.log(error);
                    this.showToast('Requête échouée');
                });
        } else {
            this.showToast('Tous les champs sont requis');
        }
    }

    async showToast(message) {
        const toast = await this.toast.create({
            message,
            duration: 1000
        });
        await toast.present();
    }

    /**Alert mot de passe oublié */
    async forgotPassword() {
        const alert = await this.alert.create({
            header: 'Entre ton mail pour réinitialiser ton mot de passe',
            inputs: [
                {
                    name: 'mail',
                    value: '',
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Réinitialiser',
                    handler: (alertData) => {
                        const email = alertData.mail;
                        this.resetPassword(email);
                    }
                }
            ]
        });
        await alert.present();
    }

    /**Envoi d'un nouveau de mot passe provisoire par mail si le mail existe dans la base */
    resetPassword(body) {
        console.log('reset : ' + body);
        if ((!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(body))) {
            this.showToast('Le mail est invalide');
        } else {
            const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
            this.http.post(GlobalVariable.BASE_API_URL + 'sofar_api/User/reset_password.php', JSON.stringify(body), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe((data: any) => {
                if (data.check === 0) {
                    this.showToast('Il n\'y a pas de compte avec ce mail.');
                } else {
                    this.showToast('Consulte ta boite mail pour te connecter à nouveau');
                }
            }, (error) => {
                console.log(error);
            });
        }
    }

    ngOnInit() {
        this.userService.getAutoConnect().then((res: boolean) => {
            if (res) {
                console.log('User has asked to stay connected: auto-connection', res);
                this.userService.fetchUser().then(obs => {
                    obs.subscribe(() => {
                        this.router.navigate(['/tabs-client'], {replaceUrl: true}).then();
                    });
                });
            }
        });
    }

    onEnter(keyCode: number) {
        // Réagit à la touche 'enter' du clavier
        if (keyCode === 13) {
            this.login();
        }
    }
}
