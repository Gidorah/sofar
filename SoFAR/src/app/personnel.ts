export interface Personnel   {
    id_per: number;
    id_pro: string;
    alias: string;
    date_creation: string;
}
