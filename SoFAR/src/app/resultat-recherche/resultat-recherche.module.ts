import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResultatRecherchePageRoutingModule } from './resultat-recherche-routing.module';

import { ResultatRecherchePage } from './resultat-recherche.page';
import {IonicRatingModule} from "ionic4-rating";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResultatRecherchePageRoutingModule,
    IonicRatingModule
  ],
  exports: [
    ResultatRecherchePage
  ],
  declarations: [ResultatRecherchePage]
})
export class ResultatRecherchePageModule {
}
