import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController, NavParams, ToastController} from '@ionic/angular';
import {InfosResultatPage} from '../infos-resultat/infos-resultat.page';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {StorageService} from "../storage.service";
import {GlobalVariable} from "../global";
import {catchError, timeout} from "rxjs/operators";
import {UserService} from "../user/user.service";


@Component({
    selector: 'app-resultat-recherche',
    templateUrl: './resultat-recherche.page.html',
    styleUrls: ['./resultat-recherche.page.scss'],
})
export class ResultatRecherchePage implements OnInit {

    item: any = [];
    research: any = [];
    localisation = navigator.geolocation;
    afficher: boolean = false;
    choices: any;
    jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
    noresult=false;

    oktext=GlobalVariable.OKTEXT;
    canceltext=GlobalVariable.CANCELTEXT;

    desactive=false;

    // On récupère les infos de la barre de recherche et du "Plus de détails" de la page recherche
    constructor(public modalCtrl: ModalController, public navParams: NavParams, public http: HttpClient, private loadingController: LoadingController,
                private storageService: StorageService, private toast: ToastController,
                private userService: UserService) {
        this.research = {
            search: navParams.get('search'),
            coords_user: navParams.get('coords_user'),
            type: navParams.get('type'),
            rayon: navParams.get('rayon'),
            tri: "Distance : par ordre croissant",
            offset:0,
            action: 'search'
        };
    }

    async ngOnInit() {
        await this.postHtml(this.research);
        if (this.research.type === 'Tous') {
            this.research.type = '';
        }
        this.choices = ["Distance : par ordre croissant", "Distance : par ordre décroissant", "Note : par ordre croissant", "Note : par ordre décroissant"];
    }

    async clickCards(item) {
        const modal = await this.modalCtrl.create({
            component: InfosResultatPage,
            componentProps: {item: item},
            cssClass: "modal_recherche"
        });
        return await modal.present();
    }

    async postHtml(item) {
        const loading = await this.loadingController.create({
            cssClass: 'loading-class',
            message: 'Recherche...',
        });
        if(this.item==[]){
            await loading.present();
        }
        const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
        // récupération des données des établissements
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_data.php', JSON.stringify(item), {
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).pipe(
            timeout(1000),
            catchError(e => {
                this.noresult=true;
                loading.dismiss();
                return null;
            })
        ).subscribe(async data => {
            let nb_reponses = Array.prototype.slice.call(data).length;
            if (nb_reponses==0 && this.item==[]){
                this.noresult=true;
                loading.dismiss();
            }
            else if(nb_reponses<3){
                this.desactive=true;
            }
            for (let i = 0; data[i]; i++) {
                let name = data[i][0];
                let adresse = data[i][1];
                let description = data[i][2];
                let categorie = data[i][5];
                let happy = data[i][6];
                let terrasse = data[i][7];
                let fav = 0;
                ////////////////////
                let service = data[i][14];
                    /////////////////
                let id = data[i][4];
                let distance = data[i][8];
                let note = data[i][9];
                let horaires = data[i][10];
                let ouvert = data[i][21] == 1;
                let distance_string;
                let needResaDetails = data[i][22] === '1';
                const horaireSemaine = await this.userService.getHoraires(id);
                if (distance < 1) {
                    distance_string = distance.toString().split('.')[1].substring(0, 3) + " m";
                } else {
                    distance_string = distance.toString().substring(0, 3) + " km";
                }
                this.item.push({
                    name: name,
                    adresse: adresse,
                    description: description,
                    id: id,
                    categorie: categorie,
                    happy: happy,
                    terrasse: terrasse,
                    fav: fav,
                    service: service,
                    distance: distance_string,
                    distance_nbr: distance,
                    ouvert: ouvert,
                    horaires: horaires,
                    horaires_semaine: horaireSemaine,
                    img: "../assets/images/logo_dark.svg",
                    note:note,
                    need_resa_details: needResaDetails
                });
                if (i == nb_reponses - 1) {
                    this.afficher = true;
                    if(this.item.length==0){
                        this.noresult=true;
                    }
                    await this.fetchPhotos();
                    await loading.dismiss();
                }
            }
        }, (error: any) => {
            console.log(error);
        });
    }

    /*
    Fetch les photos des établissements
     */
    async fetchPhotos() {
        [this.item].forEach(it => {
            it.forEach(etabl => {
                this.storageService.fetchProPhoto(etabl.id).subscribe((res: any) => {
                    console.log(res.pro.photo);
                    if (res.pro.photo !== "") {
                        etabl.img = 'data:image/jpeg;base64,' + res.pro.photo;
                    }
                });
            });
        });
    }

    async pop() {
        await this.modalCtrl.dismiss();
    }

    refresh_criteres(e) {
        this.item=[];
        this.research.offset=0;
        this.research.tri=e;
        this.postHtml(this.research);
    }

    async showToast(msg, temps) {
        const toast = await this.toast.create({
            message: msg,
            duration: temps
        });
        await toast.present();
    }

    loadData(e){
        setTimeout(() => {
            this.research.offset+=4;
            this.postHtml(this.research);
            e.target.complete();
        }, 500);
    }
}
