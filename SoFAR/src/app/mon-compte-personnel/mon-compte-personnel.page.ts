import {Component, OnInit} from '@angular/core';
import {Personnel} from "../personnel";
import {PersonnelService} from "../personnel.service";
import {AlertController, ModalController} from "@ionic/angular";
import {PasswordChangerComponent} from "./password-changer/password-changer.component";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
    selector: 'app-mon-compte-personnel',
    templateUrl: './mon-compte-personnel.page.html',
    styleUrls: ['./mon-compte-personnel.page.scss'],
})
export class MonComptePersonnelPage implements OnInit {

    pers: Personnel = null;
    etab: any = null;

    constructor(
        private personnelService: PersonnelService,
        private modalController: ModalController,
        private router: Router,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
        this.personnelService.getPersonnel().subscribe((pers: Personnel) => {
            this.pers = pers;
        });
        this.personnelService.getEtab().subscribe(etab => {
            this.etab = etab;
        });
    }

    ionViewWillEnter() {
        this.personnelService.fetchPers().then(obs => obs.subscribe());
    }

    async deconnexion() {
        const alert = await this.alert.create({
            header: 'Es-tu sûr(e) de vouloir te déconnecter ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: () => {
                        this.personnelService.logout().then(() => {
                            this.router.navigate(['/connexion-client'], { replaceUrl: true });
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async changePassword() {
        const modal = await this.modalController.create({
            component: PasswordChangerComponent
        });
        await modal.present();
    }
}
