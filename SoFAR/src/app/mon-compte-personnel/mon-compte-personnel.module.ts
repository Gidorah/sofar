import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonComptePersonnelPageRoutingModule } from './mon-compte-personnel-routing.module';

import { MonComptePersonnelPage } from './mon-compte-personnel.page';
import {PasswordChangerComponent} from "./password-changer/password-changer.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MonComptePersonnelPageRoutingModule
  ],
  declarations: [MonComptePersonnelPage, PasswordChangerComponent]
})
export class MonComptePersonnelPageModule {}
