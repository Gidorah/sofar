import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonComptePersonnelPage } from './mon-compte-personnel.page';

const routes: Routes = [
  {
    path: '',
    component: MonComptePersonnelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonComptePersonnelPageRoutingModule {}
