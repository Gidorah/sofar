import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MonComptePersonnelPage } from './mon-compte-personnel.page';

describe('MonComptePersonnelPage', () => {
  let component: MonComptePersonnelPage;
  let fixture: ComponentFixture<MonComptePersonnelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonComptePersonnelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MonComptePersonnelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
