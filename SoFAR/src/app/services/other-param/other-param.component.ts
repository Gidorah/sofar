import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams, ToastController} from "@ionic/angular";
import {ProService} from "../../pro/pro.service";
import {HttpClient} from "@angular/common/http";
import {CarteProService} from "../../carte-pro/carte-pro.service";
import {Pro} from "../../pro/pro";
import {GlobalVariable} from "../../global";

@Component({
    selector: 'app-other-param',
    templateUrl: './other-param.component.html',
    styleUrls: ['./other-param.component.scss'],
})
export class OtherParamComponent implements OnInit {

    needResaDetails: boolean;

    pro: Pro;

    constructor(
        private modalController: ModalController,
        private proService: ProService,
        private toast: ToastController,
        private httpClient: HttpClient
    ) {
    }

    ngOnInit() {
        this.proService.getPro().subscribe((pro: Pro) => {
            this.pro = pro;
            this.needResaDetails = pro.need_resa_details;
        });
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    updateNeedResaDetails($event: MouseEvent) {
        this.needResaDetails = !this.needResaDetails;
    }

    async onSubmit() {
        this.proService.getTokenProObs().then((tok: string) => {
            const body = {
                token: tok,
                action: 'update_need_resa_details',
                need_resa_details: this.needResaDetails
            };
            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_param.php', body,
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe(async (res: any) => {
                if (res.error !== '') {
                    console.error('Error updating other param:', res.error);
                    await this.proService.fetchPro();
                } else {
                    await this.showToast('Paramètre(s) enregistré(s).', 1000);
                }
            });
        });
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

}
