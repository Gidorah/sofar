import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtherParamComponent } from './other-param.component';

describe('OtherParamComponent', () => {
  let component: OtherParamComponent;
  let fixture: ComponentFixture<OtherParamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherParamComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtherParamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
