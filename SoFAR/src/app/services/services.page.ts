import {Component, OnInit} from '@angular/core';
import {Status} from "../Status";
import {ProService} from "../pro/pro.service";
import {PersonnelService} from "../personnel.service";
import {Personnel} from "../personnel";
import {Pro} from "../pro/pro";
import {ActionSheetController, AlertController, ModalController} from "@ionic/angular";
import {CreneauxComponent} from "./creneaux/creneaux.component";
import {DatesComponent} from "./dates/dates.component";
import {DelaisComponent} from "./delais/delais.component";
import {OtherParamComponent} from "./other-param/other-param.component";
import {ProStatusService} from "../pro-status.service";

@Component({
    selector: 'app-services',
    templateUrl: './services.page.html',
    styleUrls: ['./services.page.scss'],
})
export class ServicesPage implements OnInit {

    seg: any = 'reservations';

    /*
    Le status définit qui est présent sur la page: le patron ou un serveur
     */
    status: Status = Status.null;

    constructor(
        private modalCtrl: ModalController,
        private proService: ProService,
        private persService: PersonnelService,
        private actionSheetCtrl: ActionSheetController,
        private proStatusService: ProStatusService
    ) {
    }

    ngOnInit() {
        this.proStatusService.getStatus().subscribe((status: Status) => this.status = status);
    }

    segmentChanged(event: any) {
        this.seg = event.detail.value;
    }

    async openMenu() {
        const actionSheet = await this.actionSheetCtrl.create({
            header: 'Paramètres de réservation',
            buttons: [
                {
                    text: 'Retirer des créneaux de réservation',
                    role: 'block',
                    handler: () => {
                        this.creneaux();
                    }
                }, {
                    text: 'Ajouter des dates de réservation',
                    role: 'block',
                    handler: () => {
                        this.date_res();
                    }
                }, {
                    text: 'Délais de réservation',
                    handler: () => {
                        this.delais();
                    }
                }, {
                    text: 'Autres paramètres',
                    handler: () => {
                        this.otherParam();
                    }
                }, {
                    text: 'Retour',
                    handler: () => {
                        console.log('retour');
                    }
                }
            ]
        });
        await actionSheet.present();
    }

    async creneaux() {
        const modal = await this.modalCtrl.create({component: CreneauxComponent});
        return await modal.present();
    }

    async date_res() {
        const modal = await this.modalCtrl.create({component: DatesComponent});
        return await modal.present();
    }

    async delais() {
        const modal = await this.modalCtrl.create({component: DelaisComponent});
        return await modal.present();
    }

    async otherParam() {
        const modal = await this.modalCtrl.create({component: OtherParamComponent});
        return await modal.present();
    }
}
