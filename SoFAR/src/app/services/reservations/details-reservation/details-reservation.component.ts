import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from "@ionic/angular";
import {Reservation} from "../Reservation";
import {InfosUserComponent} from "../../../infos-user/infos-user.component";

@Component({
  selector: 'app-details-reservation',
  templateUrl: './details-reservation.component.html',
  styleUrls: ['./details-reservation.component.scss'],
})
export class DetailsReservationComponent implements OnInit {

  reservation: Reservation;

  constructor(
      private navParams: NavParams,
      private modalController: ModalController
  ) { }

  ngOnInit() {
    this.reservation = this.navParams.get('reservation');
  }

  async onCancel() {
    await this.modalController.dismiss();
  }

  async showUserInfos(id: number) {
    const modal = await this.modalController.create({
      component: InfosUserComponent,
      componentProps: {user_id: id}
    });
    await modal.present();
  }

}
