import {Participant} from "./Participant";

export interface Reservation {
    id: string;
    article: string;
    date: string;
    heure: string;
    nbPerso: number;
    validation: number;
    etat: string;
    id_user: number;
    id_pro: number;
    nom_user: string;
    prenom_user: string;
    // photo_user: string;
    emplacement: string;
    consigne: string;
    participants: Participant[];
    createdAt: string;
}
