import {Component, OnInit} from '@angular/core';
import {Reservation} from "./Reservation";
import {ProService} from "../../pro/pro.service";
import {HttpClient} from "@angular/common/http";
import {ActionSheetController, AlertController, ModalController} from "@ionic/angular";
import {InfosUserComponent} from "../../infos-user/infos-user.component";
import {GlobalVariable} from "../../global";
import {NotificationService} from "../../notification.service";
import {Pro} from "../../pro/pro";
import {DetailsReservationComponent} from "./details-reservation/details-reservation.component";
import {ProStatusService} from "../../pro-status.service";
import {Status} from "../../Status";

@Component({
    selector: 'app-reservations',
    templateUrl: './reservations.component.html',
    styleUrls: ['./reservations.component.scss'],
})
export class ReservationsComponent implements OnInit {

    seg: any = 'to-come';

    reservations: Array<Reservation> = [];
    reservationsAVenir: Array<Reservation> = [];
    reservationsPasses: Array<Reservation> = [];

    reservationsDatesAVenir: string[] = [];
    reservationsDatesPasses: string[] = [];

    hasTerrasse: boolean;

    status: Status = Status.null;

    constructor(
        private proService: ProService,
        private http: HttpClient,
        private modalCtrl: ModalController,
        public alert: AlertController,
        private notificationService: NotificationService,
        private proStatusService: ProStatusService
    ) {
    }

    ngOnInit() {
        this.proStatusService.getStatus().subscribe((status: Status) => {
            this.status = status;
            this.getReservations();
        });
    }

    segmentChanged(event: any) {
        this.seg = event.detail.value;
    }

    refresh(event: any) {
        this.getReservations();
        event.target.complete();
    }

    async showUserInfos(id: number) {
        const modal = await this.modalCtrl.create({
            component: InfosUserComponent,
            componentProps: {user_id: id}
        });
        await modal.present();
    }

    /**
     * Récupérations des réservations depuis la base de données
     */
    async getReservations() {
        const tok = await this.proStatusService.getToken();
        if (!tok) {
            return;
        }
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/retrieve_data_pro.php', {token: tok}, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe(
            (data: any) => {
                this.reservations = [];
                if (data.error !== '') {
                    console.log('error in getReservations');
                } else {
                    for (let i = 0; data.result[i]; i++) {
                        this.reservations.push({
                            id: data.result[i].reservation_id,
                            article: data.result[i].nom_article_res,
                            date: data.result[i].date_res,
                            heure: data.result[i].heure_res,
                            nbPerso: data.result[i].nb_perso_res,
                            validation: data.result[i].validation_res,
                            etat: data.result[i].etat_res,
                            id_user: data.result[i].id_user,
                            id_pro: data.result[i].id_pro,
                            nom_user: data.result[i].user_nom,
                            prenom_user: data.result[i].user_prenom,
                            // photo_user: (!data.result[i].photo || data.result[i].photo === '') ? '' : 'data:image/jpeg;base64,' + data.result[i].photo,
                            emplacement: data.result[i].emplacement,
                            consigne: data.result[i].consigne,
                            participants: data.result[i].participants,
                            createdAt: data.result[i].createdAt
                        });
                        if (!this.reservationsDatesAVenir.includes(data.result[i].date_res) && data.result[i].etat_res === 'A venir') {
                            this.reservationsDatesAVenir.push(data.result[i].date_res);
                        }
                        if (!this.reservationsDatesPasses.includes(data.result[i].date_res) && data.result[i].etat_res !== 'A venir') {
                            this.reservationsDatesPasses.push(data.result[i].date_res);
                        }
                    }
                }
                this.reservationsDatesAVenir.sort((r1, r2) => {
                    // @ts-ignore
                    return new Date(r1) - new Date(r2);
                });
                this.reservationsDatesPasses.sort((r1, r2) => {
                    // @ts-ignore
                    return new Date(r1) - new Date(r2);
                }).reverse();
                this.reservationsAVenir = this.reservations.filter(reservation => reservation.etat === 'A venir');
                this.reservationsPasses = this.reservations.filter(reservation => reservation.etat !== 'A venir');

            }, (error: any) => {
                console.log(error);
            });
    }

    /**Validation ou annulation d'une réservation */
    async changeStatus(event, res, etat) {
        event.stopPropagation();
        let id = res['id'];
        let alert = await this.alert.create({
            header: 'Valider la réservation ?',
            buttons: [
                {
                    text: 'Retour',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: async () => {
                        await this.confirmer(id, etat);
                        const nomPro = this.proStatusService.getNomPro();
                        const token = await this.proStatusService.getToken();
                        if (token) {
                            this.notificationService.SendMessage(
                                "Réservation confirmée !",
                                nomPro + " a accepté ta réservation le " + res.date + " à " + res.heure + " !",
                                res['id_user'],
                                "utilisateur",
                                token);
                        }
                    }
                }
            ]
        });
        await alert.present();

    }

    async refuseRes(event, res) {
        event.stopPropagation();
        let id = res['id'];
        let alert = await this.alert.create({
            header: 'Refuser la réservation ?',
            buttons: [
                {
                    text: 'Retour',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: async () => {
                        await this.refuser(id);
                        const nomPro = this.proStatusService.getNomPro();
                        const token = await this.proStatusService.getToken();
                        if (token) {
                            this.notificationService.SendMessage(
                                "Réservation refusée !",
                                nomPro + " a refusé ta réservation le " + res.date + " à " + res.heure + " !",
                                res['id_user'],
                                "utilisateur",
                                token);
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

    /**Validation d'une réservation ou service d'une commande */
    async confirmer(id, etat) {
        const tok = await this.proStatusService.getToken();
        if (!tok) {
            return;
        }
        let body = {
            id: id,
            etat: etat,
            valide: 1,
            token: tok
        };
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/confirmer.php', JSON.stringify(body), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe(
            (data) => {
                this.getReservations();
            }, (error) => {
                console.log(error);
            });
    }

    async refuser(id) {
        const tok = await this.proStatusService.getToken();
        if (!tok) {
            return;
        }
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/refuser.php', {token: tok, id: id}, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe(
            (data) => {
                this.getReservations();
            }, (error) => {
                console.log(error);
            });
    }

    /**Set validation_res à 0 et etat_res à passée pour une réservation annulée par le pro */
    annuler(id, cat) {
        if (cat === 1) {
            let body = {
                id: id,
                etat: 'Passée',
                valide: 0,
            };
            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Reservation/annuler.php', JSON.stringify(body), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe(
                (data) => {
                    console.log(data);
                    this.getReservations();
                }, (error) => {
                    console.log(error);
                });
        }
    }

    async showResaDetails(res: Reservation) {
        const modal = await this.modalCtrl.create({
            component: DetailsReservationComponent,
            componentProps: {reservation: res}
        });
        await modal.present();
    }
}

