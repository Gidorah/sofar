export interface Participant {
    reservation_id: number;
    id_u: number;
    nom_u: string;
    prenom_u: string;
}
