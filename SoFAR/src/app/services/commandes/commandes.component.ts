import {Component, OnInit} from '@angular/core';
import {GlobalVariable} from "../../global";
import {HttpClient} from "@angular/common/http";
import {ProService} from "../../pro/pro.service";
import {Commande} from "./Commande";
import {ModalController} from "@ionic/angular";
import {DetailsCommandeComponent} from "./details-commande/details-commande.component";
import {OrderStatusPrintPipe} from "./order-status-print.pipe";
import {Status} from "../../Status";
import {NotificationService} from "../../notification.service";
import {ProStatusService} from "../../pro-status.service";

@Component({
    selector: 'app-commandes',
    templateUrl: './commandes.component.html',
    styleUrls: ['./commandes.component.scss'],
})
export class CommandesComponent implements OnInit {

    seg: any = 'to-come';

    orders: Commande[];

    datePassedOrders: string[] = [];

    status: Status = Status.null;

    commanderDispo: boolean;

    constructor(
        private http: HttpClient,
        private proService: ProService,
        private modalController: ModalController,
        private proStatusService: ProStatusService
    ) {
    }

    ngOnInit() {
        this.orders = [];
        this.proStatusService.getStatus().subscribe(async (status: Status) => {
            this.status = status;
            this.commanderDispo = await this.proStatusService.getCommanderDispo();
            if (this.commanderDispo) {
                await this.getCommandes();
            }
        });
    }

    ionViewWillEnter() {
        this.getCommandes().then();
    }

    segmentChanged(event: any) {
        this.seg = event.detail.value;
    }

    async getCommandes() {
        const tok = await this.proStatusService.getToken();
        if (!tok) {
            return;
        }
        const body = {
            token: tok,
            action: 'fetch-all'
        };
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/retrieve_data_pro.php', body, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe((res: any) => {
            if (res.error) {
                console.error('fetching commandes error: ' + res.error);
            } else {
                this.orders = res.orders.sort((c1, c2) => {
                    // @ts-ignore
                    return new Date(c1.updatedAt) - new Date(c2.updatedAt);
                });
                for (const order of this.orders) {
                    const date = order.createdAt.substring(0, 10);
                    if (!this.datePassedOrders.includes(date) && order.order_status === 'served') {
                        this.datePassedOrders.push(date);
                    }
                }
                this.datePassedOrders = this.datePassedOrders.reverse();
            }
        });
    }

    refresh(event: any) {
        this.getCommandes();
        event.target.complete();
    }

    prettyPrintSinceDate(order: Commande) {
        // @ts-ignore
        const timeDiffInSeconds = Math.round((new Date() - new Date(order.createdAt)) / 1000);
        if (timeDiffInSeconds >= 60 * 60 * 24) {
            return Math.round(timeDiffInSeconds / (60 * 60 * 24)) + ' jour(s)';
        } else if (timeDiffInSeconds >= 60 * 60) {
            return Math.round(timeDiffInSeconds / (60 * 60)) + ' heure(s)';
        } else if (timeDiffInSeconds >= 60) {
            return Math.round(timeDiffInSeconds / 60) + ' minute(s)';
        } else if (timeDiffInSeconds <= 0) {
            return 'quelques secondes';
        } else {
            return timeDiffInSeconds + ' secondes(s)';
        }
    }

    async showOrderDetails(order: Commande) {
        const modal = await this.modalController.create({
            component: DetailsCommandeComponent,
            componentProps: {order: order}
        });

        modal.onDidDismiss().then(() => this.getCommandes());

        await modal.present();
    }

    async
}
