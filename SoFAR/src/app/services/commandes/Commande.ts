import {Item} from "./Item";

export interface Commande {
    id: number;
    user_id: number;
    user_prenom: string;
    user_nom: string;
    pro_id: number;
    pro_nom: string;
    order_status: string;
    // taxe: null;
    total: number;
    createdAt: string;
    updatedAt: string;
    pourboire: number;
    service: string;
    items: Item[];
}
