import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {Commande} from "../Commande";
import {InfosUserComponent} from "../../../infos-user/infos-user.component";
import {GlobalVariable} from "../../../global";
import {HttpClient} from "@angular/common/http";
import {ProService} from "../../../pro/pro.service";
import {Status} from "../../../Status";
import {ProStatusService} from "../../../pro-status.service";
import {NotificationService} from "../../../notification.service";

@Component({
    selector: 'app-details-commande',
    templateUrl: './details-commande.component.html',
    styleUrls: ['./details-commande.component.scss'],
})
export class DetailsCommandeComponent implements OnInit {

    order: Commande;

    status: Status = Status.null;

    constructor(
        private navParams: NavParams,
        private alert: AlertController,
        private modalController: ModalController,
        private http: HttpClient,
        private proService: ProService,
        private toast: ToastController,
        private notificationService: NotificationService,
        private proStatusService: ProStatusService
    ) {
    }

    ngOnInit() {
        this.order = this.navParams.get('order');
        this.proStatusService.getStatus().subscribe((status: Status) => {
            this.status = status;
        });
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    async showUserInfos(id: number) {
        const modal = await this.modalController.create({
            component: InfosUserComponent,
            componentProps: {user_id: id}
        });
        await modal.present();
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    async refuseOrder() {
        const alert = await this.alert.create({
            header: 'Refuser la commande ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: async () => {
                        const token = await this.proStatusService.getToken();
                        if (!token) {
                            return;
                        }
                        const body = {
                            token: token,
                            action: 'refuse',
                            order_id: this.order.id
                        };
                        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/manage_pro.php', body, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).subscribe((res: any) => {
                            if (res.error) {
                                console.error(res.error);
                            } else {
                                this.order.order_status = 'refused';
                                this.showToast('Commande refusé', 1500);
                                const nomPro = this.proStatusService.getNomPro();
                                if (token) {
                                    this.notificationService.SendMessage(
                                        "Commande refusé !",
                                        nomPro + " a refusé la préparation de ta commande.",
                                        this.order.user_id,
                                        "utilisateur",
                                        token);
                                }
                            }
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async acceptOrder() {
        const alert = await this.alert.create({
            header: 'Accepter la commande ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: async () => {
                        const token = await this.proStatusService.getToken();
                        if (!token) {
                            return;
                        }
                        const body = {
                            token: token,
                            action: 'confirm',
                            order_id: this.order.id
                        };
                        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/manage_pro.php', body, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).subscribe((res: any) => {
                            if (res.error) {
                                console.error(res.error);
                            } else {
                                this.order.order_status = 'payed';
                                this.showToast('Commande confirmé', 1500);
                                const nomPro = this.proStatusService.getNomPro();
                                if (token) {
                                    this.notificationService.SendMessage(
                                        "Commande accepté !",
                                        nomPro + " s'occupe de ta commande !",
                                        this.order.user_id,
                                        "utilisateur",
                                        token);
                                }
                            }
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async orderReady() {
        const alert = await this.alert.create({
            header: 'La commande est prête ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: async () => {
                        const token = await this.proStatusService.getToken();
                        if (!token) {
                            return;
                        }
                        const body = {
                            token: token,
                            action: 'ready',
                            order_id: this.order.id
                        };
                        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/manage_pro.php', body, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).subscribe((res: any) => {
                            if (res.error) {
                                console.error(res.error);
                            } else {
                                this.order.order_status = 'ready';
                                this.showToast('Commande prête', 1500);
                                const nomPro = this.proStatusService.getNomPro();
                                if (token) {
                                    this.notificationService.SendMessage(
                                        "Commande prête !",
                                        "Ta commande chez " + nomPro + " est prête ! Tu peux aller la récupérer !",
                                        this.order.user_id,
                                        "utilisateur",
                                        token);
                                }
                            }
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async orderServed() {
        const alert = await this.alert.create({
            header: 'La commande à été servie ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: async () => {
                        const token = await this.proStatusService.getToken();
                        if (!token) {
                            return;
                        }
                        const body = {
                            token: token,
                            action: 'served',
                            order_id: this.order.id
                        };
                        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/manage_pro.php', body, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).subscribe((res: any) => {
                            if (res.error) {
                                console.error(res.error);
                            } else {
                                this.order.order_status = 'served';
                                this.showToast('Commande servie', 1500);
                                const nomPro = this.proStatusService.getNomPro();
                                if (token) {
                                    this.notificationService.SendMessage(
                                        "Commande Servie !",
                                        nomPro + " a servie ta commande: bonne dégustation !",
                                        this.order.user_id,
                                        "utilisateur",
                                        token);
                                }
                            }
                        });
                    }
                }
            ]
        });
        await alert.present();
    }
}
