export interface Item {
    id: number;
    article_id: number;
    quantity: number;
    article_nom: string;
    article_prix: number;
    article_taxe: number;
    order_id: number;
    total: number;
}
