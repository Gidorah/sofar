import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'orderStatusPrint'
})
export class OrderStatusPrintPipe implements PipeTransform {

    transform(orderStatus: string): string {
        switch (orderStatus) {
            case 'creation':
                return 'Nouvelle';
            case 'refused':
                return 'Refusée';
            case 'payed':
                return 'Payée';
            case 'ready':
                return 'Prête';
            case 'served':
                return 'Servie';
        }
    }

}
