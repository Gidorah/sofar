import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, ToastController} from "@ionic/angular";
import {Ionic4DatepickerModalComponent} from "@logisticinfotech/ionic4-datepicker";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../../storage.service";
import {GlobalVariable} from "../../global";
import {ProService} from "../../pro/pro.service";

@Component({
  selector: 'app-delais',
  templateUrl: './delais.component.html',
  styleUrls: ['./delais.component.scss'],
})
export class DelaisComponent implements OnInit {

  id;
  nb_jours;
  heure_limite;
  aucune;
  delais;

  oktext = GlobalVariable.OKTEXT;
  canceltext = GlobalVariable.CANCELTEXT;

  constructor(private modalCtrl: ModalController,
              public storageservice: StorageService,
              public proService: ProService,
              public http: HttpClient,
              public toast: ToastController,
              public alertController: AlertController) { }

  onCancel() {
    this.modalCtrl.dismiss();
  }

  async showToast(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    await toast.present();
  }

  async ngOnInit() {
    this.id = this.proService.getProValue().id_pro;
    let infos = await this.getHtml(this.id);
    this.nb_jours=infos[0][1];
    if(infos[0][2]=="aucune"){
      this.aucune=true;
      this.heure_limite="00:00";
    }
    else{
      this.aucune=false;
      this.heure_limite=infos[0][2];
    }

  }



  async onSubmit_jours() {
    this.postHtml_jours(this.nb_jours);
    console.warn("nb",this.nb_jours);
  }

  async onSubmit_heures() {
    if(this.aucune==true){
      this.postHtml_heures("aucune");
    }
    else{
      this.postHtml_heures(this.heure_limite);
    }
  }

  async postHtml_jours(delais_jours) {
    await this.proService.getTokenProObs().then(tok => {
      const body = {
        token: tok,
        delais_jours:delais_jours,
        action: 'changement_jours',
      }
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Delais/manage_data.php', body, {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).subscribe(async (data: any) => {
        console.log(data);
        if (data.status === 'delais_modifies') {
          await this.showToast('Délai modifié!');
        } else {
          await this.showToast('Une erreur est survenue');
        }
        console.log(data);
      }, (error: any) => {
        console.log(error);
      });
    });


  }

  async postHtml_heures(delais_heures) {
    await this.proService.getTokenProObs().then(tok => {
      const body = {
        token: tok,
        delais_heures:delais_heures,
        action: 'changement_heures',
      }
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Delais/manage_data.php', body, {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      } ).subscribe(async (data: any) => {
        if (data.status === 'delais_modifies') {
          await this.showToast('Heure limite modifiée !');
        } else {
          await this.showToast('Une erreur est survenue');
        }
        console.log(data);
      }, (error: any) => {
        console.log(error);
      });
    });


  }

  async getHtml(item) {
    return await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Delais/retrieve_data.php', JSON.stringify(item), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).toPromise();
  }


}
