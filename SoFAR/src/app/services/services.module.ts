import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ServicesPageRoutingModule} from './services-routing.module';

import {ServicesPage} from './services.page';

import {DelaisComponent} from "./delais/delais.component";
import {DatesComponent} from "./dates/dates.component";
import {CreneauxComponent} from "./creneaux/creneaux.component";
import {ReservationsComponent} from "./reservations/reservations.component";
import {CommandesComponent} from "./commandes/commandes.component";
import {DetailsReservationComponent} from "./reservations/details-reservation/details-reservation.component";
import {DetailsCommandeComponent} from "./commandes/details-commande/details-commande.component";
import { OrderStatusPrintPipe } from './commandes/order-status-print.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ServicesPageRoutingModule
    ],
    declarations: [
        ServicesPage,
        DelaisComponent,
        DatesComponent,
        CreneauxComponent,
        ReservationsComponent,
        CommandesComponent,
        DetailsCommandeComponent,
        DetailsReservationComponent,
        OrderStatusPrintPipe],
    providers: [
        OrderStatusPrintPipe
    ]
})
export class ServicesPageModule {
}
