import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, ToastController} from "@ionic/angular";
import {Ionic4DatepickerModalComponent} from "@logisticinfotech/ionic4-datepicker";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../../storage.service";
import {GlobalVariable} from "../../global";
import {ProService} from "../../pro/pro.service";

@Component({
  selector: 'app-creneaux',
  templateUrl: './creneaux.component.html',
  styleUrls: ['./creneaux.component.scss'],
})
export class CreneauxComponent implements OnInit {

  date;
  datePickerObj;
  delais = 30;
  minTime_date: any;
  maxTime_date: any;
  disabledDates: Array<String> = new Array<String>();
  journee_entiere=false;
  min_heure;
  max_heure;
  id;
  blocked;

  oktext = GlobalVariable.OKTEXT;
  canceltext = GlobalVariable.CANCELTEXT;


  constructor(private modalCtrl: ModalController,
              public storageservice: StorageService,
              public proService: ProService,
              public http: HttpClient,
              public toast: ToastController,
              public alertController: AlertController) { }

  onCancel() {
    this.modalCtrl.dismiss();
  }

  async showToast(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    await toast.present();
  }

  async ngOnInit() {
    this.id = this.proService.getProValue().id_pro;
    let date_actuelle = new Date();
    let annee_actuelle = date_actuelle.getFullYear();
    let mois_actuel = (date_actuelle.getMonth() + 1).toString();
    if (mois_actuel.length < 2) {
      mois_actuel = "0" + mois_actuel.toString();
    }
    let jour_actuel = (date_actuelle.getDate()).toString();
    if (jour_actuel.length < 2) {
      jour_actuel = "0" + jour_actuel.toString();
    }
    date_actuelle.setDate(date_actuelle.getDate() + 365); //On affiche le calendrier jusqu'à un an après
    let date_max = date_actuelle;
    let annee_max = date_max.getFullYear();
    let mois_max = (date_max.getMonth() + 1).toString();
    if (mois_max.length < 2) {
      mois_max = "0" + mois_max.toString();
    }
    let jour_max = date_max.getDate().toString();
    if (jour_max.length < 2) {
      jour_max = "0" + jour_max.toString();
    }
    this.minTime_date = annee_actuelle + "-" + mois_actuel + "-" + jour_actuel;
    this.date = this.minTime_date;
    this.maxTime_date = annee_max + "-" + mois_max + "-" + jour_max;

    //Desactiver toutes les dates au dela du délais (on le fait manuellement pour pouvoir réactiver certaines d'entre elles)

    let jour_a_desactiver = 365 - this.delais;

    let date = new Date();

    if (jour_a_desactiver > 0) {
      date.setDate(date.getDate() + this.delais); //on désactive les dates après le délais choisi
      for (let i = 0; i < jour_a_desactiver; i++) {
        date.setDate(date.getDate() + 1);
        this.disabledDates.push(date.getFullYear().toString() + "-" + (date.getMonth() + 1).toString() + "-" + date.getDate().toString());
      }
    }
    this.datePickerObj = {
      //inputDate: new Date('2021-06-14'), // default new Date()
      fromDate: new Date(this.minTime_date), // default null
      toDate: new Date(this.maxTime_date), // default null
      showTodayButton: false, // default true
      //closeOnSelect: true, // default false
      disableWeekDays: [], // default []
      mondayFirst: true, // default false
      closeLabel: 'Retour', // default 'Close'
      setLabel: 'Valider',  // default 'Set'
      todayLabel: 'Aujourdhui', // default 'Today
      disabledDates: this.disabledDates, // default []
      titleLabel: 'Selectionner une date', // default null
      monthsList: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
      weeksList: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
      dateFormat: 'YYYY-MM-DD', // default DD MMM YYYY
      clearButton: false, // default true
      //momentLocale: 'fr-FR', // Default 'en-US'
      yearInAscending: true, // Default false
      //btnCloseSetInReverse: true, // Default false
      btnProperties: {
        expand: 'block', // Default 'block'
        fill: '', // Default 'solid'
        size: '', // Default 'default'
        disabled: '', // Default false
        strong: '', // Default false
        color: 'red' // Default ''
      },
      arrowNextPrev: {
        nextArrowSrc: 'assets/images/next.svg',
        prevArrowSrc: 'assets/images/previous.svg'
      }, // This object supports only SVG files.
      /*highlightedDates: [
        { date: new Date('2021-06-16'), color: '#ee88bf', fontColor: '#fff' },
        { date: new Date('2021-06-15'), color: '#50f2b1', fontColor: '#fff' }
      ], // Default [],*/
    };
    const body = {
      id:this.id = this.proService.getProValue().id_pro,
      action: "all",
      type: 'retrait'
    }


    this.blocked = await this.getHtml_except(body);
    console.log(this.blocked);
  }

  async onSubmit() {
    let heure;
    if(this.journee_entiere==true){
      heure = "Journee entiere";
      this.postHtml(heure,this.date);
    }
    else{
      if(this.min_heure==undefined||this.max_heure==undefined){
        await this.showToast('Veuillez saisir l\'heure');
      }
      else{
        let min_heure=this.min_heure.split('T')[1].split('.')[0].split(':')[0] + ":" + "00";
        let max_heure=this.max_heure.split('T')[1].split('.')[0].split(':')[0] + ":" + "00";
        heure = min_heure+"-"+max_heure;
        this.postHtml(heure,this.date);
      }
    }
  }

  async postHtml(heure,jour) {
    // console.log(await this.storageservice.getTokenProObs());
    await this.proService.getTokenProObs().then(tok => {
      const body = {
        token: tok,
        heure: heure,
        jour:jour,
        action: 'insert',
        type: 'retrait'
      }
      const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Exceptions_reservations/manage_data.php', body, {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).subscribe(async (data: any) => {
        console.log(data);
        if (data.status === 'creneau_ajoute') {
          await this.showToast('Créneau ajouté!');
          this.blocked.push([jour,heure])
        } else {
          await this.showToast('Une erreur est survenue');
        }
        console.log(data);
      }, (error: any) => {
        console.log(error);
      });
    });


  }

  async openDatePicker() {
    /*this.heures = [];
    for (let i = parseInt(this.minTime); i < (parseInt(this.maxTime) + 1); i++) {
      this.heures.push(i);
    }*/
    const datePickerModal = await this.modalCtrl.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: {
        'objConfig': this.datePickerObj,
        'selectedDate': this.date,
      }
    });
    await datePickerModal.present();

    datePickerModal.onDidDismiss().then((data) => {
      if (data.data.date != "Invalid date") {
        this.date = data.data.date;
        console.log(this.date);
        /*this.reserve['dateRes'] = data.data.date;
        this.supprimer_heures(this.reserve['dateRes']);*/

      }
    });
  }

  async delete_ex(block) {
    const alert = await this.alertController.create({
      header: 'Voulez vous supprimer ce créneau?',
      message: block[0]+" - "+block[1],
      buttons: [{
        text: 'Retour',
        handler: () => {
          alert.dismiss();
        }
      },
        {
          text: 'Oui',
          handler: () => {
            this.postHtml_delete(block[0],block[1]);
          }
        },]
    });

    await alert.present();
  }

  async getHtml_except(item) {
    return await this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Exceptions_reservations/retrieve_data.php', JSON.stringify(item), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).toPromise();
  }

  async postHtml_delete(jour,heure) {
    // console.log(await this.storageservice.getTokenProObs());
    await this.proService.getTokenProObs().then(tok => {
      console.log(tok);
      const body = {
        token: tok,
        heure: heure,
        jour:jour,
        action: 'delete',
        type: 'retrait'
      }
      const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
      this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Exceptions_reservations/manage_data.php', body, {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      }).subscribe(async (data: any) => {
        if (data.status === 'creneau_retire') {
          this.retirer(jour,heure);
          await this.showToast('Créneau retiré');
        } else {
          await this.showToast('Une erreur est survenue');
        }
      }, (error: any) => {
        console.log(error);
      });
    });


  }

  retirer(jour,heure){
    for( var i = 0; i < this.blocked.length; i++){
      if ( this.blocked[i][0] === jour && this.blocked[i][1] === heure) {
        this.blocked.splice(i, 1);
      }
    }
  }

}
