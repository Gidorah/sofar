import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {BehaviorSubject, Observable} from "rxjs";
import {Pro} from "./pro";
import {GlobalVariable} from "../global";
import {tap} from "rxjs/operators";
import {NotificationService} from "../notification.service";

@Injectable({
    providedIn: 'root'
})
export class ProService {

    private pro: BehaviorSubject<Pro> = new BehaviorSubject<Pro>(null);

    constructor(
        private httpClient: HttpClient,
        private storage: Storage,
        private notificationService: NotificationService
    ) {
        this.storage.create().then(() => {
            this.storage.get('PRO').then((pro: Pro) => {
                this.pro.next(pro);
            });
        });
    }

    loginPro(user: any, stayConnected: boolean): Observable<any> {
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_data.php', user, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    if (res.pro) {
                        await this.storage.set('ACCESS_TOKEN_PRO', res.pro.access_token);
                        await this.storage.set('PRO_STAY_CONNECTED', true);
                        this.pro.next({
                            id_pro: res.pro.id_pro,
                            nom_pro: res.pro.nom_pro,
                            adresse: res.pro.adresse,
                            categorie_pro: res.pro.categorie_pro,
                            description_pro: res.pro.description_pro,
                            email_pro: res.pro.email_pro,
                            terrasse_pro: res.pro.terrasse_pro
                        });
                        this.notificationService.initPushNotification().subscribe((token)=>{
                            this.notificationService.addorremovedevice(res.pro.access_token,token,"professionel","insert");
                        });
                        await this.storage.set('PRO', this.pro.value);
                    }
                } else {
                    console.error('error login pro: ', res.error);
                }
            })
        );
    }

    async logoutPro() {
        let token_pro = await this.storage.get('ACCESS_TOKEN_PRO');
        this.notificationService.initPushNotification().subscribe((token)=>{
            this.notificationService.addorremovedevice(token_pro,token,"professionel","delete");
        });
        await this.storage.remove('ACCESS_TOKEN_PRO');
        await this.storage.remove('PRO_STAY_CONNECTED');
        await this.storage.remove('PRO');
        this.pro.next(null);
    }

    async getAutoConnectPro() {
        return this.storage.get('PRO_STAY_CONNECTED');
    }

    async fetchPro(): Promise<Observable<any>> {
        const tok = await this.getTokenProObs();
        const body = {
            token: tok,
            action: 'fetch'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    if (res.pro) {
                        this.pro.next({
                            id_pro: res.pro.id_pro,
                            nom_pro: res.pro.nom_pro,
                            adresse: res.pro.adresse,
                            categorie_pro: res.pro.categorie_pro,
                            description_pro: res.pro.description_pro,
                            email_pro: res.pro.email_pro,
                            terrasse_pro: res.pro.terrasse_pro,
                            photo: res.pro.photo,
                            //service: res.pro.service,
                            need_resa_details: res.pro.need_resa_details === '1',
                            commander_dispo: res.pro.commander_dispo === '1'
                        });
                        await this.storage.set('PRO', this.pro.value);
                    }
                } else {
                    console.error('error fetching pro: ', res.error);
                }
            })
        );
    }

    async getTokenProObs(): Promise<any> {
        return this.storage.get('ACCESS_TOKEN_PRO');
    }

    getPro(): Observable<Pro> {
        return this.pro;
    }

    getProValue(): Pro {
        return this.pro.value;
    }

    changePasswordPro(form: {
        token: string,
        password: string,
        new_password: string
    }): Observable<any> {
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/update_password.php', form,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
    }

    isProConnected(): boolean {
        return this.pro.value !== null;
    }

    changePhotoPro(image: string) {
        return this.getTokenProObs().then(tok => {
            const pro = {
                token: tok,
                image: image
            };
            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/update_photo.php', pro,
                {
                    headers : {
                        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).pipe(
                tap(async (res: any) => {
                    console.log('changePhoto() pro');
                    const proCopy = this.pro.value;
                    proCopy.photo = image;
                    this.pro.next(proCopy);
                })
            ).subscribe();
        });
    }

}
