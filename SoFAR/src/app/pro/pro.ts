export interface Pro {
    id_pro: string;
    nom_pro: string;
    adresse: string;
    categorie_pro: string;
    description_pro: string;
    email_pro: string;
    terrasse_pro: string;
    photo?: string;
    need_resa_details?: boolean;
    commander_dispo?: boolean;
}
