import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController, ToastController} from '@ionic/angular';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {formatDate} from '@angular/common';
import {StorageService} from '../storage.service';
import {GlobalVariable} from "../global";
import {UserService} from "../user/user.service";

@Component({
    selector: 'app-inscription-client',
    templateUrl: './inscription-client.page.html',
    styleUrls: ['./inscription-client.page.scss'],
})
export class InscriptionClientPage implements OnInit {
    user: any = {};

    adultDate: string;

    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;

    constructor(public router: Router,
                public modalctrl: ModalController,
                public toast: ToastController,
                public http: HttpClient,
                private storageService: StorageService,
                private userService: UserService,
                private loadingController: LoadingController) {
    }

    /**Fonction qui récupère les informations du formulaire et vérifie la validité des champs
     * pour ajouter un nouvel utilisateur dans la table user et dans le storage
     */
    async insert() {
        if (this.user['nom'] === undefined) {
            this.showToast('Un nom est requis');
        } else if (this.user['prenom'] === undefined) {
            this.showToast('Un prénom est requis');
        } else if (this.user['sexe'] === undefined) {
            this.showToast('Le genre est requis');
        } else if (this.user['email'] === undefined) {
            this.showToast("Un email est requis");
        } else if (this.user["mdp"] === undefined) {
            this.showToast("Un mot de passe est requis");
        } else if (this.user["date_de_naissance"] === undefined) {
            this.showToast("Une date de naissance est requise");
        } else if (this.user["mdp"].length < 6) {
            this.showToast("Le mot de passe doit contenir au moins 6 caractères");
        } else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.user["email"])) {
            this.showToast("Le mail est invalide");
        } else {

            const loading = await this.loadingController.create({
                cssClass: 'loading-class',
                message: 'Veuillez patienter...',
            });

            await loading.present();

            this.user.inscription = formatDate(new Date(), 'yyyy/MM/dd', 'fr');

            this.user.date_de_naissance = this.user.date_de_naissance.substring(0, 10); // Reformat de la date, osef du l'heure etc..
            let headers: any = new HttpHeaders({'Content-Type': 'application/json'});
            this.user.action = 'insert';


            this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/User/manage_data.php', JSON.stringify(this.user), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe((data: any) => {
                    console.log(data['status']);
                    if (data['status'] === 'success') {
                        // login after registration
                        loading.dismiss();
                        this.showToast('inscription réussie!');

                        const user = {
                            email: this.user.email,
                            password: this.user.mdp,
                            action: 'login'
                        };

                        this.userService.login(user, true).subscribe(res => {
                                if (res.error === '') {
                                    this.router.navigate(['/tabs-client'], { replaceUrl: true });
                                } else if (res.error === 'wrong-pw') {
                                    this.showToast(' Désolé ! Le mail ou le mot de passe est invalide.');
                                } else {
                                    this.showToast(' Une erreur est survenu: ');
                                    console.error(' Une erreur est survenu: ', res.error);
                                }
                            },
                            (error: any) => {
                                loading.dismiss();
                                console.error(error);
                                this.showToast('Requête échouée');
                            });

                    } else if (data['status'] === 'existing-account') {
                        loading.dismiss();
                        this.showToast('Un compte a déjà été crée avec cette adresse mail.');
                    }
                },
                (error: any) => {
                    loading.dismiss();
                    console.log(error);
                }
            );
        }
    }

    /**Toast avec message personnalisable */
    async showToast(message) {
        let toast = await this.toast.create({
            message: message,
            duration: 2000
        });
        await toast.present();
    }

    ngOnInit() {

        // Retrait de 18 ans a la date d'ajd pour l'inscription
        const date = new Date();
        date.setMonth(date.getMonth() - 12 * 18);
        this.adultDate = formatDate(date, 'yyyy-MM-dd', 'fr');
    }

}
