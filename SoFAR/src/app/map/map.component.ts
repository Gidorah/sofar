import { Component, OnInit } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import {AlertController, ModalController, NavParams} from "@ionic/angular";
import {fromLonLat} from "ol/proj";
import {Feature, Overlay} from "ol";
import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import {Point} from "ol/geom";
import {Icon, Style} from "ol/style";
import {Coordinate} from "ol/coordinate";
import { HttpClient } from '@angular/common/http';
import {boundingExtent} from 'ol/extent';
import {getCenter} from 'ol/extent';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})

export class MapComponent implements OnInit {

  constructor(public alertController: AlertController, public modalCtrl: ModalController, navParams: NavParams, _http:HttpClient, private launchNavigator: LaunchNavigator) {
    this.item = navParams.get('item');
    console.log(navParams);
    this.http=_http;
  }

  item: Array<{name: string, adresse: string, coordinates: string}>;
  http: HttpClient;
  applis: Array<{displayname:string, appname:string}> = [];

  async ouvrir_maps(){

    let inputs=[];
    let choixapp=this.applis[0].appname;
    let choix = {google_maps:this.launchNavigator.APP.GOOGLE_MAPS,waze:this.launchNavigator.APP.WAZE,uber:this.launchNavigator.APP.UBER,citymapper:this.launchNavigator.APP.CITYMAPPER,apple_maps:this.launchNavigator.APP.APPLE_MAPS,navigon:this.launchNavigator.APP.NAVIGON,transit_app:this.launchNavigator.APP.TRANSIT_APP,yandex:this.launchNavigator.APP.YANDEX,tomtom:this.launchNavigator.APP.TOMTOM,sygic:this.launchNavigator.APP.SYGIC,here_maps:this.launchNavigator.APP.HERE_MAPS,moovit:this.launchNavigator.APP.MOOVIT,lyft:this.launchNavigator.APP.LYFT,maps_me:this.launchNavigator.APP.MAPS_ME,cabify:this.launchNavigator.APP.CABIFY,baidu:this.launchNavigator.APP.BAIDU,taxis_99:this.launchNavigator.APP.TAXIS_99,gaode:this.launchNavigator.APP.GAODE}
    let checked = true;
    for(let app of this.applis){
      inputs.push(
          {
            type: 'radio',
            label: app.displayname,
            handler: () => {
              choixapp = app.appname;
            },
            checked: checked,
          }
      );
      checked = false;
    }

    const alert = await this.alertController.create({
      header: 'Ouvrir dans ...',
      inputs: inputs,
      buttons: [
        {
          text: 'Retour',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Confirmer',
          handler: () => {
            console.log(choixapp);
            let options: LaunchNavigatorOptions = {
              app: choix[choixapp],
              start:[this.item['coords_user'][1],this.item['coords_user'][0]]
            };
            this.launchNavigator.navigate([this.item['coordonnees'][1],this.item['coordonnees'][0]],options)
                .then(success =>{
                  console.log(success);
                },error=>{
                  console.log(error);})
          }
        }
      ]
    });

    const alert_no_appli = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'ATTENTION : aucune application de navigation installée',
      buttons: [
        {
          text: 'Retour',
          role: 'cancel',
          cssClass: 'secondary',
        }
      ]
    });

    if (this.applis.length==0){
      await alert_no_appli.present();
    }
    else{
      await alert.present();
    }

  }

  ngOnInit(): void {

    this.launchNavigator.availableApps().then(results => {
      for(var app in results){
        if(results[app]){
          this.applis.push({
            displayname:this.launchNavigator.getAppDisplayName(app),
            appname:app
          })
        }
      }
    });

    let map : Map;

    let tileLayer = new TileLayer({
      source: new OSM({})
    })

    map = new Map({
      target: document.getElementById('ol-map'),
      layers: [tileLayer],
    });

    map.setSize([window.innerWidth,window.innerHeight]);

    let nom = this.item['name'];

    let coords_etab : number[] = this.item['coordonnees'];
    let coords_user : number[] = this.item['coords_user'];

    let iconFeature_etab = new Feature({
      geometry: new Point(fromLonLat(coords_etab)),
      name: nom,
    });

    let iconStyle_etab = new Style({
      image: new Icon({
        anchor: [0.5, 1],
        imgSize : [512,512],
        src: '../../assets/images/icon_rouge.png',
        scale: 0.1,
      }),
    });

    iconFeature_etab.setStyle(iconStyle_etab);

    let vectorSource_etab = new VectorSource({
      features: [iconFeature_etab],
    });

    let vectorLayer_etab = new VectorLayer({
      source: vectorSource_etab
    });
    // View and map
    let view = new View({
      center: fromLonLat(coords_etab),
      zoom: 18
    });
    map.setView(view);
    map.addLayer(vectorLayer_etab);

    let iconFeature_user = new Feature({
      geometry: new Point(fromLonLat(coords_user)),
      name: "Ma position",
    });

    let iconStyle_user = new Style({
      image: new Icon({
        anchor: [0.5, 1],
        imgSize : [512,512],
        src: '../../assets/images/icon.png',
        scale: 0.1,
      }),
    });

    iconFeature_user.setStyle(iconStyle_user);

    let vectorSource_user = new VectorSource({
      features: [iconFeature_user],
    });

    let vectorLayer_user = new VectorLayer({
      source: vectorSource_user
    });

    map.addLayer(vectorLayer_user);
    let extent = boundingExtent([fromLonLat(coords_etab),fromLonLat(coords_user)])
    let center = getCenter(extent);
    let zoom;
    let distance = Math.max(Math.abs(coords_etab[0]-coords_user[0]),Math.abs(coords_etab[1]-coords_user[1]));
    if(distance < 0.01){
      zoom=18 - 365*distance/1.2;
    }
    else{
      zoom=18 - 365*distance/2.2;
    }

    let view1 = new View({
      center: center,
      zoom: zoom,
    });
    map.setView(view1);

    let element = document.getElementById('popup-content');

    let popup = new Overlay({
      element: document.getElementById('popup'),
      autoPan: true,
      autoPanAnimation: {
        duration: 250,
      }
    });
    map.addOverlay(popup);

    map.on('click', (evt) =>{
      let feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
        return feature;
      });
      if (feature) {
        let p: Point = <Point>feature.getGeometry();
        let coordinates: Coordinate = p.getCoordinates();
        popup.setPosition(coordinates);
        element.innerHTML = feature.get('name');
      } else {
        popup.setPosition(undefined);
        element.innerHTML = '';
      }
    });

  }

  pop() {
    this.modalCtrl.dismiss();
  }

}
