import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommanderPageRoutingModule } from './commander-routing.module';

import {ArticleDetailsComponent} from "./article-details/article-details.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommanderPageRoutingModule
  ],
  declarations: [ArticleDetailsComponent]
})
export class CommanderPageModule {}
