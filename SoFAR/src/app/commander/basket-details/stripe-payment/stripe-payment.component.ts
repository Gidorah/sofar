import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams, ToastController} from "@ionic/angular";
import {HttpClient} from "@angular/common/http";
import {CommanderService} from "../../commander.service";
import {GlobalVariable} from "../../../global";
import {UserService} from "../../../user/user.service";
import {Router} from "@angular/router";
import {NotificationService} from "../../../notification.service";

declare var Stripe;

@Component({
    selector: 'app-stripe-payment',
    templateUrl: './stripe-payment.component.html',
    styleUrls: ['./stripe-payment.component.scss'],
})
export class StripePaymentComponent implements OnInit {
    stripe = Stripe(GlobalVariable.STRIPE_KEY);
    card: any;

    totalAmount: number;
    pourboire: number;
    chxService: string;

    constructor(
        private router: Router,
        private modalController: ModalController,
        private toast: ToastController,
        private httpClient: HttpClient,
        private navParams: NavParams,
        private commanderService: CommanderService,
        private userService: UserService,
        private notificationService: NotificationService
    ) {
    }

    ngOnInit() {
        this.totalAmount = Number(this.navParams.get('totalAmount').toFixed(2));
        this.pourboire = Number(this.navParams.get('pourboire').toFixed(2));
        this.chxService = String(this.navParams.get('choixService'));
        //console.log(this.chxService);
        this.setupStripe();
    }

    async onCancel(data: any) {
        if (data.close === 'all') {
            await this.modalController.dismiss(undefined, undefined, 'commander-modal').then(() => {
                this.modalController.dismiss(undefined, undefined, 'basket-modal').then(() => {
                    this.modalController.dismiss(undefined, undefined, 'stripe-modal');
                });
            });
        } else {
            await this.modalController.dismiss();
        }
    }

    setupStripe() {
        const elements = this.stripe.elements();
        const style = {
            base: {
                color: '#fcfcfc',
                lineHeight: '24px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#b0a3a3'
                },
                backgroundColor: '#1f1f1f'
            },
            invalid: {
                color: '#fa5a5a',
                iconColor: '#fa5a5a'
            }
        };

        this.card = elements.create('card', {style: style});

        this.card.mount('#card-element');

        this.card.addEventListener('change', event => {
            const displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        const form = document.getElementById('payment-form');
        form.addEventListener('submit', event => {
            event.preventDefault();

            this.stripe.createToken(this.card).then(async result => {
                if (result.error) {
                    const errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {

                    const userToken = await this.userService.getTokenObs();
                    const panier = await this.commanderService.getPanierValue();

                    const payement = {
                        user_token: userToken,
                        stripe_token: result.token.id,
                        id_pro: this.commanderService.idPro,
                        price: this.totalAmount,
                        action: 'order',
                        service: this.chxService,
                        commande: {
                            panier: panier,
                            pourboire: this.pourboire
                        }
                    };
                    await this.makePayment(payement);
                }
            });
        });
    }

    async makePayment(payement: any) {
        this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Commande/stripePayment.php', payement,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).subscribe((res: any) => {
                if (res.error) {
                    this.showToast(res.error, 3000);
                } else {
                    this.showToast('Commande effectuée !', 1500);
                    this.onCancel({close: 'all'});
                    this.userService.getTokenObs().then(tok => {
                        this.notificationService.SendMessage(
                            "Nouvelle commande !",
                            "Une nouvelle commande vient d'être passée.",
                            payement.id_pro,
                            "professionel",
                            tok);
                    });
                }
            },
            error => console.error('payment error:', error));
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

}
