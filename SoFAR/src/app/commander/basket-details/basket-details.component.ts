import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams, ToastController} from "@ionic/angular";
import {HttpClient} from "@angular/common/http";
import {CommanderService} from "../commander.service";
import {Produit} from "../produit";
import {Article} from "../../carte-pro/Article";
import {ArticleDetailsComponent} from "../article-details/article-details.component";
import {StripePaymentComponent} from "./stripe-payment/stripe-payment.component";

@Component({
    selector: 'app-basket-details',
    templateUrl: './basket-details.component.html',
    styleUrls: ['./basket-details.component.scss'],
})
export class BasketDetailsComponent implements OnInit {

    panier: Produit[];

    pourboire: number;

    subTotal: number;
    tableService=1;
    comptoirService=1;
    chxTable=0;
    chxComptoir=0;
    choixService: string;

    Service;

    constructor(
        private modalController: ModalController,
        private toast: ToastController,
        private httpClient: HttpClient,
        private navParams: NavParams,
        private commanderService: CommanderService
    ) {
    }

    ngOnInit() {
        this.commanderService.getPanier().subscribe((panier: Produit[]) => this.panier = panier);
        console.log(this.panier);
        this.subTotal = this.countSubTotal();
        console.warn(">>>>>>>>>>>  SERVICE >>>>", this.commanderService.proService);
        if(this.commanderService.proService=="table"){
            this.tableService=1;
            this.comptoirService=0;
        } else if(this.commanderService.proService=="comptoir"){
            this.comptoirService=1;
            this.tableService=0;
        }

        console.warn();

       /*     console.warn(this.commanderService.panier.value);
        this.commanderService.getServices().subscribe((result) => {
            console.warn('>test>>>>>>>>>>>>>>>>>>>>>', this.commanderService.getServices().subscribe());
        })

        //this.Service = this.commanderService.getServices();
        this.commanderService.getServices().subscribe((Service) => this.Service= Service);
        console.warn(">>>>>>>>>>>>>",this.Service);*/

    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    countSubTotal(): number {
        let total = 0;
        for (const prod of this.panier) {
            total += prod.quantity * prod.article.article_prix;
        }
        return total;
    }

    countTotal(): number {
        if (!this.pourboire) {
            return this.countSubTotal();
        }
        else {
            return this.pourboire + this.countSubTotal();
        }
    }


    async openArticle(art: Article) {
        const modal = await this.modalController.create({
            component: ArticleDetailsComponent,
            componentProps: {article: art}
        });
        await modal.present();
    }

    async purchase() {
        console.warn("choix de service comptoir:", this.chxComptoir);
        if(this.chxTable==1 && this.chxComptoir==0){
            this.choixService = "table";
        } else if(this.chxComptoir==1 && this.chxTable==0){
            this.choixService = "comptoir";
        } else {
            this.choixService = undefined;
        }
        if(this.choixService==undefined){
            await this.showToast("Il faut choisir un service", 1000);
        }else {
            if (!this.pourboire) {
                this.pourboire = 0;
            }

            console.warn("choix de service table:", this.chxTable);
            const modal = await this.modalController.create({
                component: StripePaymentComponent,
                componentProps: {
                    totalAmount: this.countTotal(),
                    choixService: this.choixService,
                    pourboire: this.pourboire
                },
                id: 'stripe-modal'
            });

            await modal.present();


        }

    }

    onClickTable(){
        this.chxComptoir=0;
        this.chxTable=1;
    }

    onClickComptoir(){
        this.chxComptoir=1;
        this.chxTable=0;
    }


    onPourboireBlur(event) {
        if (this.pourboire < 0) {
            this.pourboire = 0;
        }
        this.pourboire = Number(this.pourboire.toFixed(2));
    }

    /** Création de toast avec message et durée personnalisables */
    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }
}
