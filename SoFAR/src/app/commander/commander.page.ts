import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, IonContent, ModalController, NavParams, ToastController} from "@ionic/angular";
import {HttpClient} from "@angular/common/http";
import {Categorie} from "../carte-pro/Categorie";
import {Article} from "../carte-pro/Article";
import {CommanderService} from "./commander.service";
import {ArticleDetailsComponent} from "./article-details/article-details.component";
import {Produit} from "./produit";
import {BasketDetailsComponent} from "./basket-details/basket-details.component";
import {GlobalVariable} from "../global";


@Component({
    selector: 'app-commander',
    templateUrl: './commander.page.html',
    styleUrls: ['./commander.page.scss'],
})
export class CommanderPage implements OnInit {
    @ViewChild(IonContent, {read: IonContent}) content: IonContent;

    panier: Produit[];

    categories: [Categorie];
    articles: [Article];
    search: string;

    idPro: number;
    name: string;
    serviceP: string;
    test;

    constructor(
        private modalController: ModalController,
        private toast: ToastController,
        private httpClient: HttpClient,
        private navParams: NavParams,
        private commanderService: CommanderService,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
        console.log('navParams:', this.navParams);
        this.search = '';
        this.idPro = this.navParams.get('id_pro');
        this.name = this.navParams.get('name');
        this.serviceP = this.navParams.get('service');
        console.warn("********************************************************", this.serviceP);
        this.commanderService.proService = this.serviceP;
        this.commanderService.initPanier().subscribe((panier: Produit[]) => {
            this.panier = panier;
        });
    }

    ionViewWillEnter() {
        /*
        Permet que meme si on fait plusieurs appel, emptyCard() n'est appelé qu'une seule fois
         */
        let executed = false;

        this.commanderService.fetchCategories(this.idPro).then(obs => obs.subscribe((res: any) => {
                this.categories = res.result;
                if (this.categories.length < 1 && !executed) {
                    /*
                    Pas de catégorie => pas d'article => carte vide
                     */
                    executed = true;
                    this.emptyCard().then();
                }
            }
        ));

        this.commanderService.fetchArticles(this.idPro).then(obs => obs.subscribe(() => {
                this.commanderService.getArticles().subscribe((art: [Article]) => {
                    this.articles = art;
                });
            }
        ));
        this.commanderService.idPro = this.idPro;
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    getNumberOfArticleInCategorieFromSearch(categorie: Categorie) {
        return this.articles
            .filter((article: Article) => article.article_categorie === categorie.categorie_id)
            .filter((article: Article) => article.article_nom.includes(this.search))
            .length;
    }

    async openArticle(art: Article) {
        if (art.article_dispo) {
            const modal = await this.modalController.create({
                component: ArticleDetailsComponent,
                componentProps: {article: art}
            });
            await modal.present();
        } else {
            console.log('This article is not available:', art);
        }
    }

    updatePage() {
        this.fetchCategories();
        this.fetchArticles();
    }

    fetchCategories() {
        this.commanderService.fetchCategories(this.idPro).then(obs => obs.subscribe());
    }

    fetchArticles() {
        this.commanderService.fetchArticles(this.idPro).then(obs => obs.subscribe());
    }

    countArticleInPanier(): number {
        let count = 0;
        for (const prod of this.panier) {
            count += prod.quantity;
        }
        return count;
    }

    async goToBasket() {
        const modal = await this.modalController.create({
            component: BasketDetailsComponent,
            id: 'basket-modal'
        });
        await modal.present();
    }

    /*
    Scroll jusqu'à la catégorie dans la liste
     */
    scrollToCat(catId: number) {
        const y = document.getElementById('cat-' + String(catId)).getBoundingClientRect().top - 50;
        console.log('Scroll to categorie:' + String(catId) + ' - ' + y);
        this.content.scrollToPoint(0, y, 300).then();
    }

    /*
    Alert si la carte est vide
     */
    async emptyCard() {
        console.log('Carte pro vide');
        const alert = await this.alert.create({
            header: 'Carte vide',
            message: 'Le menu de cet établissement est vide. La commande est alors désactivé.',
            buttons: [
                {
                    text: 'Fermer',
                    role: 'cancel',
                    handler: () => {
                        this.onCancel();
                    }
                }
            ]
        });
        return alert.present();
    }
}
