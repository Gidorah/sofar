import {Article} from "../carte-pro/Article";

export interface Produit {
    article: Article;
    quantity: number;
}
