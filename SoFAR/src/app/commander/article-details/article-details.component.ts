import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams, ToastController} from "@ionic/angular";
import {HttpClient} from "@angular/common/http";
import {CommanderService} from "../commander.service";
import {Article} from "../../carte-pro/Article";
import {Produit} from "../produit";

@Component({
    selector: 'app-article-details',
    templateUrl: './article-details.component.html',
    styleUrls: ['./article-details.component.scss'],
})
export class ArticleDetailsComponent implements OnInit {

    article: Article;
    isArticleInPanier: boolean;

    quantity: number;

    constructor(
        private modalController: ModalController,
        private toast: ToastController,
        private httpClient: HttpClient,
        private navParams: NavParams,
        private commanderService: CommanderService
    ) {
    }

    ngOnInit() {
        this.article = this.navParams.get('article');
        const product: Produit = this.commanderService.getProductFromArticle(this.article);
        if (product) {
            this.isArticleInPanier = true;
            this.quantity = product.quantity;
        } else {
            this.isArticleInPanier = false;
            this.quantity = 1;
        }
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    decrQuantity() {
        if (this.quantity > 1) {
            this.quantity--;
        }
    }

    incrQuantity() {
        this.quantity++;
    }


    async addToPanier() {
        console.log(this.quantity);
        console.log(this.article);
        const produit: Produit = {
            quantity: this.quantity,
            article: this.article
        };
        this.commanderService.addToPanier(produit);
        await this.onCancel();
    }

    async removeFromPanier() {
        this.commanderService.removeFromPanier(this.article);
        await this.onCancel();
    }

    prixTotal(): number {
        return (Math.round((this.article.article_prix * this.quantity + Number.EPSILON) * 100) / 100);
    }
}
