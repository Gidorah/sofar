import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {Categorie} from "../carte-pro/Categorie";
import {Article} from "../carte-pro/Article";
import {HttpClient} from "@angular/common/http";
import {ProService} from "../pro/pro.service";
import {GlobalVariable} from "../global";
import {tap} from "rxjs/operators";
import {CarteProService} from "../carte-pro/carte-pro.service";
import {Produit} from "./produit";

@Injectable({
    providedIn: 'root'
})
export class CommanderService {
    //servicePRO;
    //IDPRO;
    panier: BehaviorSubject<Produit[]> = new BehaviorSubject<Produit[]>(new Array<Produit>());
    // tslint:disable-next-line:variable-name
    private _idPro: number;
    proService;


    constructor(
        private carteProService: CarteProService
    ) {
    }

    getCategories(): Observable<[Categorie]> {
        return this.carteProService.getCategories();
    }

    getCategoriesValue(): [Categorie] {
        return this.carteProService.getCategoriesValue();
    }

    getArticles(): Observable<[Article]> {
        return this.carteProService.getArticles();
    }

    getArticlesValue(): [Article] {
        return this.carteProService.getArticlesValue();
    }

    async fetchCategories(idPro: number) {
        return this.carteProService.fetchCategories(idPro);
    }

    async fetchArticles(idPro: number) {
        return this.carteProService.fetchArticles(idPro);
    }

    initPanier(): Observable<Produit[]> {
        this.panier.next(new Array<Produit>());
        return this.getPanier();
    }

    getPanier(): Observable<Produit[]> {
        return this.panier;
    }

    getPanierValue(): Produit[] {
        return this.panier.value;
    }
/*
    getServices(): Observable< string > {
       // console.warn(this.carteProService.getServices());
        return this.carteProService.getServices();
    }

    getServicesValue(): string {
        return this.carteProService.getServicesValue();
    }*/

    addToPanier(product: Produit): void {
        const newPanier = this.panier.value;
        /*
        On regarde si l'article composant le produit est déjà dans le panier
        Si oui, on met a jour la nouvelle quantité d'article choisit
        Sinon, on ajoute simplement le produit
         */
        let productIsInBasket = false;
        for (const prod of newPanier) {
            if (prod.article.article_id === product.article.article_id) {
                prod.quantity = product.quantity;
                productIsInBasket = true;
            }
        }
        if (!productIsInBasket) {
            newPanier.push(product);
        }
        this.panier.next(newPanier);
    }

    removeFromPanier(article: Article): void {
        const newPanier = this.panier.value.filter((prod: Produit) => prod.article.article_id !== article.article_id);
        this.panier.next(newPanier);
    }

    isArticleInPanier(article: Article): boolean {
        for (const prod of this.panier.value) {
            if (prod.article.article_id === article.article_id) {
                return true;
            }
        }
        return false;
    }

    getProductFromArticle(article: Article): Produit {
        for (const prod of this.panier.value) {
            if (prod.article.article_id === article.article_id) {
                return prod;
            }
        }
        return null;
    }

    get idPro(): number {
        return this._idPro;
    }

    set idPro(value: number) {
        this._idPro = value;
    }




}
