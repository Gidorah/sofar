import {NgModule, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {Camera} from '@ionic-native/camera/ngx';
import { IonicStorageModule } from '@ionic/storage-angular';
import {FormsModule} from '@angular/forms';
import {Ionic4DatepickerModule} from '@logisticinfotech/ionic4-datepicker';
import {MapComponent} from './map/map.component';
import {CommanderPage} from "./commander/commander.page";
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {ReserverPage} from "./reserver/reserver.page";
import {InfosUserComponent} from "./infos-user/infos-user.component";
import {HoursChangerComponent} from "./mon-compte-partenaire/hours-changer/hours-changer.component";
import {IonicRatingModule} from "ionic4-rating";
import {Crop} from "@ionic-native/crop";
import {Push} from "@ionic-native/push/ngx";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import {Stripe} from "@ionic-native/stripe/ngx";
import {StripePaymentComponent} from "./commander/basket-details/stripe-payment/stripe-payment.component";
import {DetailsCommandeComponent} from "./commande-client/details-commande/details-commande.component";
import {BasketDetailsComponent} from "./commander/basket-details/basket-details.component";
import {OtherParamComponent} from "./services/other-param/other-param.component";

registerLocaleData(localeFr);

@NgModule({
  declarations: [
      AppComponent,
      MapComponent,
      CommanderPage,
      ReserverPage,
      InfosUserComponent,
      HoursChangerComponent,
      StripePaymentComponent,
      BasketDetailsComponent,
      OtherParamComponent
  ],
  entryComponents: [],
    imports: [
        CommonModule,
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        IonicStorageModule.forRoot(),
        FormsModule,
        Ionic4DatepickerModule,
        IonicRatingModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        Diagnostic,
        LaunchNavigator,
        {provide: [RouteReuseStrategy, LOCALE_ID], useClass: IonicRouteStrategy, useValue: 'fr-FR'},
        Push,
        Stripe
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
