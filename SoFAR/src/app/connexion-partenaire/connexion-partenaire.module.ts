import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConnexionPartenairePageRoutingModule } from './connexion-partenaire-routing.module';

import { ConnexionPartenairePage } from './connexion-partenaire.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConnexionPartenairePageRoutingModule
  ],
  declarations: [ConnexionPartenairePage]
})
export class ConnexionPartenairePageModule {}
