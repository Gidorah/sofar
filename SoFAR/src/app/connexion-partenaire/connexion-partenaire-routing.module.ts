import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConnexionPartenairePage } from './connexion-partenaire.page';

const routes: Routes = [
  {
    path: '',
    component: ConnexionPartenairePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConnexionPartenairePageRoutingModule {}
