import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ModalController, ToastController, AlertController} from '@ionic/angular';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StorageService} from '../storage.service';
import {GlobalVariable} from "../global";
import {PersonnelService} from "../personnel.service";
import {ProService} from "../pro/pro.service";

@Component({
    selector: 'app-connexion-partenaire',
    templateUrl: './connexion-partenaire.page.html',
    styleUrls: ['./connexion-partenaire.page.scss'],
})
export class ConnexionPartenairePage implements OnInit {
    login: string = '';
    mdp: string = '';

    constructor(
        private router: Router,
        public toast: ToastController,
        public modalctrl: ModalController,
        public http: HttpClient,
        public storageservice: StorageService,
        private personnelService: PersonnelService,
        private proService: ProService,
        public alert: AlertController) {
    }

    ngOnInit() {
        this.proService.getAutoConnectPro().then((res: boolean) => {
            if (res) {
                console.log('Pro has asked to stay connected: auto-connection');
                this.proService.fetchPro().then(obs => obs.subscribe(() =>
                    this.router.navigate(['/tabs-partenaire'], { replaceUrl: true })
                ));
            }
        });

        this.personnelService.getAutoConnect().then((res: boolean) => {
            if (res) {
                console.log('Pers has asked to stay connected: auto-connection');
                this.personnelService.fetchPers().then(obs => obs.subscribe());
                this.router.navigate(['/tabs-personnel'], { replaceUrl: true });
            }
        });
    }

    /**Fonction qui connecte l'utilisateur à l'application */

    // login() {
    //     //Vérification que tous les champs sont complétés
    //     if (this.email !== '' && this.mdp !== '') {
    //         const body = {
    //             email: this.email,
    //             mdp: this.mdp,
    //             action: 'login'
    //         };
    //         const headers: any = new HttpHeaders({'Content-Type': 'application/json'});
    //         this.http.post("/sofar_api/Pro/manage_data.php", JSON.stringify(body), headers).subscribe(data => {
    //                 //Si un pro est trouvé, connexion et initialisation de ses données dans le storage
    //                 if (data !== null) {
    //                     this.storageservice.setPro('storage_pro', data).then((res) => {
    //                         this.router.navigate(['/tabs-partenaire']);
    //                         this.showToast('Bienvenue !', 500);
    //                     });
    //                 }
    //                 //Sinon message d'erreur
    //                 else {
    //                     this.showToast(" Désolé ! Le mail ou le mot de passe est invalide.", 1000)
    //                 }
    //             },
    //             (error: any) => {
    //                 console.log(error);
    //             });
    //     } else {
    //         this.showToast('Tous les champs sont requis', 1000);
    //     }
    // }

    logIn() {
        // Vérification que tous les champs sont complétés
        if (this.login !== '' && this.mdp !== '') {
            if (this.isEmail(this.login)) {
                /*
                Connexion avec un mail -> il s'agit du compte du patron
                 */
                const pro = {
                    email: this.login,
                    mdp: this.mdp,
                    action: 'login'
                };
                this.proService.loginPro(pro, true).subscribe(data => {
                        if (data.error === '') {
                            this.router.navigate(['/tabs-partenaire'], { replaceUrl: true });
                        } else if (data.error === 'wrong-pw') {
                            this.showToast(' Désolé ! Le mail ou le mot de passe est invalide.', 3000);
                        } else {
                            this.showToast(' Une erreur est survenu', 3000);
                        }
                    },
                    (error: any) => {
                        console.log(error);
                        this.showToast('Requête échouée', 1000);
                    });
            } else if (this.isID(this.login)) {
                /*
                Connexion avec un ID -> serveur
                 */
                const body = {
                    login: this.login,
                    mdp: this.mdp,
                    action: 'login'
                };
                this.personnelService.login(body, true).subscribe(data => {
                    if (data.error === '') {
                        this.router.navigate(['/tabs-personnel'], { replaceUrl: true });
                    } else if (data.error === 'wrong-pw') {
                        this.showToast(' L\'ID ou le mot de passe est invalide.', 3000);
                    } else {
                        this.showToast(' Une erreur est survenu', 3000);
                    }
                });
            } else {
                // wrong ID
                this.showToast('ID/Email erroné', 2000);
            }
        } else {
            this.showToast('Tous les champs sont requis', 1000);
        }
    }

    isEmail(email) {
        const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return re.test(email);
    }

    isID(id) {
        const re = /^[A-Z0-9]{6}$/;
        return re.test(id);
    }

    async showToast(message, duration) {
        let toast = await this.toast.create({
            message: message,
            duration: duration
        });
        await toast.present();
    }

    /**Alert mot de passe oublié */
    async forgotPassword() {
        const alert = await this.alert.create({
            header: 'Mot de passe oublié',
            message: 'Entrez votre email pour réinitialiser votre mot de passe',
            inputs: [
                {
                    name: 'mail',
                    value: '',
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Réinitialiser',
                    handler: (alertData) => {
                        if (this.isEmail(alertData.mail)) {
                            let email = alertData.mail;
                            this.resetPassword(email);
                        } else if (this.isID(alertData.mail)) {
                            this.persMdpOublieAlert();
                        } else {
                            this.showToast('Erreur dans l\'email renseignée', 2000);
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

    async persMdpOublieAlert() {
        const alert = await this.alert.create({
            header: 'Mot de passe oublié',
            message: `
                <p>Si vous êtes serveur et que vous avez oublié votre mot de passe, 
                contacter le reponsable du compte partenaire So FAR lié à votre établissement afin qu'il puisse changer
                manuellement votre mot de passe.</p>
            `,
            buttons: [
                {
                    text: 'OK',
                    role: 'cancel'
                }
            ]
        });
        await alert.present();
    }

    /**Alert première connexion pour recevoir son mot de passe*/
    async generateMdp() {
        const alert = await this.alert.create({
            header: 'Entre ton mail pour recevoir ton mot de passe',
            inputs: [
                {
                    name: 'mail',
                    value: '',
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Envoyer',
                    handler: (alertData) => {
                        let email = alertData.mail;
                        this.firstConnection(email);
                    }
                }
            ]
        })
        await alert.present();
    }

    /**Envoi d'un nouveau de mot passe provisoire par mail si le mail existe dans la base */
    resetPassword(body) {
        let headers: any = new HttpHeaders({'Content-Type': 'application/json'});
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/reset_password.php', JSON.stringify(body), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe((data) => {
            if (data['check'] === 0) {
                this.showToast("Il n'y a pas de compte avec ce mail.", 2000);
            } else {
                this.showToast("Consulte ta boite mail pour te connecter à nouveau", 3000);
            }
        }, (error) => {
            console.log(error);
        })
    }

    /**Envoi d'un mot passe provisoire par mail si le mail existe dans la base */
    firstConnection(body) {
        let headers: any = new HttpHeaders({'Content-Type': 'application/json'});
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/first_connection.php', JSON.stringify(body), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).subscribe((data) => {
            if (data['check'] === 0) {
                this.showToast("Malheureusement tu n'as pas encore de compte. Inscris-toi!", 3000)
            } else {
                this.showToast("Consulte ta boite mail pour te connecter ", 3000);
            }
        }, (error) => {
            console.log(error);
        })

    }

    async becomePartner() {
        const alert = await this.alert.create({
            header: 'Devenir partenaire',
            message: 'Si vous possédez un établissement et souhaitez devenir partenaire, veuillez contacter sofar.api@gmail.com', // TODO : mail pro
            buttons: [
                {
                    text: 'Fermer',
                    role: 'cancel',
                }
            ]
        });
        await alert.present();
    }

    coServ() {

    }



    onEnter(keyCode: number) {
        // Réagit à la touche 'enter' du clavier
        if (keyCode === 13) {
            this.logIn();
        }
    }
}
