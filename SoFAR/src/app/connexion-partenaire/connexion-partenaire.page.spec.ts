import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConnexionPartenairePage } from './connexion-partenaire.page';

describe('ConnexionPartenairePage', () => {
  let component: ConnexionPartenairePage;
  let fixture: ComponentFixture<ConnexionPartenairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnexionPartenairePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConnexionPartenairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
