import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";
import {StorageService} from "../../storage.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GlobalVariable} from "../../global";

@Component({
    selector: 'position-hours-changer',
    templateUrl: './position-changer.component.html',
    styleUrls: ['./position-changer.component.scss'],
})
export class PositionChangerComponent implements OnInit {

    adresse='';
    ma_pos=false;

    constructor(private modalController: ModalController,
                public toast: ToastController,
                private router: Router,
                private storageService: StorageService,
                private loadingController: LoadingController,
                public http: HttpClient,
                public navParams: NavParams) { }

    ngOnInit() {
        let position = this.navParams.get('localisation');
        if(position=="Ma position"){
            this.ma_pos=true;
        }
        else if (position!="Sélectionner"){
            this.adresse=position;
        }

    }

    onCancel() {
        this.modalController.dismiss();
    }

    async onSubmit() {
        if(this.ma_pos==true){
            this.modalController.dismiss("Ma position");
        }
        else{
            this.modalController.dismiss(this.adresse);
        }
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    oninputadresse(e){
        this.adresse = e.target.value;
    }
}
