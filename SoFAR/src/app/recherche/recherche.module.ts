import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecherchePageRoutingModule } from './recherche-routing.module';

import { RecherchePage } from './recherche.page';
import { RouterModule } from '@angular/router';
import {PositionChangerComponent} from "./position-changer/position-changer.component";
import {ResultatRecherchePageModule} from "../resultat-recherche/resultat-recherche.module";
import {IonicRatingModule} from "ionic4-rating";
import { AccordeonComponent } from '../components/accordeon/accordeon.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RecherchePageRoutingModule,
        RouterModule.forChild([
            {
                path: '',
                component: RecherchePage
            }
        ]),
        ResultatRecherchePageModule,
        IonicRatingModule
    ],
    exports: [
        AccordeonComponent
    ],
    declarations: [RecherchePage, PositionChangerComponent, AccordeonComponent]
})
export class RecherchePageModule {}
