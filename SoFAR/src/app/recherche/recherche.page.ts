import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, ToastController} from '@ionic/angular';
import {Geolocation} from "@ionic-native/geolocation";
import {PositionChangerComponent} from "./position-changer/position-changer.component";
import * as $ from "jquery";
import {HttpClient} from "@angular/common/http";
import {GlobalVariable} from "../global";
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {InfosResultatPage} from "../infos-resultat/infos-resultat.page";
import {catchError, timeout} from "rxjs/operators";
import {StorageService} from "../storage.service";
import {UserService} from "../user/user.service";

@Component({
    selector: 'app-recherche',
    templateUrl: './recherche.page.html',
    styleUrls: ['./recherche.page.scss'],
})
export class RecherchePage implements OnInit {
    choices_criteres= ['Bar', 'Restaurant', 'Boite de nuit', 'Tous'];
    nbCritere = 4;
    myInput: any = '';
    element: any = '';
    rayon: number = 5;
    public items: any = [];
    localisation = '';
    coords_user;
    type='Tous';
    item: any = [];
    research: { search: string, coords_user: any, type:string, rayon:any, tri : string, offset:number, action: string};
    afficher: boolean = false;
    choices: any;
    jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
    noresult=false;
    desactive=false;
    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;
    chargement_adresse=false;

    constructor(public modalCtrl: ModalController,
                public alertController: AlertController,
                private diagnostic: Diagnostic,
                private storageService: StorageService,
                public http: HttpClient,
                private userService: UserService,
                private toast: ToastController) {
        this.items = [
            {expanded: false}
        ];
    }

    async onInput(event) {
        this.afficher=false;
        this.noresult=false;
        this.myInput = event.srcElement.value;
        this.item=[];
        this.research.offset=0;
        this.research.rayon=this.rayon*1000;
        await this.postHtml(this.research);
    }

    async presentAlert() {
        const alert = await this.alertController.create({
            header: 'Attention',
            message: 'Vous devez choisir une position',
            buttons: ['OK']
        });

        await alert.present();
    }

    async presentAlert_geolocalisation() {
        const alert = await this.alertController.create({
            header: 'Géolocalisation désactivée',
            message: 'Vous devez activer la géolocalisation dans vos paramètres',
            buttons: ['OK']
        });

        await alert.present();
    }

    async presentAlert_adresse() {
        const alert = await this.alertController.create({
            header: 'L\'adresse saisie n\'est pas valide',
            buttons: ['OK']
        });

        await alert.present();
    }

    // Voir si la card est etendue ou non
    expandItem(item): void {
        if (item.expanded) {
            item.expanded = false;
        } else {
            this.items.map(listItem => {
                if (item === listItem) {
                    listItem.expanded = !listItem.expanded;
                } else {
                    listItem.expanded = false;
                }
                return listItem;
            });
        }
    }

    async ngOnInit() {
        this.localisation = 'Sélectionner une position';
        this.research = {
            search: '',
            coords_user: [0,0],
            type: '',
            rayon: 5000,
            tri: "Distance : par ordre croissant",
            offset:0,
            action: 'search'
        };
        // pour trigger la permission de geolocation on fait une demande
        Geolocation.getCurrentPosition().then(async (res) => {
            console.log("POSITION ACTIVEE");
            this.research.coords_user = [res.coords.longitude, res.coords.latitude];
            this.desactive=false;
            this.afficher=false;
            this.noresult=false;
            this.localisation = 'Ma position';
            this.item=[];
            this.research.offset=0;
            if (this.type === 'Tous') {
                this.research.type = '';
            }
            else{
                this.research.type = this.type;
            }
            console.log(this.research.coords_user);
            await this.postHtml(this.research);
            this.choices = ["Distance : par ordre croissant", "Distance : par ordre décroissant", "Note : par ordre croissant", "Note : par ordre décroissant"];

        }).catch(()=>{
            console.log("POSITION DESACTIVEE")
            }
        );
    }


    async changePosition() {

        let localisation;

        if(this.localisation!="Sélectionner une position"){
            localisation=this.localisation;
        }
        else{
            localisation='';
        }

        const modal = await this.modalCtrl.create({
            component: PositionChangerComponent,
            componentProps: {
                localisation: localisation,
            },
        });

        modal.onDidDismiss().then(data => {
            this.afficher=false;
            if (data['data'] && data['data'] !== "") {
                if (data['data'] != "Ma position") {
                    this.chargement_adresse=true;
                    let adresse = data['data'];
                    $.getJSON('https://api-adresse.data.gouv.fr/search/?q=' + adresse, async (data) => {
                        if (data.features[0] == undefined) {
                            await this.presentAlert_adresse();
                            this.afficher=true;
                            this.chargement_adresse=false;
                        } else {
                            this.research.coords_user = data.features[0].geometry.coordinates;
                            if (data.features[0].properties.label.split(" ") == data.features[0].properties.label) { //Si plusieurs villes sont trouvées avec le meme nom on affiche le code postal
                                this.localisation = data.features[0].properties.label + " (" + data.features[0].properties.postcode + ")";
                            } else {
                                this.localisation = data.features[0].properties.label;
                            }
                            this.item=[];
                            this.research.offset=0;
                            this.research.rayon=this.rayon*1000;
                            this.desactive=false;
                            this.afficher=false;
                            this.noresult=false;
                            this.chargement_adresse=false;
                            await this.postHtml(this.research);
                        }
                    });
                } else {
                    if (this.localisation != 'Ma position') {
                        Geolocation.getCurrentPosition().then(async (res) => {
                            console.log("POSITION ACTIVEE");
                            this.research.coords_user = [res.coords.longitude, res.coords.latitude];
                            this.localisation = 'Ma position';
                            this.item=[];
                            this.research.offset=0;
                            this.desactive=false;
                            this.afficher=false;
                            this.noresult=false;
                            if (this.type === 'Tous') {
                                this.research.type = '';
                            }
                            else{
                                this.research.type = this.type;
                            }
                            console.log(this.research.coords_user);
                            await this.postHtml(this.research);

                        }).catch(()=>{
                            this.afficher=true;
                            this.presentAlert_geolocalisation();
                            console.log("POSITION DESACTIVEE");
                            }
                        );
                    }
                }

            }
        });

        await modal.present();
    }

    async clickCards(item) {
        const modal = await this.modalCtrl.create({
            component: InfosResultatPage,
            componentProps: {item: item},
            cssClass: "modal_recherche"
        });
        return await modal.present();
    }

    async postHtml(item) {
        this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Pro/manage_data.php', JSON.stringify(item), {
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).pipe(
            timeout(1000),
            catchError(e => {
                this.noresult=true;
                return null;
            })
        ).subscribe(async (data:Array<any>) => {
            let nb_reponses = data.length;
            if (nb_reponses==0 && this.item.length==0){
                this.noresult=true;
            }
            else if(nb_reponses<3){
                this.desactive=true;
            }
            for (let i = 0; data[i]; i++) {
                let name = data[i][0];
                let adresse = data[i][1];
                let description = data[i][2];
                let categorie = data[i][5];
                let happy = data[i][6];
                let terrasse = data[i][7];
                let fav = 0;
                let id = data[i][4];
                let distance = data[i][8];
                let note = data[i][9];
                let horaires = data[i][10];
                let service = data[i][25];
                let ouvert = data[i][21] == 1;
                let distance_string;
                let coordonnees  = [data[i][24],data[i][23]];
                const horaireSemaine = await this.userService.getHoraires(id);
                if (distance < 1) {
                    distance_string = distance.toString().split('.')[1].substring(0, 3) + " m";
                } else {
                    distance_string = distance.toString().substring(0, 3) + " km";
                }
                let img;
                let res:any = await this.storageService.fetchProPhoto(id).toPromise();
                if (res.pro.photo !== "") {
                    img = 'data:image/jpeg;base64,' + res.pro.photo;
                }
                else{
                    img = "../assets/images/logo_dark.svg"
                }
                this.item.push({
                    name: name,
                    adresse: adresse,
                    description: description,
                    id: id,
                    categorie: categorie,
                    happy: happy,
                    terrasse: terrasse,
                    fav: fav,
                    distance: distance_string,
                    distance_nbr: distance,
                    ouvert: ouvert,
                    horaires: horaires,
                    horaires_semaine: horaireSemaine,
                    img: img,
                    note:note,
                    coordonnees: coordonnees,
                    coords_user: this.research.coords_user,
                    service: service,
                    commander_dispo: data[i][26] === '1',
                    need_resa_details: data[i][22] === '1'
                });
                if (i == nb_reponses - 1) {
                    this.noresult = this.item.length == 0;
                    this.afficher=true;
                }
            }
        }, (error: any) => {
            console.log(error);
        });
    }

    async pop() {
        await this.modalCtrl.dismiss();
    }

    async refresh_criteres(e,crit) {
        this.afficher=false;
        this.noresult=false;
        this.research.rayon=this.rayon*1000;
        this.item=[];
        this.research.offset=0;
        if(crit!="rayon") {
            if (crit != "type") {
                this.research.tri = e;
            } else if (e == 'Tous') {
                this.research.type = '';
            } else {
                this.research.type = e;
            }
        }
        this.desactive=false;
        await this.postHtml(this.research);
    }

    async showToast(msg, temps) {
        const toast = await this.toast.create({
            message: msg,
            duration: temps
        });
        await toast.present();
    }

    loadData(e){
        setTimeout(async () => {
            this.research.offset+=4;
            await this.postHtml(this.research);
            e.target.complete();
        }, 500);
    }

}
