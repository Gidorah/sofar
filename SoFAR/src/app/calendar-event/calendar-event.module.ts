import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalendarEventPageRoutingModule } from './calendar-event-routing.module';

import { CalendarEventPage } from './calendar-event.page';
import { NgCalendarModule } from 'ionic2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalendarEventPageRoutingModule,
    NgCalendarModule
  ],
  declarations: [CalendarEventPage]
})
export class CalendarEventPageModule {}
