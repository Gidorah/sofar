import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from '../storage.service';
import {GlobalVariable} from "../global";

@Component({
  selector: 'app-calendar-event',
  templateUrl: './calendar-event.page.html',
  styleUrls: ['./calendar-event.page.scss'],
})

/**
 * Calendar event est la page d'ajout d'un évènement en cliquant sur le bouton '+' ou le bouton 'ajout event'
 */
export class CalendarEventPage implements AfterViewInit {

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };
  viewTitle: string;
  event = {
    title: '',
    desc: '',
    startTime: null,
    endTime: null,
    allDay: false
  };
  hour_start: '';
  hour_end: '';
  dateForm: any;

  reduc: any;
  evHappy: Array<{idp, dateHH, debutHH, finHH, reducHH, action}> = [];
  modalReady = false;
  happyh = false;

  oktext = GlobalVariable.OKTEXT;
  canceltext = GlobalVariable.CANCELTEXT;


  constructor(private modalCtrl: ModalController,
              public http: HttpClient,
              public service: StorageService ) { }
/**
 * pour ajuster la taille du calendrier dans la page event sur le telephone
 */
  ngAfterViewInit(){
    setTimeout(() => {
      this.modalReady = true;
    }, 0);
  }
  /**
   * selection happy hour
   */

   isHappy(){
     if(this.happyh === false){
       this.happyh = true;
     }
     else{
       this.happyh = false;
     }
   }
/**
 * save renvoie l'élément event(title, description, startTime, endTime) à Calendar via un modalController
 * et renvoie l'objet event happy hour à la base de données
 */
  save() {
    console.log('here');
    this.event.startTime.setHours(this.hour_start.split('T')[1].split(':')[0]);
    this.event.startTime.setMinutes(this.hour_start.split('T')[1].split(':')[1]);
    this.event.endTime.setHours(this.hour_end.split('T')[1].split(':')[0]);
    this.event.endTime.setMinutes(this.hour_end.split('T')[1].split(':')[1]);

    if (this.happyh === true){
      /*
      console.log(this.event.startTime.getFullYear());
      console.log(this.event.startTime.getUTCMonth());
      console.log(this.event.startTime.getDate());
      this.dateForm= this.event.startTime.getFullYear()+ '-' + this.event.startTime.getMonth() +'-'+
                    this.event.startTime.getDate() ;
*/
      this.dateForm = this.hour_start.split('T')[0];
      this.evHappy = [{
        // idp: this.service.getPro()[0][0],
        idp: null,
        dateHH:  this.dateForm,
        debutHH: this.hour_start.split('T')[1].split('.')[0],
        finHH: this.hour_end.split('T')[1].split('.')[0],
        reducHH : this.reduc,
        action: 'update'

      }];
      console.log(this.evHappy);
      this.postHtml(this.evHappy[0]);
    }
    this.modalCtrl.dismiss({event: this.event});
  }
 /**
  * Pour changer le titre du mois courant
  */
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
 /**
  * Permet de planifier un event sur un jour autre que le mois courant
  */
  onTimeSelected(ev) {
    console.log('ev: ', ev);
    this.event.startTime = ev.selectedTime;
    const start = this.event.startTime;
    this.event.endTime = new Date(
        start.getFullYear(),
        start.getMonth(),
        start.getDate()
    );
  }
/**
 * envoie les données du happy hour à la base de données
 */
  postHtml(element) {
    console.log(element);
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post(GlobalVariable.BASE_API_URL + '/sofar_api/Calendrier/manage_data.php', JSON.stringify(element), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).subscribe(data => {
      console.log(data);
  }, (error: any) => {
    console.log(error);
  });
  }
/**
 * Bouton 'x' annulation de l'ajout d'un event
 */
  close() {
    this.modalCtrl.dismiss();
  }

}
