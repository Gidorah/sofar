import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {Status} from "./Status";
import {ProService} from "./pro/pro.service";
import {PersonnelService} from "./personnel.service";
import {Pro} from "./pro/pro";
import {Personnel} from "./personnel";

@Injectable({
    providedIn: 'root'
})
export class ProStatusService {

    private status: BehaviorSubject<Status> = new BehaviorSubject<Status>(Status.null);

    /*
    Ce service a pour fonction de gérer le type d'utilisateur connecté dans la partie partenaire de l'application:
        - boss: il s'agit du gérant du compte principal de l'établissement partenaire
        - personnel: il s'agit d'un serveur ou autre, sous compte lié à l'établissement partenaire, géré par le boss (ce
            compte possède moins de droit qu'un compte 'boss')
     */
    constructor(
        private proService: ProService,
        private persService: PersonnelService,
    ) {
        this.updateStatus();
    }

    /*
      Check si boss ou serveur de connecté pour update le status
       */
    updateStatus(): void {
        this.proService.getPro().subscribe((pro: Pro) => {
            if (pro) {
                this.status.next(Status.boss);
                console.log('Status in service is', this.status.value);
            }
        });
        this.persService.getPersonnel().subscribe((pers: Personnel) => {
            if (pers) {
                this.status.next(Status.pers);
                console.log('Status in service is', this.status.value);
            }
        });
    }

    getStatus(): Observable<Status> {
        return this.status;
    }

    /*
    Renvoi le bon token en fonction du type d'utilisateur connecté
     */
    async getToken(): Promise<any> {
        switch (this.status.value) {
            case Status.boss:
                return await this.proService.getTokenProObs();
            case Status.pers:
                return await this.persService.getTokenObs();
            case Status.null:
                return null;
        }
    }

    getNomPro(): string {
        switch (this.status.value) {
            case Status.boss:
                return this.proService.getProValue().nom_pro;
            case Status.pers:
                return this.persService.getEtabValue().nom_pro;
            case Status.null:
                return null;
        }
    }


    async getCommanderDispo(): Promise<any> {
        switch (this.status.value) {
            case Status.boss:
                return this.proService.getProValue().commander_dispo;
            case Status.pers:
                return this.persService.getEtabValue().commander_dispo;
            case Status.null:
                return null;
        }
    }
}
