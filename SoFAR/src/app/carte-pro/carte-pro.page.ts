import {Component, OnInit} from '@angular/core';
import {ActionSheetController, AlertController, ModalController} from "@ionic/angular";
import {CreateCategorieComponent} from "./create-categorie/create-categorie.component";
import {ProService} from "../pro/pro.service";
import {GlobalVariable} from "../global";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {Categorie} from "./Categorie";
import {CarteProService} from "./carte-pro.service";
import {CreateArticleComponent} from "./create-article/create-article.component";
import {Article} from "./Article";
import {EditArticleComponent} from "./edit-article/edit-article.component";
import {EditCategorieComponent} from "./edit-categorie/edit-categorie.component";
import {ServicesComponent} from "./services/services.component";

@Component({
    selector: 'app-carte-pro',
    templateUrl: './carte-pro.page.html',
    styleUrls: ['./carte-pro.page.scss'],
})
export class CarteProPage implements OnInit {

    categories: [Categorie];
    articles: [Article];
    services: "Service";

    search: string;

    constructor(
        private actionSheetController: ActionSheetController,
        private modalController: ModalController,
        private proService: ProService,
        private carteProService: CarteProService,
        private httpClient: HttpClient,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
        this.search = '';
    }

    ionViewWillEnter() {
        this.carteProService.getCategories().subscribe((cat: [Categorie]) => {
            this.categories = cat;
        });
        this.carteProService.getArticles().subscribe((art: [Article]) => {
            this.articles = art;
        });
        this.updatePage();
    }

    async openMenu() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Gestion de la carte',
            buttons: [{
                text: 'Ajouter une catégorie',
                icon: 'list',
                handler: () => {
                    this.addCategorie();
                }
            }, {
                //     text: 'Modifier une catégorie',
                //     icon: 'pencil',
                //     handler: () => {
                //         // this.EditCategorie();
                //     }
                // }, {
                text: 'Ajouter un article',
                icon: 'pricetag',
                handler: () => {
                    this.addItem(null);
                }
            }, {
                text: 'Services commandes',
                icon: 'restaurant',
                handler:() => {
                    this.servicesTable();
                }

            }, {
                text: 'Annuler',
                icon: 'close',
                role: 'cancel'
            }]
        });

        await actionSheet.present();

        actionSheet.onDidDismiss().then(() => {
            console.log('Carte pro action sheet dismissed.');
        });
    }

    async addCategorie() {
        const modal = await this.modalController.create({
            component: CreateCategorieComponent
        });
        await modal.present();
        await modal.onDidDismiss().then(() => this.updatePage());
    }

    async addItem(catId?: Categorie) {
        if (this.categories && this.categories.length > 0) {
            const modal = await this.modalController.create({
                component: CreateArticleComponent,
                componentProps: {cat_id: catId}
            });
            await modal.present();
            await modal.onDidDismiss().then(() => this.updatePage());
        } else {
            const alert = await this.alert.create({
                header: 'Ajouter un article',
                message: 'Pour ajouter un article, veuillez créer au moins une catégorie',
                buttons: [
                    {
                        text: 'Fermer',
                        role: 'cancel',
                    }, {
                        text: 'Créer une categorie',
                        handler: () => {
                            this.addCategorie();
                        }
                    }
                ]
            });
            await alert.present();
        }
    }

    async servicesTable(){
        const modal = await this.modalController.create({
            component: ServicesComponent
        });
        await modal.present();
        await modal.onDidDismiss().then(() => this.updatePage());
    }

    async editArticle(article: Article) {
        const modal = await this.modalController.create({
            component: EditArticleComponent,
            componentProps: article
        });
        await modal.present();
        await modal.onDidDismiss().then(() => this.updatePage());
    }

    async editCategorie(categorie: Categorie) {
        const modal = await this.modalController.create({
            component: EditCategorieComponent,
            componentProps: categorie
        });
        await modal.present();
        await modal.onDidDismiss().then(() => this.updatePage());
    }

    updatePage() {
        this.fetchCategories();
        this.fetchArticles();
        //this.fetchServices();
    }

    fetchCategories() {
        this.carteProService.fetchCategories().then(obs => obs.subscribe());
    }

    fetchArticles() {
        this.carteProService.fetchArticles().then(obs => obs.subscribe());
    }

    fetchServices() {
        this.carteProService.fetchServices().then(obs => obs.subscribe());
    }

    getNumberOfArticleInCategorieFromSearch(categorie: Categorie) {
        return this.articles
            .filter((article: Article) => article.article_categorie === categorie.categorie_id)
            .filter((article: Article) => article.article_nom.includes(this.search))
            .length;
    }
}
