import {Component, OnInit} from '@angular/core';
import {Categorie} from "../Categorie";
import {Article} from "../Article";
import {GlobalVariable} from "../../global";
import {AlertController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {ProService} from "../../pro/pro.service";
import {CarteProService} from "../carte-pro.service";
import {HttpClient} from "@angular/common/http";
import {EditArticleComponent} from "../edit-article/edit-article.component";

@Component({
    selector: 'app-edit-categorie',
    templateUrl: './edit-categorie.component.html',
    styleUrls: ['./edit-categorie.component.scss'],
})
export class EditCategorieComponent implements OnInit {

    categorie: Categorie;

    articles: [Article];

    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;

    constructor(
        private modalController: ModalController,
        private proService: ProService,
        private carteProService: CarteProService,
        private toast: ToastController,
        private httpClient: HttpClient,
        private navParams: NavParams,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
        this.categorie = this.navParams.data as Categorie;
        console.log('categorie: ', this.categorie);
        this.carteProService.getArticles().subscribe((articles: [Article]) => {
            this.articles = articles.filter((art: Article) => art.article_categorie === this.categorie.categorie_id) as [Article];
            console.log('list articles', this.articles);
        });
        // this.articles = this.carteProService.getArticlesFromCategorie(this.categorie.categorie_id);
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    async onSubmit() {
        console.log('categorie: ', this.categorie);
        if (this.categorie.categorie_nom === '') {
            await this.showToast('Veuillez rentrer un nom de catégorie correct.', 1500);
        } else {
            this.editCategorie(
                this.categorie.categorie_nom,
                this.categorie.categorie_id
            );
        }
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    editCategorie(catNom: string, catId: number) {
        this.proService.getTokenProObs().then((tok: string) => {
            const body = {
                action: 'edit',
                token: tok,
                categorie_id: catId,
                nom: catNom
            };
            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Categories/manage.php', body,
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe(async (res: any) => {
                if (res.error !== '') {
                    console.error('Error editing categorie:', res.error);
                } else {
                    await this.onCancel();
                    await this.showToast('Categorie modifié.', 1000);
                }
            });
        });
    }

    async deleteArticle() {
        const alert = await this.alert.create({
            header: 'Supprimer la catégorie',
            message: 'Etes-vous sûr(e) de vouloir supprimé cette catégorie : ' + this.categorie.categorie_nom + ' ?' +
                ' Attention: l\'ensemble des articles associés à cette catégorie sera également supprimé.',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: () => {
                        this.proService.getTokenProObs().then((tok: string) => {
                            const body = {
                                action: 'delete',
                                token: tok,
                                categorie_id: this.categorie.categorie_id,
                            };
                            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Categories/manage.php', body,
                                {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                    }
                                }).subscribe(async (res: any) => {
                                if (res.error !== '') {
                                    console.error('Error removing catégorie:', res.error);
                                } else {
                                    await this.onCancel();
                                    await this.showToast('Catégorie supprimé.', 1000);
                                }
                            });
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async editArticle(article: Article) {
        const modal = await this.modalController.create({
            component: EditArticleComponent,
            componentProps: article
        });
        await modal.present();
        await modal.onDidDismiss().then(() => this.carteProService.fetchArticles().then(obs => obs.subscribe()));
    }

}
