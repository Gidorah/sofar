import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarteProPage } from './carte-pro.page';

const routes: Routes = [
  {
    path: '',
    component: CarteProPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarteProPageRoutingModule {}
