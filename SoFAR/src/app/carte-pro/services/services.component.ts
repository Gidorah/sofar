import { Component, OnInit } from '@angular/core';
import {ModalController, ToastController} from "@ionic/angular";
import {ProService} from "../../pro/pro.service";
import {GlobalVariable} from "../../global";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
})
export class ServicesComponent implements OnInit {

  serviceTable: boolean;
  serviceRetrait: boolean;
  service: string;

  constructor(
      private modalController: ModalController,
      private proService: ProService,
      private toast: ToastController,
      private httpClient: HttpClient
  ) {

  }

  ngOnInit() {
    this.serviceTable= true;
    this.serviceRetrait= true;


  }

  async onCancel() {
    await this.modalController.dismiss();
  }

  async onSubmit(){
    if(this.serviceTable== true && this.serviceRetrait == true){
      this.service = "tout";
    }else if (this.serviceTable == false && this.serviceRetrait == true){
      this.service = "comptoir";
    }else if (this.serviceTable == true && this.serviceRetrait == false){
      this.service = "table";
    }
    console.log('services: ', {
      serviceTable: this.serviceTable,
      serviceRetrait: this.serviceRetrait,
      service: this.service
    });
    if (this.serviceTable === false && this.serviceRetrait === false) {
      await this.showToast('Il faut choisir au moins une option', 1500);
    }else{

      this.proService.getTokenProObs().then((tok: string) => {
        const body = {
          token: tok,
          service: this.service,
          action: 'add'
        };
        this.httpClient.post( GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Services/manage.php', body,
            {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
              }
            }).subscribe(async (res: any) => {
              if (res.error !== ''){
                console.error('Error update services', res.error);
              } else {
                await this.onCancel();
                await this.showToast('Modification des services enregistrée', 1000);
              }
        });
      });

    }
  }

  async showToast(message, duration) {
    const toast = await this.toast.create({
      message,
      duration
    });
    await toast.present();
  }

  updateServiceTable($event: MouseEvent){
    this.serviceTable = !this.serviceTable;
  }

  updateServiceRetrait($event: MouseEvent){
    this.serviceRetrait = !this.serviceRetrait;
  }

}
