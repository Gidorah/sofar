import {Component, OnInit} from '@angular/core';
import {Categorie} from "../Categorie";
import {GlobalVariable} from "../../global";
import {AlertController, ModalController, NavParams, ToastController} from "@ionic/angular";
import {ProService} from "../../pro/pro.service";
import {HttpClient} from "@angular/common/http";
import {CarteProService} from "../carte-pro.service";
import {Article} from "../Article";

@Component({
    selector: 'app-edit-article',
    templateUrl: './edit-article.component.html',
    styleUrls: ['./edit-article.component.scss'],
})
export class EditArticleComponent implements OnInit {

    categories: [Categorie];
    categorie: Categorie;

    article: Article;

    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;

    constructor(
        private modalController: ModalController,
        private proService: ProService,
        private carteProService: CarteProService,
        private toast: ToastController,
        private httpClient: HttpClient,
        private navParams: NavParams,
        private alert: AlertController
    ) {
    }

    ngOnInit() {
        this.article = this.navParams.data as Article;
        this.categories = this.carteProService.getCategoriesValue();
        this.categories.forEach((cat: Categorie) => {
            if (cat.categorie_id === this.article.article_categorie) {
                this.categorie = cat;
            }
        });
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    async onSubmit() {
        console.log('article: ', this.article);
        if (this.article.article_nom === '') {
            await this.showToast('Le nom est trop court', 1500);
        } else if (this.article.article_desc === '') {
            await this.showToast('La description est trop courte', 1500);
        } else if (!this.article.article_prix) {
            await this.showToast('Insérer un prix', 1500);
        } else if (!this.article.article_taxe) {
            await this.showToast('Insérer le taux de taxe', 1500);
        } else {
            this.article.article_photo = "";
            this.editArticle(
                this.article.article_id,
                this.article.article_nom,
                Number(this.article.article_prix).toFixed(2),
                this.article.article_desc,
                this.article.article_dispo,
                this.categorie.categorie_id,
                this.article.article_photo,
                Number(this.article.article_taxe).toFixed(2)
            );
        }
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    updateCategorie($event) {
        const newCatId = $event.detail.value;
        this.categories.forEach((cat: Categorie) => {
            if (cat.categorie_id === newCatId) {
                this.categorie = cat;
            }
        });
    }

    updateDispo($event: MouseEvent) {
        this.article.article_dispo = !this.article.article_dispo;
    }

    editArticle(idArticle: number, nom: string, prix: string, desc: string, dispo: boolean, categorieId: number, photo: string, taxe: string) {
        this.proService.getTokenProObs().then((tok: string) => {
            const body = {
                action: 'edit',
                token: tok,
                article_id: idArticle,
                nom: nom,
                prix: prix,
                desc: desc,
                dispo: dispo,
                categorie: categorieId,
                photo: photo,
                taxe: taxe
            };
            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Articles/manage.php', body,
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe(async (res: any) => {
                if (res.error !== '') {
                    console.error('Error editing :', res.error);
                } else {
                    await this.onCancel();
                    await this.showToast('Article modifié.', 1000);
                }
            });
        });
    }

    async deleteArticle() {
        const alert = await this.alert.create({
            header: 'Supprimer l\'article',
            message: 'Etes-vous sûr(e) de vouloir supprimé cet article : ' + this.article.article_nom + ' ?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                },
                {
                    text: 'Oui',
                    handler: () => {
                        this.proService.getTokenProObs().then((tok: string) => {
                            const body = {
                                action: 'delete',
                                token: tok,
                                article_id: this.article.article_id,
                            };
                            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Articles/manage.php', body,
                                {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                    }
                                }).subscribe(async (res: any) => {
                                if (res.error !== '') {
                                    console.error('Error removing article:', res.error);
                                } else {
                                    await this.onCancel();
                                    await this.showToast('Article supprimé.', 1000);
                                }
                            });
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

}
