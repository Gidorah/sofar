import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CarteProPage } from './carte-pro.page';

describe('CarteProPage', () => {
  let component: CarteProPage;
  let fixture: ComponentFixture<CarteProPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarteProPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CarteProPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
