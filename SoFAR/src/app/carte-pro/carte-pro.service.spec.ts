import { TestBed } from '@angular/core/testing';

import { CarteProService } from './carte-pro.service';

describe('CarteProService', () => {
  let service: CarteProService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarteProService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
