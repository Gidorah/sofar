import {Component, OnInit} from '@angular/core';
import {GlobalVariable} from "../../global";
import {ModalController, NavParams, ToastController} from "@ionic/angular";
import {ProService} from "../../pro/pro.service";
import {HttpClient} from "@angular/common/http";
import {Categorie} from "../Categorie";
import {CarteProService} from "../carte-pro.service";

@Component({
    selector: 'app-create-article',
    templateUrl: './create-article.component.html',
    styleUrls: ['./create-article.component.scss'],
})
export class CreateArticleComponent implements OnInit {

    categories: [Categorie];

    nom: string;
    prix: number;
    desc: string;
    dispo: boolean;
    categorie: Categorie;
    photo: string;
    taxe: number;

    currencyFormatter: Intl.NumberFormat;

    oktext = GlobalVariable.OKTEXT;
    canceltext = GlobalVariable.CANCELTEXT;

    constructor(
        private modalController: ModalController,
        private proService: ProService,
        private toast: ToastController,
        private httpClient: HttpClient,
        public navParams: NavParams,
        private carteProService: CarteProService
    ) {
    }

    ngOnInit() {
        this.dispo = true;
        this.categories = this.carteProService.getCategoriesValue();
        if (this.navParams.get('cat_id')) {
            this.categorie = this.navParams.get('cat_id');
        } else {
            this.categorie = this.categories[0];
        }
        console.log('Categories:', this.categories);
        console.log('Categorie:', this.categorie);

    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    async onSubmit() {
        console.log('article: ', {
            nom: this.nom,
            prix: this.prix,
            desc: this.desc,
            dispo: this.dispo,
            categorie: this.categorie,
            photo: this.photo,
            taxe: this.taxe
        });
        if (this.nom === '') {
            await this.showToast('Le nom est trop court', 1500);
        } else if (this.desc === '') {
            await this.showToast('La description est trop courte', 1500);
        } else if (!this.prix) {
            await this.showToast('Insérer un prix', 1500);
        } else if (!this.taxe) {
            await this.showToast('Insérer le taux de taxe', 1500);
        } else {
            this.photo = "";
            this.createArticle(
                this.nom,
                Number(this.prix).toFixed(2),
                this.desc,
                this.dispo,
                this.categorie.categorie_id,
                this.photo,
                Number(this.taxe).toFixed(2)
            );
        }
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }

    updateNom($event) {
        this.nom = $event.target.value;
    }

    updatePrix($event) {
        this.prix = Number($event.target.value);
    }

    updateDescription($event) {
        this.desc = $event.target.value;
    }

    updateCategorie($event) {
        const newCatId = $event.detail.value;
        this.categories.forEach((cat: Categorie) => {
            if (cat.categorie_id === newCatId) {
                this.categorie = cat;
            }
        });
    }

    updateTaxe($event) {
        this.taxe = $event.target.value;
    }

    updateDispo($event: MouseEvent) {
        this.dispo = !this.dispo;
    }

    private createArticle(nom: string, prix: string, desc: string, dispo: boolean, categorieId: number, photo: string, taxe: string) {
        this.proService.getTokenProObs().then((tok: string) => {
            const body = {
                action: 'add',
                token: tok,
                nom: nom,
                prix: prix,
                desc: desc,
                dispo: dispo,
                categorie: categorieId,
                photo: photo,
                taxe: taxe
            };
            this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Articles/manage.php', body,
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).subscribe(async (res: any) => {
                if (res.error !== '') {
                    console.error('Error creating new article :', res.error);
                } else {
                    await this.onCancel();
                    await this.showToast('Article enregistré.', 1000);
                }
            });
        });
    }
}
