import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {Categorie} from "./Categorie";
import {HttpClient} from "@angular/common/http";
import {GlobalVariable} from "../global";
import {ProService} from "../pro/pro.service";
import {tap} from "rxjs/operators";
import {Article} from "./Article";

@Injectable({
    providedIn: 'root'
})
export class CarteProService {

    categories: BehaviorSubject<[Categorie]> = new BehaviorSubject<[Categorie]>(null);
    articles: BehaviorSubject<[Article]> = new BehaviorSubject<[Article]>(null);
    services: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(
        private httpClient: HttpClient,
        private proService: ProService
    ) {
    }

    getCategories(): Observable<[Categorie]> {
        return this.categories;
    }

    getCategoriesValue(): [Categorie] {
        return this.categories.value;
    }

    getArticles(): Observable<[Article]> {
        return this.articles;
    }

    getArticlesValue(): [Article] {
        return this.articles.value;
    }
/*
    getServices(id){
        return this.httpClient.get(GlobalVariable.BASE_API_URL + '/sofar-api/Carte/Services/data.php?var=' + this.id);


    }
*/
//    getServicesValue(): string{
  //      return this.services.value;
    //}

    async fetchCategories(idPro?: number) {
        console.log('fetchCategories()');
        const body = {
            action: 'fetch-all',
            id_pro: idPro !== undefined ? idPro : this.proService.getProValue().id_pro
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Categories/manage.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap((res: any) => {
                if (res.error !== '') {
                    console.error('Error fetching categories:', res.error);
                } else {
                    if (res.result) {
                        console.log('Fetched categories:', res.result);
                        this.categories.next(res.result);
                    } else {
                        console.error('Error getting categories');
                    }
                }
            }));
    }

    async fetchArticles(idPro?: number) {
        console.log('fetchArticle()');
        const body = {
            action: 'fetch-all',
            id_pro: idPro !== undefined ? idPro : this.proService.getProValue().id_pro
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Articles/manage.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap((res: any) => {
                if (res.error !== '') {
                    console.error('Error fetching articles:', res.error);
                } else {
                    if (res.result) {
                        /*
                        Dans la DB, article_dispo est un tinyint (0 ou 1), on le transforme ici en boolean
                         */
                        for (const art of res.result) {
                            art.article_dispo = art.article_dispo === '1';
                        }
                        console.log('Fetched articles:', res.result);
                        this.articles.next(res.result);
                    } else {
                        console.error('Error getting articles');
                    }
                }
            }));
    }

    async fetchServices(idPro?: number) {
        console.log('fetchServices()');
        const body = {
            action: 'fetch-all',
            id_pro: idPro !== undefined ? idPro : this.proService.getProValue().id_pro
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Services/manage.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap((res: any) => {
                if (res.error !== '') {
                    console.error('Error fetching services:', res.error);
                } else {
                    if (res.result) {
                        console.log('Fetched services:', res.result);
                        this.services.next(res.result);
                    } else {
                        console.error('Error getting services');
                    }
                }
            }));
    }
}
