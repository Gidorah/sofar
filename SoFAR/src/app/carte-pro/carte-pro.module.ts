import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarteProPageRoutingModule } from './carte-pro-routing.module';

import { CarteProPage } from './carte-pro.page';
import {CreateArticleComponent} from "./create-article/create-article.component";
import {CreateCategorieComponent} from "./create-categorie/create-categorie.component";
import {EditArticleComponent} from "./edit-article/edit-article.component";
import {EditCategorieComponent} from "./edit-categorie/edit-categorie.component";
import {ServicesComponent} from "./services/services.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarteProPageRoutingModule
  ],
  declarations: [CarteProPage, CreateArticleComponent, CreateCategorieComponent, EditArticleComponent, EditCategorieComponent, ServicesComponent]
})
export class CarteProPageModule {}
