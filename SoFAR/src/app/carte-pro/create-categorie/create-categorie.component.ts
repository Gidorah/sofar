import {Component, OnInit} from '@angular/core';
import {ModalController, ToastController} from "@ionic/angular";
import {ProService} from "../../pro/pro.service";
import {GlobalVariable} from "../../global";
import {HttpClient} from "@angular/common/http";

@Component({
    selector: 'app-create-categorie',
    templateUrl: './create-categorie.component.html',
    styleUrls: ['./create-categorie.component.scss'],
})
export class CreateCategorieComponent implements OnInit {

    categorieName: string;

    constructor(
        private modalController: ModalController,
        private proService: ProService,
        private toast: ToastController,
        private httpClient: HttpClient
    ) {
        this.categorieName = '';
    }

    ngOnInit() {
    }

    async onCancel() {
        await this.modalController.dismiss();
    }

    onSubmit() {
        if (this.categorieName !== '') {
            this.proService.getTokenProObs().then((tok: string) => {
                const body = {
                    token: tok,
                    cat_name: this.categorieName,
                    action: 'add'
                };
                this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Carte/Categories/manage.php', body,
                    {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).subscribe(async (res: any) => {
                    if (res.error !== '') {
                        console.error('Error creating new categorie;', res.error);
                    } else {
                        await this.onCancel();
                        await this.showToast('Catégorie enregistrée.', 1000);
                    }
                });
            });
        } else {
            this.showToast('Veuillez rentrer un nom de catégorie correct.', 2000);
        }
    }

    getNewCatName(event) {
        this.categorieName = event.target.value;
    }

    async showToast(message, duration) {
        const toast = await this.toast.create({
            message,
            duration
        });
        await toast.present();
    }
}
