export interface Categorie {
    id_pro: number;
    categorie_id: number;
    categorie_nom: string;
}
