export interface Article {
    article_id: number;
    id_pro: number;
    article_nom: string;
    article_prix: number;
    article_desc: string;
    article_dispo: boolean;
    article_categorie: number;
    article_photo: string;
    article_taxe: number;
}