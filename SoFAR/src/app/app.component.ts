import {Component, VERSION} from '@angular/core';

import {AlertController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';
import {UserService} from "./user/user.service";
import {ProService} from "./pro/pro.service";
import {PersonnelService} from "./personnel.service";

import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import {Storage} from "@ionic/storage";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public storage: Storage,
        public router: Router,
        private userService: UserService,
        private proService: ProService,
        public alertCtrl: AlertController,
        private persService: PersonnelService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleBlackTranslucent();
            this.splashScreen.hide();
            this.statusBar.overlaysWebView(false);
            this.statusBar.backgroundColorByHexString("#1f1f1f");
        });
    }

}
