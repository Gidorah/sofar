import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabsPartenairePage } from './tabs-partenaire.page';

describe('TabsPartenairePage', () => {
  let component: TabsPartenairePage;
  let fixture: ComponentFixture<TabsPartenairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsPartenairePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabsPartenairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
