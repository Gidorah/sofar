import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPartenairePage } from './tabs-partenaire.page';

const routes: Routes = [

{
  path: '',
  component: TabsPartenairePage,
  children: [
    {
      path: 'calendrier',
      loadChildren: () => import('../calendrier/calendrier.module').then(m => m.CalendrierPageModule)
    },
    {
      path: 'services',
      loadChildren: () => import('../services/services.module').then(m => m.ServicesPageModule)
    },
    {
      path: 'carte-pro',
      loadChildren: () => import('../carte-pro/carte-pro.module').then(m => m.CarteProPageModule)
    },
    {
      path: 'mon-compte-partenaire',
      loadChildren: () => import('../mon-compte-partenaire/mon-compte-partenaire.module').then(m => m.MonComptePartenairePageModule)
    },
    {
      path: 'gestion-personnel',
      loadChildren: () => import('../gestion-personnel/gestion-personnel.module').then(m => m.GestionPersonnelPageModule)
    },
    {
      path: '',
      redirectTo: 'services',
      pathMatch: 'full'
    }
  ],
  },
  {
    path: '',
    redirectTo: 'services',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPartenairePageRoutingModule {}
