import {Component, OnInit} from '@angular/core';
import {StorageService} from '../storage.service';
import {ProService} from "../pro/pro.service";
import {Pro} from "../pro/pro";

@Component({
    selector: 'app-tabs-partenaire',
    templateUrl: './tabs-partenaire.page.html',
    styleUrls: ['./tabs-partenaire.page.scss'],
})
export class TabsPartenairePage implements OnInit {

    constructor(
        public proService: ProService) {
    }

    ngOnInit() {
        this.proService.fetchPro().then(obs => obs.subscribe());
    }

    ionViewWillEnter() {
    }
}
