import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsPartenairePageRoutingModule } from './tabs-partenaire-routing.module';

import { TabsPartenairePage } from './tabs-partenaire.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsPartenairePageRoutingModule
  ],
  declarations: [TabsPartenairePage]
})
export class TabsPartenairePageModule {}
