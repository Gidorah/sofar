import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {GlobalVariable} from "./global";
import {tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {Personnel} from "./personnel";

@Injectable({
    providedIn: 'root'
})
export class PersonnelService {

    private personnel: BehaviorSubject<Personnel> = new BehaviorSubject(null);
    private etab = new BehaviorSubject(null);

    constructor(
        private httpClient: HttpClient,
        private storage: Storage
    ) {
        this.storage.create().then(() => {
            this.storage.get('PERS').then((pers: Personnel) => {
                this.personnel.next(pers);
            });
        });
    }

    login(body: any, stayConnected: boolean): Observable<any> {
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Personnel/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    if (res.result.pers) {
                        const pers = res.result.pers;
                        await this.storage.set('ACCESS_TOKEN_PERS', res.result.access_token).then();
                        await this.storage.set('PERS_STAY_CONNECTED', true);

                        this.personnel.next({
                            id_per: pers.id_per,
                            id_pro: pers.id_pro,
                            alias: pers.alias,
                            date_creation: pers.date_creation
                        });
                        await this.storage.set('PERS', this.personnel.value);
                    }
                }
            })
        );
    }

    async logout() {
        await this.storage.remove('ACCESS_TOKEN_PERS');
        await this.storage.remove('PERS_STAY_CONNECTED');
        await this.storage.remove('PERS');
        this.personnel.next(null);
    }

    async getAutoConnect() {
        return this.storage.get('PERS_STAY_CONNECTED');
    }

    async fetchPers(): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            action: 'fetch'
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Personnel/manage_data.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    if (res.pers) {
                        const pers = res.pers;

                        this.personnel.next({
                            id_per: pers.id_per,
                            id_pro: pers.id_pro,
                            alias: pers.alias,
                            date_creation: pers.date_creation
                        });
                        await this.storage.set('PERS', this.personnel.value);
                    }
                    if (res.etab) {
                        res.etab.commander_dispo = res.etab.commander_dispo === '1';
                        this.etab.next(res.etab);
                    }

                } else {
                    console.log('error fetching pers');
                }
            })
        );
    }

    getTokenObs(): Promise<any> {
        return this.storage.get('ACCESS_TOKEN_PERS');
    }


    getPersonnel(): Observable<Personnel> {
        return this.personnel;
    }

    getEtab(): Observable<any> {
        return this.etab;
    }

    getEtabValue(): any {
        return this.etab.value;
    }

    async changePassword(pw, newPw): Promise<Observable<any>> {
        const tok = await this.getTokenObs();
        const body = {
            token: tok,
            password: pw,
            new_password: newPw
        };
        return this.httpClient.post(GlobalVariable.BASE_API_URL + '/sofar_api/Personnel/update_password.php', body,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            }).pipe(
            tap(async (res: any) => {
                if (res.error === '') {
                    console.log('Changed password');
                } else {
                    console.log('error while changing password');
                }
            })
        );
    }

    isPersConnected(): boolean {
        return this.personnel.value !== null;
    }
}
