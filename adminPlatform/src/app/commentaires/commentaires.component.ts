import { Component, OnInit } from '@angular/core';
import {StorageService} from "../storage.service";

@Component({
  selector: 'app-commentaires',
  templateUrl: './commentaires.component.html',
  styleUrls: ['./commentaires.component.css']
})
export class CommentairesComponent implements OnInit {

  dataNotes: any;
  status: any;
  constructor(
    private notesData:StorageService,
    private IdComment: StorageService
  ) { }

  // @ts-ignore
  DeleteCommentaire(e){
      var idNote = e.getAttribute('data-id');
      this.IdComment.CommentCurrentId=idNote;
      console.log(idNote);
      console.warn("commentId: ", this.IdComment.CommentCurrentId);

      this.notesData.deleteComments().subscribe(() => this.status = 'Delete successful');;
      console.log("details supression", this.IdComment.CommentCurrentId);
      window.alert("Le commentaire a été supprimé");

  }
  ngOnInit(): void {
    this.notesData.getComments().subscribe((resultNotes)=>{
      this.dataNotes=resultNotes;

    })
  }

}
