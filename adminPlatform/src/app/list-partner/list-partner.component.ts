import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service'


@Component({
  selector: 'app-list-partner',
  templateUrl: './list-partner.component.html',
  styleUrls: ['./list-partner.component.css']
})
export class ListPartnerComponent implements OnInit {

  pro: any

  constructor( private partnersData:StorageService,
               private testId: StorageService) { }

  // @ts-ignore
  clickEvent(e){

    var id = e.getAttribute('data-id');
    this.testId.currentId=id;
    console.log(id);
    console.warn("proid: ", this.testId.currentId);

  }

  ngOnInit() {
    console.log("test1");
    this.partnersData.getPartner().subscribe((result)=>{
      console.warn("result",result);
      this.pro=result;

    })

  }

}
