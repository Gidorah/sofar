import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service'
import * as $ from "jquery";
import {Observable} from "rxjs";
import {GlobalVariable} from "../global";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-list-details',
  templateUrl: './list-details.component.html',
  styleUrls: ['./list-details.component.css']
})
export class ListDetailsComponent implements OnInit {

  test: undefined;
  item : any
  proDetails: any
  status: any
  comptoir:any;
  table:any;
  tout:any;
  commander_dispo: any;
  commanderFalse: any;
  commanderTrue: any;
  terrasse:any;
  url=GlobalVariable.BASE_API_URL;



  constructor(
    private testId: StorageService,
    private ProData: StorageService,
    private http: HttpClient

  ) {

  }

  deletePro123(){

    this.ProData.deletePartner().subscribe(() => this.status = 'Delete successful');;
    console.log("details supression", this.testId.currentId);
    location.replace("/listPartner");
    window.alert("Suppression réussie");

  }

  ngOnInit() {
    this.proDetails = this.ProData.getDetails().subscribe((result) => {
      this.proDetails = result;


      if(this.proDetails[0].service=="tout"){
        this.tout=true;
        this.comptoir=false;
        this.table=false;
      }
      else if(this.proDetails[0].service=="table"){
        this.tout=false;
        this.comptoir=false;
        this.table=true;
      }
      else if(this.proDetails[0].service=="comptoir"){
        this.tout=false;
        this.comptoir=true;
        this.table=false;
      }

      if(this.proDetails[0].commander_dispo == 1){
        console.warn("true",this.proDetails[0].commander_dispo);
        this.commanderTrue = true;
        this.commanderFalse = false;
      } else if(this.proDetails[0].commander_dispo == 0){
        console.warn("false",this.proDetails[0].commander_dispo);
        this.commanderTrue = false;
        this.commanderFalse = false;
      }

      this.proDetails[0].lundimatin=this.proDetails[0].lundi.split('/')[0];
      this.proDetails[0].lundisoir=this.proDetails[0].lundi.split('/')[1];
      this.proDetails[0].lundiouverturematin=this.proDetails[0].lundimatin.split("-")[0];
      this.proDetails[0].lundifermeturematin=this.proDetails[0].lundimatin.split("-")[1];
      if(this.proDetails[0].lundisoir!=undefined) {
        this.proDetails[0].lundiouverturesoir=this.proDetails[0].lundisoir.split("-")[0];
        this.proDetails[0].lundifermeturesoir=this.proDetails[0].lundisoir.split("-")[1];
      }

      this.proDetails[0].mardimatin=this.proDetails[0].mardi.split('/')[0];
      this.proDetails[0].mardisoir=this.proDetails[0].mardi.split('/')[1];
      this.proDetails[0].mardiouverturematin=this.proDetails[0].mardimatin.split("-")[0];
      this.proDetails[0].mardifermeturematin=this.proDetails[0].mardimatin.split("-")[1];
      if(this.proDetails[0].mardisoir!=undefined) {
        this.proDetails[0].mardiouverturesoir=this.proDetails[0].mardisoir.split("-")[0];
        this.proDetails[0].mardifermeturesoir=this.proDetails[0].mardisoir.split("-")[1];
      }

      this.proDetails[0].mercredimatin=this.proDetails[0].mercredi.split('/')[0];
      this.proDetails[0].mercredisoir=this.proDetails[0].mercredi.split('/')[1];
      this.proDetails[0].mercrediouverturematin=this.proDetails[0].mercredimatin.split("-")[0];
      this.proDetails[0].mercredifermeturematin=this.proDetails[0].mercredimatin.split("-")[1];
      if(this.proDetails[0].mercredisoir!=undefined) {
        this.proDetails[0].mercrediouverturesoir=this.proDetails[0].mercredisoir.split("-")[0];
        this.proDetails[0].mercredifermeturesoir=this.proDetails[0].mercredisoir.split("-")[1];
      }

      this.proDetails[0].jeudimatin=this.proDetails[0].jeudi.split('/')[0];
      this.proDetails[0].jeudisoir=this.proDetails[0].jeudi.split('/')[1];
      this.proDetails[0].jeudiouverturematin=this.proDetails[0].jeudimatin.split("-")[0];
      this.proDetails[0].jeudifermeturematin=this.proDetails[0].jeudimatin.split("-")[1];
      if(this.proDetails[0].jeudisoir!=undefined) {
        this.proDetails[0].jeudiouverturesoir=this.proDetails[0].jeudisoir.split("-")[0];
        this.proDetails[0].jeudifermeturesoir=this.proDetails[0].jeudisoir.split("-")[1];
      }

      this.proDetails[0].vendredimatin=this.proDetails[0].vendredi.split('/')[0];
      this.proDetails[0].vendredisoir=this.proDetails[0].vendredi.split('/')[1];
      this.proDetails[0].vendrediouverturematin=this.proDetails[0].vendredimatin.split("-")[0];
      this.proDetails[0].vendredifermeturematin=this.proDetails[0].vendredimatin.split("-")[1];
      if(this.proDetails[0].vendredisoir!=undefined) {
        this.proDetails[0].vendrediouverturesoir=this.proDetails[0].vendredisoir.split("-")[0];
        this.proDetails[0].vendredifermeturesoir=this.proDetails[0].vendredisoir.split("-")[1];
      }

      this.proDetails[0].samedimatin=this.proDetails[0].samedi.split('/')[0];
      this.proDetails[0].samedisoir=this.proDetails[0].samedi.split('/')[1];
      this.proDetails[0].samediouverturematin=this.proDetails[0].samedimatin.split("-")[0];
      this.proDetails[0].samedifermeturematin=this.proDetails[0].samedimatin.split("-")[1];
      if(this.proDetails[0].samedisoir!=undefined) {
        this.proDetails[0].samediouverturesoir=this.proDetails[0].samedisoir.split("-")[0];
        this.proDetails[0].samedifermeturesoir=this.proDetails[0].samedisoir.split("-")[1];
      }

      this.proDetails[0].dimanchematin=this.proDetails[0].dimanche.split('/')[0];
      this.proDetails[0].dimanchesoir=this.proDetails[0].dimanche.split('/')[1];
      this.proDetails[0].dimancheouverturematin=this.proDetails[0].dimanchematin.split("-")[0];
      this.proDetails[0].dimanchefermeturematin=this.proDetails[0].dimanchematin.split("-")[1];
      if(this.proDetails[0].dimanchesoir!=undefined) {
        this.proDetails[0].dimancheouverturesoir=this.proDetails[0].dimanchesoir.split("-")[0];
        this.proDetails[0].dimanchefermeturesoir=this.proDetails[0].dimanchesoir.split("-")[1];
      }
    })
  }

  modifierPro123(){
    this.proDetails[0].terrasse_pro = this.proDetails[0].terasse;
    console.warn("valeur terrasse:1", this.proDetails[0].terasse);
    console.warn("valeur terrasse:2", this.proDetails[0].terrasse_pro);
   // console.warn("valeur terrasse:3", this.item.terasse);

    if(this.proDetails[0].lundimatin=="ferme"){
      this.proDetails[0].lundi="ferme";
    }
    else if(this.proDetails[0].lundisoir==undefined){
      this.proDetails[0].lundi=this.proDetails[0].lundiouverturematin+"-"+this.proDetails[0].lundifermeturematin;
    }
    else{
      this.proDetails[0].lundi=this.proDetails[0].lundiouverturematin+"-"+this.proDetails[0].lundifermeturematin+"/"+this.proDetails[0].lundiouverturesoir+"-"+this.proDetails[0].lundifermeturesoir;
    }

    if(this.proDetails[0].mardimatin=="ferme"){
      this.proDetails[0].mardi="ferme";
    }
    else if(this.proDetails[0].mardisoir==undefined){
      this.proDetails[0].mardi=this.proDetails[0].mardiouverturematin+"-"+this.proDetails[0].mardifermeturematin;
    }
    else{
      this.proDetails[0].mardi=this.proDetails[0].mardiouverturematin+"-"+this.proDetails[0].mardifermeturematin+"/"+this.proDetails[0].mardiouverturesoir+"-"+this.proDetails[0].mardifermeturesoir;
    }

    if(this.proDetails[0].mercredimatin=="ferme"){
      this.proDetails[0].mercredi="ferme";
    }
    else if(this.proDetails[0].mercredisoir==undefined){
      this.proDetails[0].mercredi=this.proDetails[0].mercrediouverturematin+"-"+this.proDetails[0].mercredifermeturematin;
    }
    else{
      this.proDetails[0].mercredi=this.proDetails[0].mercrediouverturematin+"-"+this.proDetails[0].mercredifermeturematin+"/"+this.proDetails[0].mercrediouverturesoir+"-"+this.proDetails[0].mercredifermeturesoir;
    }


    if(this.proDetails[0].jeudimatin=="ferme"){
      this.proDetails[0].jeudi="ferme";
    }
    else if(this.proDetails[0].jeudisoir==undefined){
      this.proDetails[0].jeudi=this.proDetails[0].jeudiouverturematin+"-"+this.proDetails[0].jeudifermeturematin;
    }
    else{
      this.proDetails[0].jeudi=this.proDetails[0].jeudiouverturematin+"-"+this.proDetails[0].jeudifermeturematin+"/"+this.proDetails[0].jeudiouverturesoir+"-"+this.proDetails[0].jeudifermeturesoir;
    }


    if(this.proDetails[0].vendredimatin=="ferme"){
      this.proDetails[0].vendredi="ferme";
    }
    else if(this.proDetails[0].vendredisoir==undefined){
      this.proDetails[0].vendredi=this.proDetails[0].vendrediouverturematin+"-"+this.proDetails[0].vendredifermeturematin;
    }
    else{
      this.proDetails[0].vendredi=this.proDetails[0].vendrediouverturematin+"-"+this.proDetails[0].vendredifermeturematin+"/"+this.proDetails[0].vendrediouverturesoir+"-"+this.proDetails[0].vendredifermeturesoir;
    }


    if(this.proDetails[0].samedimatin=="ferme"){
      this.proDetails[0].samedi="ferme";
    }
    else if(this.proDetails[0].samedisoir==undefined){
      this.proDetails[0].samedi=this.proDetails[0].samediouverturematin+"-"+this.proDetails[0].samedifermeturematin;
    }
    else{
      this.proDetails[0].samedi=this.proDetails[0].samediouverturematin+"-"+this.proDetails[0].samedifermeturematin+"/"+this.proDetails[0].samediouverturesoir+"-"+this.proDetails[0].samedifermeturesoir;
    }


    if(this.proDetails[0].dimanchematin=="ferme"){
      this.proDetails[0].dimanche="ferme";
    }
    else if(this.proDetails[0].dimanchesoir==undefined){
      this.proDetails[0].dimanche=this.proDetails[0].dimancheouverturematin+"-"+this.proDetails[0].dimanchefermeturematin;
    }
    else{
      this.proDetails[0].dimanche=this.proDetails[0].dimancheouverturematin+"-"+this.proDetails[0].dimanchefermeturematin+"/"+this.proDetails[0].dimancheouverturesoir+"-"+this.proDetails[0].dimanchefermeturesoir;
    }

    console.log(this.proDetails[0].nom_pro);
    console.log(this.proDetails[0].adresse);
    console.log(this.proDetails[0].email_pro);
    console.log(this.proDetails[0].siret);
    console.log(this.proDetails[0].description_pro);
    console.log(this.proDetails[0].terrasse_pro);
    console.log(this.proDetails[0].service);
    console.log(this.proDetails[0].commander_dispo);
    console.log(this.proDetails[0].categorie_pro);
    console.log(this.proDetails[0].lundi);
    console.log(this.proDetails[0].mardi);
    console.log(this.proDetails[0].mercredi);
    console.log(this.proDetails[0].jeudi);
    console.log(this.proDetails[0].vendredi);
    console.log(this.proDetails[0].samedi);
    console.log(this.proDetails[0].dimanche);

    $.getJSON('https://api-adresse.data.gouv.fr/search/?q=' + this.proDetails[0].adresse, async (data:any) => {
      this.proDetails[0].longitude=data.features[0].geometry.coordinates[0];
      this.proDetails[0].latitude=data.features[0].geometry.coordinates[1];
      console.log(this.proDetails[0].latitude);
      console.log(this.proDetails[0].longitude);
      this.modifyPartner();
      }
    );

  }

  modifyPartner(){
    console.log(this.proDetails[0]);
    this.http.post(this.url + '/sofar_admin/modifyPro.php', JSON.stringify(this.proDetails[0]), {
      headers : {
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      }}).subscribe((data: any) => {console.log(data)
      if(data!=''){
        window.alert("Modification effectuée !");
      }});

  }


}
