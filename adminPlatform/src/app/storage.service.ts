import { Injectable } from '@angular/core';
import {GlobalVariable} from "./global";
import { HttpHeaders, HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  pro: any = '';
  currentId: any;
  CommentCurrentId: any;

  url=GlobalVariable.BASE_API_URL;
  proId: any;


  constructor( private http: HttpClient) { }
  getPartner()
  {
   return this.http.get(this.url + '/sofar_admin/listPro.php');
  }

  addPartner( formData: FormData ): Observable<any> {
    let headers: any = new HttpHeaders({'Content-Type': 'application/json'});

      return this.http.post(this.url + '/sofar_admin/addPro.php', JSON.stringify(formData), {
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }})

  }

  getDetails(  ){
    return this.http.get(this.url + '/sofar_admin/detailsPro.php?var='+ this.currentId     )

  }

  deletePartner( ){
    return this.http.get(this.url + '/sofar_admin/deletePro.php?var='+ this.currentId)
  }

  getComments()
  {
    return this.http.get(this.url + '/sofar_admin/listCommentaires.php');
  }

  deleteComments(){
    return this.http.get(this.url + '/sofar_admin/deleteCommentaires.php?var='+ this.CommentCurrentId)
  }


}
