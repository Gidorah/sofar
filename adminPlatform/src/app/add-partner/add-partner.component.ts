import { Component} from '@angular/core';
import { StorageService } from '../storage.service';
import { HttpClient} from '@angular/common/http';
import * as $ from 'jquery';

@Component({
  selector: 'app-add-partner',
  templateUrl: './add-partner.component.html',
  styleUrls: ['./add-partner.component.css']

})

export class AddPartnerComponent{
  response =""

  pro: any = {};
  shown: any ;
  shown1: any;
  shown2: any;
  shown3: any ;
  shown4: any;
  shown5: any;
  shown6: any;
  demiL:any;
  demiMa:any;
  demiMe:any;
  demiJ:any;
  demiV:any;
  demiS:any;
  demiD:any;


  constructor(
    //private formBuilder: FormBuilder,
    public http: HttpClient,

    private formData:StorageService,
    private storageService: StorageService
  ){

  }

  onSubmit(){

    if(this.pro.name==undefined){
      window.alert("ATTENTION : Nom de l'établissement à renseigner !");
    }
    else if(this.pro.siret==undefined){
      window.alert("ATTENTION : Numéro de siret de l'établissement à renseigner !");
    }
    else if(this.pro.email==undefined){
      window.alert("ATTENTION : E-mail de l'établissement à renseigner !");
    }
    else if(this.pro.adresse==undefined){
      window.alert("ATTENTION : Adresse de l'établissement à renseigner !");
    }
    else if(this.pro.cat==undefined){
      window.alert("ATTENTION : Catégorie de l'établissement à renseigner !");
    }
    else if(this.pro.terasse==undefined){
      window.alert("ATTENTION : Térrasse de l'établissement à renseigner !");
    }else if(this.pro.commander==undefined){
      window.alert("ATTENTION : Possibilité de commander de l'établissement à renseigner !");
    }
    else if(this.pro.service==undefined){
      window.alert("ATTENTION : Type de service de l'établissement à renseigner !");
    }
    else{
      $.getJSON('https://api-adresse.data.gouv.fr/search/?q=' + this.pro.adresse, async (data:any) => {
        this.pro.longitude=data.features[0].geometry.coordinates[0];
        this.pro.latitude=data.features[0].geometry.coordinates[1];

        this.storageService.addPartner(this.pro).subscribe( res =>{
            console.log(res);
          },
          err =>{
            console.log(err);
          }
        )
        }
      );
    }
  }

  showL(){
    if (!this.pro.lundi) {
      this.shown=1;
    }else {
      this.shown=0;
      this.pro.horairesOuvertS= "NULL";
      this.pro.horairesFermeS= "NULL";
    }
  }

  showMa(){
    if (!this.pro.Mardi) {
      this.shown1=1;
    }else {
      this.shown1=0;
      this.pro.horairesOuvertS= "NULL";
      this.pro.horairesFermeS= "NULL";
    }
  }

  showMe(){
    if (!this.pro.Mercredi) {
      this.shown2=1;
    }else{
      this.shown2=0;
      this.pro.horairesOuvertS= "NULL";
      this.pro.horairesFermeS= "NULL";
    }
  }

  showJ(){
    if (!this.pro.Jeudi) {
      this.shown3=1;
    }else{
      this.shown3=0;
      this.pro.horairesOuvertS= "NULL";
      this.pro.horairesFermeS= "NULL";
    }
  }

  showV(){
    if (!this.pro.Vendredi) {
      this.shown4=1;
    }else{
      this.shown4=0;
      this.pro.horairesOuvertS= "NULL";
      this.pro.horairesFermeS= "NULL";
    }
  }

  showS(){
    if (!this.pro.Samedi) {
      this.shown5=1;
    }else{
      this.shown5=0;
      this.pro.horairesOuvertS= "NULL";
      this.pro.horairesFermeS= "NULL";
    }

  }

  showD(){
    if (!this.pro.Dimanche) {
      this.shown6=1;
    }else{
      this.shown6=0;
      this.pro.horairesOuvertD= "NULL";
      this.pro.horairesFermeD= "NULL";
    }


  }

  demijourneeD(){
    if (!this.pro.demiDimanche) {
      this.demiD=1;
    }else{
      this.demiD=0;
      this.pro.demiHorairesOuvertD= "NULL";
      this.pro.demiHorairesFermeD= "NULL";
    }

  }

  demijourneeL(){
    if (!this.pro.demiLundi) {
      this.demiL=1;
    }else{
      this.demiL=0;
      this.pro.demiHorairesOuvertL= "NULL";
      this.pro.demiHorairesFermeL= "NULL";
    }

  }

  demijourneeMa(){
    if (!this.pro.demiMardi) {
      this.demiMa=1;
    }else{
      this.demiMa=0;
      this.pro.demiHorairesOuvertMa= "NULL";
      this.pro.demiHorairesFermeMa= "NULL";
    }
  }

  demijourneeMe(){
    if (!this.pro.demiMecredi) {
      this.demiMe=1;
    }else{
      this.demiMe=0;
      this.pro.demiHorairesOuvertMe= "NULL";
      this.pro.demiHorairesFermeMe= "NULL";
    }
  }

  demijourneeJ(){
    if (!this.pro.demiJeudi) {
      this.demiJ=1;
    }else{
      this.demiJ=0;
      this.pro.demiHorairesOuvertJ= "NULL";
      this.pro.demiHorairesFermeJ= "NULL";
    }
  }

  demijourneeV(){
    if (!this.pro.demiVendredi) {
      this.demiV=1;
    }else{
      this.demiV=0;
      this.pro.demiHorairesOuvertV= "NULL";
      this.pro.demiHorairesFermeV= "NULL";
    }
  }
  demijourneeS(){
    if (!this.pro.demiSamedi) {
      this.demiS=1;
    }else{
      this.demiS=0;
      this.pro.demiHorairesOuvertS= "NULL";
      this.pro.demiHorairesFermeS= "NULL";
    }
  }

  ngOnInit(): void {
    }

  }




