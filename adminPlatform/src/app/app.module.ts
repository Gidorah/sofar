import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { AddPartnerComponent } from './add-partner/add-partner.component';
import { ListPartnerComponent } from './list-partner/list-partner.component';
import {RouterModule} from "@angular/router";
import { HomeComponent } from './home/home.component';
import { HttpClient } from  '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { StorageService } from './storage.service';
import { ListDetailsComponent } from './list-details/list-details.component';
import { CommentairesComponent } from './commentaires/commentaires.component'


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AddPartnerComponent,
    ListPartnerComponent,
    HomeComponent,
    ListDetailsComponent,
    CommentairesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: 'listPartner' , component: ListPartnerComponent
      },
      {
        path: 'addPartner' , component: AddPartnerComponent
      },
      {
        path: 'home', component: HomeComponent
      },
      {
        path: 'listDetails', component: ListDetailsComponent
      },
      {
        path: 'commentaires', component: CommentairesComponent
      }
    ])
  ],
  providers: [ StorageService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
